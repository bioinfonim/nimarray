from dbconnect.dbConnect import dbConnect
import configparser
import json
import os.path
from nimdate import date2string

campos_request = "Chr, PositionStart, PositionStop, Categorization, CopyNumber, GeneNames, coments, " \
                  "PETICIONCB"

class PETICIONCB:
    PETICIONCB = ""  # separados por comas luego los parseo.
    db = None
    config_parser = configparser.ConfigParser(allow_no_value=True)
    
    def __init__(self,PETICIONCB):
        self.PETICIONCB = PETICIONCB
        # Conexion NIMArray
        bindir = os.path.abspath(os.path.dirname(__file__))
        self.config_parser.read(bindir+"/cfg.cnf")
        host = self.config_parser.get("DB", "hostname")
        db = self.config_parser.get("DB", "db")
        user = self.config_parser.get("DB", "user")
        password = self.config_parser.get("DB", "password")
        self.db = dbConnect(host, user, password, db)
        
    def peticion(self):
        # Información del paciente
        where1 = ('IDPACIENTE = (SELECT IDPACIENTE FROM PETICIONES WHERE PETICIONCB = "%s")' % (self.PETICIONCB))
        PACIENTE = self.db.select_df('PACIENTES', '*', where1)
        where2 = ('PETICIONCB = "%s"' % (self.PETICIONCB))
        TRESULTADO = self.db.select_df('PETICIONES', 'TRESULTADO', where2)
        PACIENTE['TRESULTADO'] = TRESULTADO
        PACIENTE = PACIENTE.apply(lambda row: date2string(row), axis=1)

        # Información peticiones
        where3 = "PETICIONCB= '%s'" % (self.PETICIONCB)
        VARIANTES_INFORMADAS = self.db.select_df('VARIANTES_INFORMADAS', campos_request, where3)
        VARIANTES_INFORMADAS = VARIANTES_INFORMADAS.apply(lambda row:date2string(row), axis=1)

        PACIENTE['Variantes_Informadas'] = VARIANTES_INFORMADAS.to_json(orient='records', date_format='iso')

        return PACIENTE.to_json(orient='records', date_format='iso')

