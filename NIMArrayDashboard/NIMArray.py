import sys
import getopt
from NIMArraygenes import GENES
from NIMArraycoord import RANGOCOORDENADAS
from NIMArraypetcb import PETICIONCB
from NIMArrayapellidos import APELLIDOS

# Conexion NIMArray

def help():
    print ("Este programa permite consultar el paciente y las alteraciones según su peticion (PETICION),"
           "las variantes según el nombre de uno o varios genes (GENES), según sus coordenadas (COORDENADAS) y "
           "por sus apellidos (APELLIDOS)")
    sys.exit()


if len(sys.argv) == 1:
    print('No se ha elegido una opción')
    exit(-1)

try:
    opts, args = getopt.getopt(sys.argv[1:], 'h:o:c:t:p:b:g:a:', ['help', "opcion=", "Chr=", "PosStart=", "PosStop=", "PETICION=", "genes=", "apellidos="])
    # print(args)
    # print(opts)

   #if len(args)>0:

except getopt.GetoptError as msg:
    help()

for opt,  arg in opts:
    if opt in ('-h', '--help'):
        help()
    elif opt in ('-o', "--opcion"):
        OPCION = arg
    elif opt in ('-c', "--Chr"):
        Chr = arg
    elif opt in ('-t', "--PosStart"):
        PosStart = arg
    elif opt in ('-p', "--PosStop"):
        PosStop = arg
    elif opt in ('-b', "--PETICION"):
        PETICION = arg
    elif opt in ('-g', "--genes"):
        genes = arg
    elif opt in ('-a', "--apellidos"):
        apellidos = arg

if OPCION.upper() == "PETICION":
    pet = PETICIONCB(PETICION)
    print(pet.peticion())

if OPCION.upper() == "GENES":
    g = GENES(genes)
    print(g.genes())

if OPCION.upper() == "COORDENADAS":
    coords = RANGOCOORDENADAS(Chr, PosStart, PosStop)
    print(coords.rangocoordenadas())

if OPCION.upper() == "APELLIDOS":
    apellidos = APELLIDOS(apellidos)
    print(apellidos.apellidos())
