from dbconnect.dbConnect import dbConnect
import configparser
import json
import os.path
from nimdate import date2string

tablas_request = 'PACIENTES pac join PACIENTE_PETICION pp on (pac.IDPACIENTE = pp.IDPACIENTE) join PETICIONES ' \
                 'pet on (pp.PETICIONCB = pet.PETICIONCB) join VARIANTES_INFORMADAS vi on (vi.PETICIONCB = pet.PETICIONCB)'
campos_request = "vi.Chr, vi.PositionStart, vi.PositionStop, vi.Categorization, vi.GeneNames, pet.TRESULTADO, " \
                  "pet.PETICIONCB, vi.coments, pac.APELLIDOS, pac.NOMBRE"

class GENES:
    genes = ""  # separados por comas luego los parseo.
    db = None
    config_parser = configparser.ConfigParser(allow_no_value=True)
    
    def __init__(self, GENES):
        self.GENES = GENES
        
        # Conexion NIMArray
        bindir = os.path.abspath(os.path.dirname(__file__))
        self.config_parser.read(bindir+"/cfg.cnf")
        host = self.config_parser.get("DB", "hostname")
        db = self.config_parser.get("DB", "db")
        user = self.config_parser.get("DB", "user")
        password = self.config_parser.get("DB", "password")
        self.db = dbConnect(host, user, password,db)
        
    def genes(self):
        
        listaGenes = self.GENES
        listaGenes = str(listaGenes)
        listaGenes = listaGenes.split(sep=',')

        where = ""

        for n, val in enumerate(listaGenes):

            val = val.strip()
            if n != len(listaGenes)-1:
              where += " vi.GeneNames LIKE '%%%s%%'" % val + "OR"
            else:
                where += " vi.GeneNames LIKE '%%%s%%'" % val

        RESULTADO = self.db.select_df(tablas_request, campos_request, where)
        RESULTADO = RESULTADO.apply(lambda row:date2string(row), axis=1)

        return RESULTADO.to_json(orient='records', date_format='iso')
