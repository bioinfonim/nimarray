from dbconnect.dbConnect import dbConnect
import configparser
import json
import os.path
from nimdate import date2string

tablas_request = 'PACIENTES pac join PACIENTE_PETICION pp on (pac.IDPACIENTE = pp.IDPACIENTE) join PETICIONES ' \
                 'pet on (pp.PETICIONCB = pet.PETICIONCB) join VARIANTES_INFORMADAS vi on (vi.PETICIONCB = pet.PETICIONCB)'
campos_request = "vi.Chr, vi.PositionStart, vi.PositionStop, vi.Categorization, vi.GeneNames, pet.TRESULTADO, " \
                  "pet.PETICIONCB, vi.coments, pac.APELLIDOS, pac.NOMBRE"


class RANGOCOORDENADAS:
    PosStart = ""
    PosStop = ""
    Chr = ""
    db = None
    config_parser = configparser.ConfigParser(allow_no_value=True)
    
    def __init__(self, Chr, PosStart, PosStop):
        self.PosStart = PosStart 
        self.PosStop = PosStop
        self.Chr = Chr

        # Conexion NIMArray
        bindir = os.path.abspath(os.path.dirname(__file__))
        self.config_parser.read(bindir+"/cfg.cnf")
        host = self.config_parser.get("DB", "hostname")
        db = self.config_parser.get("DB", "db")
        user = self.config_parser.get("DB", "user")
        password = self.config_parser.get("DB", "password")
        self.db = dbConnect(host, user, password,db)
        
    def rangocoordenadas(self):

        PosStart = int(self.PosStart) - 15000
        PosStop = int(self.PosStop) + 15000

        where = " Chr = '%s' and PositionStart >= %s and PositionStop <= %s" % (self.Chr, PosStart, PosStop)
        
        RESULTADO = self.db.select_df(tablas_request, campos_request, where)
        RESULTADO = RESULTADO.apply(lambda row:date2string(row),axis=1)
           
        return RESULTADO.to_json(orient='records', date_format='iso')

