from dbconnect.dbConnect import dbConnect
import configparser
import json
import os.path
import pandas as pd
from nimdate import date2string

campos = ('IDPACIENTE, APELLIDOS, NOMBRE, NIF, SEXO, EDAD')
cols = ['APELLIDOS', 'NOMBRE', 'EDAD', 'SEXO', 'NIF', 'PETICIONCB', 'IDPACIENTE']

class APELLIDOS:
    apellidos = ""
    db = None
    config_parser = configparser.ConfigParser(allow_no_value=True)

    def __init__(self, APELLIDOS):
        self.APELLIDOS = APELLIDOS

        # Conexion NIMArray
        bindir = os.path.abspath(os.path.dirname(__file__))
        self.config_parser.read(bindir + "/cfg.cnf")
        host = self.config_parser.get("DB", "hostname")
        db = self.config_parser.get("DB", "db")
        user = self.config_parser.get("DB", "user")
        password = self.config_parser.get("DB", "password")
        self.db = dbConnect(host, user, password, db)

    def apellidos(self):

        where1 = "APELLIDOS = '%s'" % self.APELLIDOS
        PACIENTES = self.db.select_df('PACIENTES', campos, where1)
        PACIENTES = PACIENTES.apply(lambda row: date2string(row), axis=1)

        where2 = ('IDPACIENTE IN (SELECT IDPACIENTE FROM PACIENTES WHERE APELLIDOS = "%s")' % (self.APELLIDOS))
        PETICIONSCB = self.db.select_df('PACIENTE_PETICION', 'IDPACIENTE, PETICIONCB', where2)

        RESULTADO = pd.merge(PACIENTES, PETICIONSCB, how='outer')
        RESULTADO = RESULTADO[cols]

        return RESULTADO.to_json(orient='records', date_format='iso')