[TOC] 

![NIMARRAY.png](https://bitbucket.org/repo/y8ekpzj/images/2597057887-NIMARRAY.png)

## NIMArray

Conjunto de scripts para manejar la base de datos NIMArray

## Carpeta Tracks
Contiene los traks generados.

## Carpeta Otros
Contiene scripts escritos con pruebas o partes de código utilizados para construir NIMArray.

## Carpeta NIMArrayDashboard

En esta carpeta se guardan los scripts que configuran las búsquedas de dashboard para NIMArray.

- **NIMArray.py:** código principal, pide los datos por terminal de comandos.
- **NIMArrayapellidos.py:** configura la búsqueda por apellidos en NIMArray para dashboard.
- **NIMArraycoord.py:** determina la búsqueda por coordenadas en NIMarray para dashboard.
- **NIMArraypetcb.py:** configura la búsqueda por PETICIONCB en NIMArray para dashboard.
- **NIMArraygenes.py:** configura la búsqueda por genes en NIMArray para dashboard.
- **nimdate.py:** Lleva la función date to string que transforma una fecha a un string.

## Scripts Principales

- **TrackGenerator V4 y V5:** Script que conecta con la base de datos NIMArray y saca un .bed final que se puede cargar en Cytogenomics como Track. Es específico para la versión 4 o 5 de Agilent Cytogenomics.
- **NIMGTFM2014-19:** Este script carga cada excel de la carpeta de Arrays de 2014-19 y genera un documento final, en donde se recopilan todos los casos de la carpeta con el formato definido.
- **AlaBasededatosExcels:** Mdifica los valores en las líneas para que el formato de las mismas sea compatible con SQL.
- **insert_destaca_2_arrays.py:** recupera los excels de destaca_2 y los agrega a la base de datos.