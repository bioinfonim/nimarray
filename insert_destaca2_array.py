# encoding: utf8
# Author: Rafael Navarro
import glob
import fdb
import configparser
import pandas as pd
from dbconnect.dbConnect import dbConnect
import xlrd
import math
from termcolor import colored
import os
from email_nimarray import email_destaca2
import mysql.connector

config_parser = configparser.ConfigParser(allow_no_value=True)
bindir = os.path.abspath(os.path.dirname(__file__))
config_parser.read(bindir + "/cfg.cnf")

#Conexion a bases de datos
host = config_parser.get("DB", "hostname")
db = config_parser.get("DB", "db")
user = config_parser.get("DB", "user")
password = config_parser.get("DB", "password")
dsn_gestlab = config_parser.get("GESTLAB", "dsn")
user_gestlab = config_parser.get("GESTLAB", "user")
password_gestlab = config_parser.get("GESTLAB", "password")

#Paths
#dir_srcRafa = config_parser.get("PATHS", "dir_srcRafa")
dir_src2020 = config_parser.get("PATHS", "dir_src2020")
dir_src2019 = config_parser.get("PATHS", "dir_src2019")
nimarray_logs = config_parser.get("PATHS", "nimarray_logs")
nimarray_destacados_logs = config_parser.get("PATHS", "nimarray_destacados_logs")
excel_insert = config_parser.get("PATHS", "nimarray_logs")
excel_update = config_parser.get("PATHS", "nimarray_logs")
excel_RevisadoUpdate = config_parser.get("PATHS", "nimarray_logs")

# dbArrayCerebro = dbConnect(host, user, password, db)
dbArrayLocal = dbConnect(host, user, password, db)
GestCon = fdb.connect(dsn=dsn_gestlab, user=user_gestlab, password=password_gestlab, charset='ISO8859_1')
connection = mysql.connector.connect(host='bioinfodb.nimgenetics.com',user='nimarray', password='nimarray',database='NIMArray')


#CONSULTA DE DATOS YA SUBIDOS A NIMArray
# connection = mysql.connector.connect(host='bioinfodb.nimgenetics.com', user='nimarray', password='nimarray', database='NIMArray')
#
# select = """
#
#        SELECT DISTINCT(PETICIONCB) FROM NIMArray.VARIANTES_INFORMADAS WHERE Analizado = 'Analizado';
#
#        """
# query_paneles = select
#
# cursor = connection.cursor()
# cursor.execute(query_paneles)
# rows = cursor.fetchall()
# names = [x[0] for x in cursor.description]
# PeticionesSubidas = pd.DataFrame(rows)


ARRAY_CNV_TRIP = 'Triplicación o Superior'
ARRAY_CNV_DUP = 'Duplicación'
ARRAY_CNV_DEL_HET = 'Deleción Heterocigota'
ARRAY_CNV_DEL_HEMI = 'Deleción Hemicigota'
ARRAY_CNV_DEL_HOMO = 'Deleción Homocigota'
ARRAY_CNV_DEL_NOCLAS = 'Deleción no clasificada'
ARRAY_CHRY = 'chrY'

ERRORES = pd.DataFrame()
ERRORES['Fecha'] = ""

ERRORES.loc['NIMArray', 'Fecha'] = pd.to_datetime('today')

ExcelVIDestaca2 = []
ListaCount = []
ListaAdvertencias = []
ListaGraves = []
lista_erroresPacientes = []
lista_erroresPeticiones = []
lista_erroresPacyPet = []
lista_erroresVI = []
DV_todaslist = []
Peticiones = []
ColumnasDF = ['PETICIONCB', 'AberrationNo', 'Chr', 'Cytoband', 'Start', 'Stop', '#Probes', 'Amplification', 'Deletion', 'Analizado',
           'pval','Gene Names', 'Tamaño', 'Categorization', 'Copy Number', 'Coments', 'Coordenadas']
mensaje = ""
nanCount1 = 0
nanCount2 = 0
Count = 0
Counter = 0
CountPacientes = 0
CountFiles = 0
exceptBool = False

def change_to_int(dataframe):
    """
       Returns a datframe with an appropiate format of the columns start and stop. Turns those columns into integers

       :param dataframe: the whole dataframe

       :return: the modified dataframe
       """

    Start = dataframe['Start'].tolist()
    Stop = dataframe['Stop'].tolist()


    # for x in range(len(Start)):
    #     if isinstance(Start[x], str):
    #         Start[x] = int(Start[x].replace(',', ''))
    #     if isinstance(Stop[x], str):
    #         Stop[x] = int(Stop[x].replace(',', ''))
    #     if isinstance(Start[x], float):
    #         Start[x] = int(str(Start[x]).replace('.', ''))
    #     if isinstance(Stop[x], float):
    #         Stop[x] = int(str(Stop[x]).replace('.', ''))

    dataframe.loc[:,('Start')] = Start
    dataframe.loc[:,('Stop')] = Stop

    return dataframe
def exchange_columns(dataframe):
    """
    Returns a datframe with only the columns amplification and deletion which include the information in Gain and Loss

    :param dataframe: the whole dataframe

    :return: the modified dataframe or a message with a warning
    """

    if ('Gain' in dataframe.columns.tolist()) or ('Loss' in dataframe.columns.tolist()):
        Amplification = dataframe['Amplification'].tolist()
        Gain = dataframe['Gain'].tolist()
        Deletion = dataframe['Deletion'].tolist()
        Loss = dataframe['Loss'].tolist()

        for x in range(len(Gain)):
            if (float(Amplification[x]) == 0) and (float(Gain[x]) != 0):
                Amplification[x] = Gain[x]
        for x in range(len(Loss)):
            if (float(Deletion[x]) == 0) and (float(Loss[x]) != 0):
                Deletion[x] = Loss[x]

        dataframe['Amplification'] = Amplification
        dataframe['Deletion'] = Deletion
        del dataframe['Gain']
        del dataframe['Loss']
        # mensaje = path+' Caso con formato del software de análisis antiguo, con columnas Gain y Loss'
        # print(mensaje)
        # ListaAdvertencias.append(mensaje)

        return dataframe
    else:
        return dataframe
def _cnv_value(amplification, deletion, chr):
    """
    Retrieves the copy number value by given amplification and deletion values

    :param amplification: the amplification value
    :param deletion: the deletion value
    :param chr: the chromosome value

    :return: the copy number value
    """
    copy_number = None
    conditions = []
    sentences = []

    if deletion == 0:
        conditions = [amplification >= 1, 0 < amplification < 1]
        sentences = [ARRAY_CNV_TRIP, ARRAY_CNV_DUP]

    elif amplification == 0:
        conditions = [deletion < -2, deletion < -3 and chr == ARRAY_CHRY, -1 <= deletion < 0, -2 <= deletion < -1]
        sentences = [ARRAY_CNV_DEL_HOMO, ARRAY_CNV_DEL_HEMI, ARRAY_CNV_DEL_HET, ARRAY_CNV_DEL_NOCLAS]

    for condition, sentence in zip(conditions, sentences):
        if condition:
            copy_number = sentence
            break

    return copy_number

def list_dir(dir_path):
    #print "Searching in {} ".format(dir_path)
    for f in os.listdir(dir_path):
        full_src_path = os.path.join(dir_path, f)


        if os.path.isdir(full_src_path):
            list_dir(full_src_path)
            all_paths.append(full_src_path)

        elif os.path.splitext(full_src_path)[1] == ".xlsx":
            all_files.append(full_src_path)



def keep_PETICIONCB(lista_path):
    lista_peticiones = list()
    for i in lista_path:
        i = i.split('/')
        try:
            lista_peticiones.append(i[4])
        except:
            continue


    return lista_peticiones

print('Imprimimos la lista de paths')
all_files = list()
all_paths=list()

all_files = [name for name in all_files if "20NR" not in str(name)]

list_dir(dir_src2019)
list_dir(dir_src2020)
len_all_files = len(all_files)

all_paths_pets = keep_PETICIONCB(all_paths)
all_files_pets = keep_PETICIONCB(all_files)

print(all_paths_pets)
print(all_files)

muestras_sin_destacado = [x for x in all_paths_pets if x not in all_files_pets]


for p in muestras_sin_destacado:

    ERRORES.loc[p, 'Fecha'] = pd.to_datetime('today')
    ERRORES.loc[p, 'ERRORES GRAVES']= 'No se ha encontrado archivo destacado'

all_files = [name for name in all_files if "_destaca" in str(name)]
all_files = [name for name in all_files if "~$" not in str(name)]


# creates a list of files from the pointed folder with the files which are going to be uploaded to the database

for path in all_files:
    Counter = Counter + 1
    print(path+' '+str(Counter))

    DV_analizadas = pd.read_excel(path, sheet_name='Final')
    peticion = DV_analizadas["AberrationNo"].values[0][:9]
    print('peticioncb',peticion)



    # Gets PETICIONCN number from file (erases '-2' and other variantions in the name)

    # throws exception if a file can not be opened
    try:
        excel = pd.ExcelFile(path)
    except xlrd.XLRDError:
        mensaje = ' Archivo corrupto o con algún problema en su formato.'
        mesajecolor = colored(mensaje, 'red')
        print(mensajecolor)
        ERRORES.loc[peticion, 'Fecha'] = pd.to_datetime('today')
        ERRORES.loc[peticion,'ERRORES GRAVES'] = mensaje
        continue

    # In case the number is wrong (lacks a 0 in the begining, 1 case in 2019) adds it

    if len(peticion) < 9:
        peticion = peticion[0:4]+'0'+peticion[4:9]
        mensaje = ' Problema con el nombre de la peticion, comprobar que PETICIONCB es correcto, es posible' \
                             'que falte un 0 en PETICIONCB. Se ha añadido automáticamente.'
        mensajecolor = colored(mensaje, 'yellow')
        print(mensajecolor)
        ERRORES.loc[peticion, 'Fecha'] = pd.to_datetime('today')
        ERRORES.loc[peticion,'ADVERTENCIAS'] = mensaje


    # In case the excel has more than 2 sheets writes a warning message, and adds it to ListaAdvertencias

    if len(excel.sheet_names) > 2:
        mensaje = ' El excel tiene más de dos hojas. Comprobar si se añade correctamente a la base de datos.'
        mensajecolor = colored(mensaje, 'yellow')
        print(mensajecolor)
        ERRORES.loc[peticion, 'Fecha'] = pd.to_datetime('today')
        ERRORES.loc[peticion,'ADVERTENCIAS'] = mensaje


    # Query for Peticiones

    select1 = """select p.PETICIONCB, pru.IDPRUEBA, p.IDPETICION, p.IDLABORATORIO, p.IDPACIENTE, p.FECHA_TOMA, p.FECHA_ALTA, 
    p.COMENTARIO, p.IDPROCEDENCIA, pp.TRESULTADO from PETICION p join PET_PRUEBA pp on (p.IDPETICION = pp.IDPETICION)
    join PRUEBA pru on (pp.IDPRUEBA = pru.IDPRUEBA) where pp.IDPRUEBA in (300,85) and p.PETICIONCB = '%s'"""

    query1 = select1 % peticion

    # Query for Pacientes

    select2 = """select pac.IDPACIENTE, pf.CLAVE, pac.APELLIDOS, pac.NOMBRE, pac.FECHA_NACIMIENTO, pac.SEXO,
    (p.FECHA - pac.FECHA_NACIMIENTO)/365 as EDAD, pac.NIF, pac.IDPACIENTEFAMILIA, p.FECHA from PETICION p
    join PACIENTE pac on (p.IDPACIENTE = pac.IDPACIENTE) full outer join PACIENTE_FAMILIA pf on (pac.IDPACIENTEFAMILIA = 
    pf.IDPACIENTEFAMILIA) where p.PETICIONCB = '%s'"""

    query2 = select2 % peticion

    PeticionesTabla = pd.read_sql(query1, GestCon)
    PacientesTabla = pd.read_sql(query2, GestCon)
    PacientesTabla = PacientesTabla.drop_duplicates(subset="IDPACIENTE")
    PacyPet = PeticionesTabla[['PETICIONCB', 'IDPACIENTE']]

    #     ###########################################################################################################
    #                  ## UPLOADS DATA TO THE TABLE PACIENTES ###
    #     ###########################################################################################################

    PacientesTabla = PacientesTabla.replace({'': 'NULL'})
    PacientesTabla = PacientesTabla.replace({' ': 'NULL'})
    PacientesTabla = PacientesTabla.replace({'-': 'NULL'})
    PacientesTabla = PacientesTabla.replace({None: "NULL"})
    PacientesTabla = PacientesTabla.replace({'*': "NULL"})
    PacientesTabla = PacientesTabla.replace({'"': ''})
    PacientesTabla = PacientesTabla.replace({"'": ''})

    # Checks if data from GestLab has been gathered, if not writes a warning message and adds it to ListaGraves

    if PacientesTabla.empty:
        mensaje = ' Problema al buscar el paciente en GestLab. No se incluye nada en la BBDD. Comproar si ' \
                           'PETICIONCB es correcto.'
        mensajecolor = colored(mensaje, 'red')
        print(mensajecolor)
        ERRORES.loc[peticion, 'Fecha'] = pd.to_datetime('today')
        ERRORES.loc[peticion,'ERRORES GRAVES'] = mensaje

        continue
    else:
        values = tuple(PacientesTabla.values.tolist()[0])

    if len(PacientesTabla) > 1:
        print(PacientesTabla)
        mensaje = ' Varios pacientes con la misma PETICIONCB'
        mensajecolor = colored(mensaje, 'red')
        print(mensaje)
        ERRORES.loc[peticion, 'Fecha'] = pd.to_datetime('today')
        ERRORES.loc[peticion,'ERRORES GRAVES'] = mensaje

        continue

    IDPACIENTE = values[0]
    if IDPACIENTE != "NULL":
        IDPACIENTE = "'" + str(IDPACIENTE) + "'"
    IDEXTERNO = values[1]
    if IDEXTERNO != "NULL":
        IDEXTERNO = "'" + IDEXTERNO + "'"
    APELLIDOS = values[2]
    if APELLIDOS != "NULL":
        APELLIDOS = "'" + str(APELLIDOS) + "'"
    NOMBRE = values[3]
    if NOMBRE != "NULL":
        NOMBRE = "'" + str(NOMBRE) + "'"
    FECHA_NACIMIENTO = values[4]
    if FECHA_NACIMIENTO != "NULL":
        FECHA_NACIMIENTO = "'" + str(FECHA_NACIMIENTO) + "'"
    SEXO = values[5]
    if SEXO != "NULL":
        SEXO = "'" + SEXO + "'"
    EDAD = values[6]
    if EDAD != "NULL":
        EDAD = "'" + str(EDAD) + "'"
    NIF = values[7]
    if NIF != "NULL":
        NIF = "'" + NIF + "'"
    idFAMILIA = values[8]
    if idFAMILIA != "NULL":
        idFAMILIA = "'" + str(idFAMILIA) + "'"
    FECHA_RECEPCION = values[9]
    if FECHA_RECEPCION != "NULL":
        FECHA_RECEPCION = "'" + str(FECHA_RECEPCION) + "'"

    where = ("IDPACIENTE = %s, APELLIDOS = %s, NOMBRE = %s" % (IDPACIENTE, APELLIDOS, NOMBRE))
    fields = (
    'IDPACIENTE', 'IDEXTERNO', 'APELLIDOS', 'NOMBRE', 'NIF', 'SEXO', 'EDAD', 'FECHA_NACIMIENTO', 'FECHA_RECEPCION',
    'idFAMILIA')

    value_list = "(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)" % (IDPACIENTE, IDEXTERNO, APELLIDOS, NOMBRE, NIF,
                                                      SEXO, EDAD, FECHA_NACIMIENTO, FECHA_RECEPCION, idFAMILIA)

    insert = dbArrayLocal.insert_update('PACIENTES', fields, value_list, where)
    CountPacientes = CountPacientes + 1

    dbArrayLocal.commit()
    print(str(insert) + ' Insert en Pacientes')
    if insert == -1:
        ERRORES.loc[peticion, 'Fecha'] = pd.to_datetime('today')
        ERRORES.loc[peticion,'PACIENTES'] = IDPACIENTE

    dbArrayLocal.commit()

    #     ###########################################################################################################
    #                  ## UPLOADS DATA TO THE TABLE PETICIONES ###
    #     ###########################################################################################################

    PeticionesTabla = PeticionesTabla.replace({'': 'NULL'})
    PeticionesTabla = PeticionesTabla.replace({' ': 'NULL'})
    PeticionesTabla = PeticionesTabla.replace({'-': 'NULL'})
    PeticionesTabla = PeticionesTabla.replace({None: "NULL"})
    PeticionesTabla = PeticionesTabla.replace({'*': "NULL"})
    PeticionesTabla = PeticionesTabla.replace({'"': ''})
    PeticionesTabla = PeticionesTabla.replace({"'": ''})

    # Checks if data from GestLab has been gathered, if not writes a warning message and adds it to ListaGraves

    if PeticionesTabla.empty:
        mensaje = path + ' Problema al buscar la peticion en GestLab. Habitualmente no incluye IDPRUEBA 300 o 85. Se incluye el paciente' \
                 ' pero no la peticion ni sus alteraciones en la BBDD'
        mensajecolor = colored(mensaje,'red')
        print(mensajecolor)
        ERRORES.loc[peticion, 'Fecha'] = pd.to_datetime('today')
        ERRORES.loc[peticion,'ERRORES GRAVES'] = mensaje

        continue
    else:
        values = tuple(PeticionesTabla.values.tolist()[0])

    PETICIONCB = values[0]
    if PETICIONCB != "NULL":
        PETICIONCB = "'" + PETICIONCB + "'"
    IDPRUEBA = values[1]
    if IDPRUEBA != "NULL":
        IDPRUEBA = "'" + str(IDPRUEBA) + "'"
    IDPETICION = values[2]
    if IDPETICION != "NULL":
        IDPETICION = "'" + str(IDPETICION) + "'"
    IDLABEXTERNO = values[3]
    if IDLABEXTERNO != "NULL":
        IDLABEXTERNO = "'" + str(IDLABEXTERNO) + "'"
    IDPACIENTE = values[4]
    if IDPACIENTE != "NULL":
        IDPACIENTE = "'" + str(IDPACIENTE) + "'"
    FECHA_TOMA = values[5]
    if FECHA_TOMA != "NULL":
        FECHA_TOMA = "'" + str(FECHA_TOMA) + "'"
    FECHA_ALTA = values[6]
    if FECHA_ALTA != "NULL":
        FECHA_ALTA = "'" + str(FECHA_ALTA) + "'"
    COMENTARIO = values[7]
    if COMENTARIO != "NULL":
        COMENTARIO = COMENTARIO.replace("'", "")
        COMENTARIO = COMENTARIO.replace("\n", "")
        COMENTARIO = COMENTARIO.replace("\r", "")  # Avoids SQL problems when uploading to the database.
        COMENTARIO = "'" + str(COMENTARIO) + "'"
    PROCEDENCIAid = values[8]
    if PROCEDENCIAid != "NULL":
        PROCEDENCIAid = "'" + str(PROCEDENCIAid) + "'"
    TRESULTADO = values[9]
    if TRESULTADO != "NULL":
        TRESULTADO = TRESULTADO.replace("'", "")
        TRESULTADO = TRESULTADO.replace("\n", "")
        TRESULTADO = TRESULTADO.replace("\r", "")
        TRESULTADO = "'" + TRESULTADO + "'"

    where = ("PETICIONCB = %s, IDPETICION = %s, FECHA_TOMA =%s, FECHA_ALTA = %s" % (
        PETICIONCB, IDPETICION, FECHA_TOMA, FECHA_ALTA))
    fields = (
    'PETICIONCB', 'IDPRUEBA', 'IDPETICION', 'IDLABEXTERNO', 'IDPACIENTE', 'FECHA_TOMA', 'FECHA_ALTA', 'COMENTARIO',
    'PROCEDENCIAid', 'TRESULTADO')

    value_list = "(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)" % (
        PETICIONCB, IDPRUEBA, IDPETICION, IDLABEXTERNO, IDPACIENTE, FECHA_TOMA, FECHA_ALTA, COMENTARIO, PROCEDENCIAid,
        TRESULTADO)

    insert = dbArrayLocal.insert_update('PETICIONES', fields, value_list, where)
    Count = Count + 1

    dbArrayLocal.commit()
    print(str(insert) + ' Insert en Peticiones')
    if insert == -1:
        ERRORES.loc[peticion, 'Fecha'] = pd.to_datetime('today')
        ERRORES.loc[peticion,'PETICIONES'] = PETICIONCB


    dbArrayLocal.commit()

    #     ###########################################################################################################
    #                  ## UPLOADS DATA TO THE TABLE PACIENTE_PETICION ###
    #     ###########################################################################################################

    PacyPet = PacyPet.replace({'': 'NULL'})
    PacyPet = PacyPet.replace({' ': 'NULL'})
    PacyPet = PacyPet.replace({'-': 'NULL'})
    PacyPet = PacyPet.replace({None: "NULL"})
    PacyPet = PacyPet.replace({'*': "NULL"})
    PacyPet = PacyPet.replace({'"': ''})
    PacyPet = PacyPet.replace({"'": ''})

    values = tuple(PacyPet.values.tolist()[0])

    PETICIONCB = values[0]
    if PETICIONCB != "NULL":
        PETICIONCB = "'" + PETICIONCB + "'"
    IDPACIENTE = values[1]
    if IDPACIENTE != "NULL":
        IDPACIENTE = "'" + str(IDPACIENTE) + "'"

    where = ("PETICIONCB = %s, IDPACIENTE = %s" % (PETICIONCB, IDPACIENTE))
    fields = ('PETICIONCB', 'IDPACIENTE')

    value_list = "(%s,%s)" % (PETICIONCB, IDPACIENTE)

    insert = dbArrayLocal.insert_update('PACIENTE_PETICION', fields, value_list, where)
    Count = Count + 1

    dbArrayLocal.commit()
    print(str(insert) + ' Insert en Paciente peticion')
    if insert == -1:
        ERRORES.loc[peticion, 'Fecha'] = pd.to_datetime('today')
        ERRORES.loc[peticion, 'PETICIONES'] = PETICIONCB

    dbArrayLocal.commit()

    #################################### STARTS WITH THE TABLE VARIANTES_INFORMADAS #########################################


    nanCount1 = 0
    DV_analizadas = DV_analizadas.drop(0, axis=0)

    # Checks if the structure of the Final sheet, analysed results, is correct. In case is not, modifies the sheet,
    # sets it up appropiately, writes a warning message and adds it to Lista advertencias.

    for value in DV_analizadas['AberrationNo'].values.tolist():
        if (not (isinstance(value, str)) and (math.isnan(value))):
            nanCount1 = nanCount1 + 1
    if nanCount1 != 0:
        row1 = len(DV_analizadas['AberrationNo'].values.tolist())-nanCount1+1
        row2 = len(DV_analizadas['AberrationNo'].values.tolist())+1
        DV_analizadas = DV_analizadas.drop(range(row1, row2), axis=0)
        mensaje = ' Este archivo tiene algún caracter en la hoja final fuera de la estructura de destacados; ' \
                       'Comprobar que no halla erores al incluir sus alteraciones en Variantes Informadas.'
        mensajecolor = colored(mensaje,'yellow')
        print(mensajecolor)
        ERRORES.loc[peticion, 'Fecha'] = pd.to_datetime('today')
        ERRORES.loc[peticion, 'ADVERTENCIAS'] = mensaje

    # Loads the second sheet, raw data in DV_noanalizadas. In case of erros tries different options and writes
    # warning messages depending on the error.

    try:
        DV_noanalizadas = pd.read_excel(path, sheet_name=peticion)
    except xlrd.XLRDError:
        try:
            DV_noanalizadas = pd.read_excel(path, sheet_name=peticion)
        except xlrd.XLRDError:
            try:
                DV_noanalizadas = pd.read_excel(path, sheet_name=1)
                mensaje = ' Archivo con el Nombre del caso mal de alguna forma. Comprobar que el nombre del archivo ' \
                                 'coincide con el nombre en las hojas; Se ha incluido en la BBDD con el nombre en el interior de las hojas.'
                mensajecolor = colored(mensaje, 'yellow')
                print(mensajecolor)
                #ERRORES.loc[peticion, 'Fecha'] = pd.to_datetime('today')
                #ERRORES.loc[peticion, 'ADVERTENCIAS'] = mensaje

            except xlrd.XLRDError:
                mensaje = 'Archivo con el Nombre del caso mal de alguna forma. Comprobar que el nombre del archivo ' \
                               'coincide con el nombre en las hojas; Sus alteraciones no se incluyen en la BBDD.'
                mensajecolor = colored(mensaje,'red')
                print(mensajecolor)
                ERRORES.loc[peticion, 'Fecha'] = pd.to_datetime('today')
                ERRORES.loc[peticion, 'ERRORES GRAVES'] = mensaje

                continue

    # Sets up the format of the raw data sheet as required. In case there is some error in the sheet, writes a warning message
    # and adds it to ListaAdvertencias or ListaGraves depending on the error.

    row1 = 0
    nanCount2 = 0
    row2 = DV_noanalizadas.loc[DV_noanalizadas[DV_noanalizadas.columns.tolist()[0]] == 'AberrationNo'].index[0]
    DV_noanalizadas = DV_noanalizadas.drop(range(row1, row2), axis=0)
    DV_noanalizadas = DV_noanalizadas.reset_index()
    DV_noanalizadas = DV_noanalizadas.drop('index', axis=1)
    colUsed = DV_noanalizadas.iloc[0].tolist()
    DV_noanalizadas.columns = colUsed
    DV_noanalizadas = DV_noanalizadas.drop(0, axis=0)
    for value in DV_noanalizadas['Chr'].values.tolist():
        if (not(isinstance(value, str))and(math.isnan(value))):
            nanCount2 = nanCount2+1
    if nanCount2 > 5:
        DV_noanalizadas = DV_noanalizadas.reset_index()
        DV_noanalizadas = DV_noanalizadas.drop('index', axis=1)
        row1 = 0
        row2 = DV_noanalizadas.loc[DV_noanalizadas[DV_noanalizadas.columns.tolist()[0]] == peticion].index[0]
        DV_noanalizadas = DV_noanalizadas.drop(range(row1, row2), axis=0)
        row1 = DV_noanalizadas.loc[DV_noanalizadas[DV_noanalizadas.columns.tolist()[0]] == 'Number of calls per sample:'].index[0]
        DV_noanalizadas = DV_noanalizadas.drop(row1+1, axis=0)
        DV_noanalizadas = DV_noanalizadas.reset_index()
        DV_noanalizadas = DV_noanalizadas.drop('index', axis=1)
        mensaje = ' Archivo con algún problema en la hoja de raw data; Sus alteraciones se incluyen en la BBDD.'
        mensajecolor = colored(mensaje,'yellow')
        print(mensajecolor)
        ERRORES.loc[peticion, 'Fecha'] = pd.to_datetime('today')
        ERRORES.loc[peticion, 'ADVERTENCIAS'] = mensaje

    elif nanCount2 != 5:
        mensaje = ' Archivo con algún problema en la hoja de raw data; Sus alteraciones no se incluyen en la BBDD.'
        mensajecolor = colored(mensaje,'red')
        print(mensajecolor)
        ERRORES.loc[peticion, 'Fecha'] = pd.to_datetime('today')
        ERRORES.loc[peticion, 'ERRORES GRAVES'] = mensaje

        continue
    row1 = DV_noanalizadas.loc[DV_noanalizadas[DV_noanalizadas.columns.tolist()[0]] == 'Number of calls per sample:'].index[0]-2
    row2 = DV_noanalizadas.loc[DV_noanalizadas[DV_noanalizadas.columns.tolist()[0]] == 'Number of calls per sample:'].index[0]+2
    DV_noanalizadas = DV_noanalizadas.drop(range(row1, row2), axis=0)
    DV_noanalizadas = DV_noanalizadas.reset_index()
    DV_noanalizadas = DV_noanalizadas.drop('index', axis=1)
    DV_noanalizadas.columns = colUsed
    DV_noanalizadas = exchange_columns(DV_noanalizadas)
    DV_analizadas = exchange_columns(DV_analizadas)
    DV_noanalizadas = DV_noanalizadas.drop(0, axis=0)

    # Merges raw data and analysed data in a single dataframe. Goes over the raw data erasing the analysed variants.
    # This is done because for the case the columns GAIN and LOSS are present, problems appeared with the merge function.

    if not DV_analizadas.empty:
        DV_analizadas.insert(8, 'Analizado', 'Analizado')
        DV_analizadas = DV_analizadas.astype(object)
        DV_noanalizadas = DV_noanalizadas.astype(object)
        for value in DV_analizadas['AberrationNo'].tolist():
            if value in DV_noanalizadas['AberrationNo'].tolist():
                rowindex = DV_noanalizadas.index[DV_noanalizadas['AberrationNo'] == value].tolist()[0]
                DV_noanalizadas = DV_noanalizadas.drop(rowindex, axis=0)
        DV_todasMerged = pd.merge(DV_analizadas, DV_noanalizadas, how='outer')
        DV_todasMerged.insert(0, 'PETICIONCB', peticion)
        DV_todas = DV_todasMerged[ColumnasDF]
    else:
        DV_todas = DV_noanalizadas
        DV_todas.insert(0, 'PETICIONCB', peticion)
        DV_todas.insert(9, 'Analizado', 'NULL')
        DV_todas.insert(12, 'Tamaño', float('nan'))
        DV_todas.insert(13, 'Categorization', 'NULL')
        DV_todas.insert(14, 'Copy Number', float('nan'))
        DV_todas.insert(15, 'Coments', 'NULL')
        DV_todas.insert(16, 'Coordenadas', float('nan'))

    DV_todas = change_to_int(DV_todas) # transform data type to int. Problems if not.

    DV_todaslist = []
    columnas = DV_todas.columns.tolist()

    # Adds Copy Number and coordinates to the row in case the file is not empty.

    if not DV_todas.empty:
        for row in DV_todas.values.tolist():
            row[14] = _cnv_value(row[7], row[8], row[2])
            row[16] = str(row[2]) + ':' + str(row[4]) + '-' + str(row[5])
            DV_todaslist.append(row)
        DV_todas = pd.DataFrame(DV_todaslist)
        DV_todas.columns = columnas
    else:
        mensaje = ' archivo vacío. Se incluye el paciente y la pecticion en la BBDD pero no sus alteraciones. No tiene.'
        mensajecolor = colored(mensaje, 'yellow')
        print(mensajecolor)
        #ERRORES.loc[peticion, 'Fecha'] = pd.to_datetime('today')
        #ERRORES.loc[peticion, 'ADVERTENCIAS'] = mensaje

        continue

    # Retrieves dates and test type data from GestLab and adds it to the Database

    select = """SELECT p.FECHA_REGISTRO, p.FECHA_RESULTADO, pp.IDPRUEBA_LISTA from PETICION p
    join PET_PRUEBA pp on (p.IDPETICION = pp.IDPETICION) where pp.IDPRUEBA in (300,85) and p.PETICIONCB = '%s';"""

    query = select % peticion
    query_df = pd.read_sql(query, GestCon)

    if not query_df.empty:
        DV_todas['FECHA_REGISTRO'] = query_df['FECHA_REGISTRO'][0]
        DV_todas['FECHA_RESULTADO'] = query_df['FECHA_RESULTADO'][0]
        DV_todas['TestType'] = query_df['IDPRUEBA_LISTA'][0]
    else:
        DV_todas['FECHA_REGISTRO'] = ''
        DV_todas['FECHA_RESULTADO'] = ''
        DV_todas['TestType'] = ''

    DV_todas = DV_todas.sort_values(by=['AberrationNo'])

    delete = """
    DELETE FROM VARIANTES_INFORMADAS where PETICIONCB = '%s';
    """

    query_array = delete % peticion
    print(query_array)
    cur = connection.cursor()
    cur.execute(query_array)
    print('Eliminadas variantes previas')
    connection.commit()



    #     ###########################################################################################################
    #                  ## UPLOADS DATA TO THE TABLE VARIANTES_INFORMADAS ###
    #     ###########################################################################################################

    lista_errores = []
    DV_todas = DV_todas.replace({'': 'NULL'})
    DV_todas = DV_todas.replace({' ': 'NULL'})
    DV_todas = DV_todas.replace({'-': 'NULL'})
    DV_todas = DV_todas.replace({None: "NULL"})
    DV_todas = DV_todas.replace({'"': ''})
    DV_todas = DV_todas.replace({"'": ''})

    for row in DV_todas.values.tolist():

        values = row
        values = tuple(values)

        # Checks if an analysed aberration is categorized

        if (values[12] == 'Analizado') and (values[14] == 'NULL'):
            mensaje = ' Caso con alteración analizada sin categorizar'
            mensajecolor = colored(mensaje, 'red')
            print(mensajecolor)
            ERRORES.loc[peticion, 'Fecha'] = pd.to_datetime('today')
            ERRORES.loc[peticion, 'ERRORES GRAVES'] = mensaje
            continue

        PETICIONCB = values[0]
        if PETICIONCB != "NULL":
            PETICIONCB = "'" + PETICIONCB + "'"
        AberrationNo = values[1]
        if AberrationNo != "NULL":
            AberrationNo = "'" + str(AberrationNo) + "'"
        Chr = values[2]
        if Chr != "NULL":
            Chr = "'" + Chr + "'"
        Cytoband = values[3]
        if Cytoband != "NULL":
            Cytoband = "'" + Cytoband + "'"
        try:
            PositionStart = int(values[4])
        except:
            PositionStart = PositionStart.replace(',','')
            #PositionStart = int(PositionStart)
        print(PositionStart)
        if PositionStart != "NULL":
            PositionStart = "'" + str(PositionStart) + "'"
        try:
            PositionStop = int(values[5])
        except:
            PositionStop = PositionStop.replace(',', '')
        print(PositionStop)
        if PositionStop != "NULL":
            PositionStop = "'" + str(PositionStop) + "'"
        ProbesNum = values[6]
        if ProbesNum != "NULL":
            ProbesNum = "'" + str(ProbesNum) + "'"
        Amplification = values[7]
        if Amplification != "NULL":
            Amplification = "'" + str(Amplification) + "'"
        Deletion = values[8]
        if Deletion != "NULL":
            Deletion = "'" + str(Deletion) + "'"
        Analizado = values[9]
        if Analizado != "NULL":
            Analizado = "'" + Analizado + "'"
        pval = values[10]
        if pval != "NULL":
            if isinstance(pval, str):
                pval = pval.replace(',', '.')
                pval = float(pval)
            pval = "'" + str(pval) + "'"
        GeneNames = values[11]
        if GeneNames != "NULL":
            GeneNames = "'" + GeneNames + "'"
        Tamano = values[12]
        if Tamano != "NULL":
            Tamano = "'" + str(Tamano) + "'"
        if values[13] == 'Heredada':
            values = list(values)
            values[13] = 'Heredado'
            values = tuple(values)
        Categorization = values[13]
        if Categorization != "NULL":
            Categorization = "'" + Categorization + "'"
        if values[14] == 'Delección Homocigota':
            values = list(values)
            values[14] = 'Deleción Homocigota'
            values = tuple(values)
        if values[14] == 'Delección Heterocigota':
            values = list(values)
            values[14] = 'Deleción Homocigota'
            values = tuple(values)
        if values[14] == 'Delección Homo':
            values = list(values)
            values[14] = 'Deleción Homocigota'
            values = tuple(values)
        CopyNumber = values[14]
        if CopyNumber != "NULL":
            CopyNumber = "'" + str(CopyNumber) + "'"
        Coments = values[15]
        if Coments != "NULL":
            Coments = Coments.replace("'", "")  # Avoids SQL problems when uploading to the database.
            Coments = Coments.replace("\n", "")
            Coments = Coments.replace("\r", "")
            Coments = "'" + str(Coments) + "'"
        Coordenadas = values[16]
        if Coordenadas != "NULL":
            Coordenadas = "'" + Coordenadas + "'"
        FECHA_REGISTRO = values[17]
        if FECHA_REGISTRO != "NULL":
            FECHA_REGISTRO = "'" + str(FECHA_REGISTRO) + "'"
        FECHA_RESULTADO = values[18]
        if FECHA_RESULTADO != "NULL":
            FECHA_RESULTADO = "'" + str(FECHA_RESULTADO) + "'"
        TestType = values[19]
        if TestType != 'NULL':
            TestType = "'" + str(TestType) + "'"


        where = ("PETICIONCB = %s, AberrationNo = %s, FECHA_REGISTRO =%s, FECHA_RESULTADO = %s" % (
            PETICIONCB, AberrationNo, FECHA_REGISTRO, FECHA_RESULTADO))
        # whereUpdate = ("Categorization = %s, CopyNumber = %s, Coments =%s" % (Categorization, CopyNumber, Coments))

        fields = (
        'PETICIONCB', 'AberrationNo', 'Chr', 'Cytoband', 'PositionStart', 'PositionStop', 'ProbesNum', 'Amplification',
        'Deletion', 'pval', 'GeneNames', 'Analizado', 'Tamano', 'Categorization', 'CopyNumber', 'Coments',
        'Coordenadas','FECHA_REGISTRO', 'FECHA_RESULTADO', 'TestType')

        value_list = "(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s, %s)" % (
            PETICIONCB, AberrationNo, Chr, Cytoband, PositionStart, PositionStop, ProbesNum, Amplification,
            Deletion, pval, GeneNames, Analizado, Tamano, Categorization, CopyNumber, Coments, Coordenadas,
            FECHA_REGISTRO, FECHA_RESULTADO, TestType)

        insert = dbArrayLocal.insert_update('VARIANTES_INFORMADAS', fields, value_list, where)
        Count = Count + 1

        dbArrayLocal.commit()
        print(str(insert)+' Insert en Variantes Informadas')
        if insert == -1:
            ERRORES.loc[peticion, 'Fecha'] = pd.to_datetime('today')
            ERRORES.loc[peticion, 'ERRORES'] = mensaje


        dbArrayLocal.commit()
############################################## DELIVER ERROR LISTS ################################################

ERRORES.to_csv(nimarray_destacados_logs, mode='a')
email_destaca2(ERRORES.to_html())
