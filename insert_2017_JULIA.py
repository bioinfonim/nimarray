import numpy as np
import fdb
import pandas as pd
import glob
import sys,os
import configparser
import mysql.connector
from datetime import datetime
from dbconnect.dbConnect import dbConnect
#from consultas import consultas as cons

cnvs = pd.read_excel('/Users/jgonzalez/Desktop/Instert_NIMArray/limpiado.xls','Sheet1')

connection = mysql.connector.connect(host='bioinfodb.nimgenetics.com',user='nimarray', password='nimarray',database='NIMArray')
config_parser = configparser.ConfigParser(allow_no_value=True)
bindir = os.path.abspath(os.path.dirname(__file__))
config_parser.read(bindir + "/cfg.cnf")

#Conexion a bases de datos
host = config_parser.get("DB", "hostname")
db = config_parser.get("DB", "db")
user = config_parser.get("DB", "user")
password = config_parser.get("DB", "password")
dsn_gestlab = config_parser.get("GESTLAB", "dsn")
user_gestlab = config_parser.get("GESTLAB", "user")
password_gestlab = config_parser.get("GESTLAB", "password")

cnvs = cnvs.astype(object).replace(np.nan, 'NULL')
cnvs = cnvs.astype(object).replace('Problamente benigna','Probablemente Benigna')
cnvs = cnvs.astype(object).replace('Probablrmente benigna','Probablemente Benigna')
cnvs = cnvs.astype(object).replace('probablmente benigna','Probablemente Benigna')
cnvs = cnvs.astype(object).replace('Probabemente patogénica', 'Probablemente Patogénica')
cnvs = cnvs.astype(object).replace('Probablente patogénica', 'Probablemente Patogénica')
cnvs = cnvs.astype(object).replace("'", '')
cnvs = cnvs.astype(object).replace('Basural-No real','Basura-No real')
cnvs = cnvs.astype(object).replace('Basurilla-No real','Basura-No real')


for row in cnvs.values.tolist():
    values = row
    values = tuple(values)

    PETICIONCB = values[0]
    print (PETICIONCB)
    AberrationNo = values[1]
    Chr = values[2]
    Cytoband = values[3]
    PositionStart = values[4]
    PositionStop = values[5]
    ProbesNum = values[6]
    Amplification = values[7]
    Deletion = values[8]
    pval = values[9]
    GeneNames = values[10]
    Analizado = values[11]
    print(Analizado)
    Tamano = values[12]
    print(Tamano)
    Categorization = values[13]
    Categorization = Categorization.strip()
    if Categorization != 'NULL':
        Categorization = "'" + Categorization + "'"
    print(Categorization)
    CopyNumber = values[14]
    print(CopyNumber)
    Coments = values[15]
    Coordenadas = values[16]
    TRESULTADO = values[17]
    FECHA_REGISTRO = values[18]
    FECHA_RESULTADO = values[19]

    update = """

    UPDATE NIMArray.VARIANTES_INFORMADAS
    SET Analizado = %s, Categorization = %s, Coments = '%s'
    WHERE PETICIONCB = '%s' and PositionStart = %s and PositionStop = %s

    """
    query_exons = update % (Analizado, Categorization,Coments,PETICIONCB, PositionStart, PositionStop)
    cursor = connection.cursor()
    cursor.execute(query_exons)
    connection.commit()