# This script analyses the excel loaded in Doc, and adds the coordinates and the Copy number if it is lacking.
# These fields are also calculated when adding the alterations from the excels or inserting the destaca_2 cases.

import pandas as pd
import fdb
import os
import configparser

ARRAY_CNV_TRIP = 'Triplicación o Superior'
ARRAY_CNV_DUP = 'Duplicación'
ARRAY_CNV_DEL_HET = 'Deleción Heterocigota'
ARRAY_CNV_DEL_HEMI = 'Deleción Hemicigota'
ARRAY_CNV_DEL_HOMO = 'Deleción Homocigota'
ARRAY_CNV_DEL_NOCLAS = 'Deleción no clasificada'
ARRAY_CHRY = 'chrY'

config_parser = configparser.ConfigParser(allow_no_value=True)
bindir = os.path.abspath(os.path.dirname(__file__))
config_parser.read(bindir + "/cfg.cnf")

# This function determines the copy number to add depending on the values of chromosome, amplification and deletion
def _cnv_value(amplification, deletion, chr):
    """
    Retrieves the copy number value by given amplification and deletion values

    :param amplification: the amplification value
    :param deletion: the deletion value
    :param chr: the chromosome value

    :return: the copy number value
    """
    copy_number = None
    conditions = []
    sentences = []

    if deletion == 0:
        conditions = [amplification >= 1, 0 < amplification < 1]
        sentences = [ARRAY_CNV_TRIP, ARRAY_CNV_DUP]

    elif amplification == 0:
        conditions = [deletion < -2, deletion < -3 and chr == ARRAY_CHRY, -1 <= deletion < 0, -2 <= deletion < -1]
        sentences = [ARRAY_CNV_DEL_HOMO, ARRAY_CNV_DEL_HEMI, ARRAY_CNV_DEL_HET, ARRAY_CNV_DEL_NOCLAS]

    for condition, sentence in zip(conditions, sentences):
        if condition:
            copy_number = sentence
            break

    return copy_number

Columnas = ['Index', 'PETICIONCB', 'AberrationNo', 'Chr', 'Cytoband', 'PositionStart', 'PositionStop',
                           'ProbesNum', 'Amplification', 'Deletion', 'pval', 'GeneNames', 'Analizado', 'Tamano',
                           'Categorization', 'CopyNumber','Coments', 'Coordenadas', 'Gain', 'Loss', 'TRESULTADO', 'FECHA_REGISTRO',
                           'FECHA_RESULTADO']

# Connection to the database
host = config_parser.get("DB", "hostname")
db = config_parser.get("DB", "db")
user = config_parser.get("DB", "user")
password = config_parser.get("DB", "password")
localhost = config_parser.get("LOCAL DB", "hostname")
localdb = config_parser.get("LOCAL DB", "db")
localuser = config_parser.get("LOCAL DB", "user")
localPassword = config_parser.get("LOCAL DB", "password")
dsn_gestlab = config_parser.get("GESTLAB", "dsn")
user_gestlab = config_parser.get("GESTLAB", "user")
password_gestlab = config_parser.get("GESTLAB", "password")

GestCon = fdb.connect(dsn=dsn_gestlab, user=user_gestlab, password=password_gestlab, charset='ISO8859_1')

# reads the excel to modify
Doc = pd.read_excel("/Users/rnavarro/Desktop/BBDD/Viejos/UpdateSQLViejo.xls", 'Sheet1')

Peticiones = Doc['PETICIONCB']

ListaDoc = Doc.values.tolist()

NuevoDoc = []

# Recorre el documento y utiliza las funcioenes definidas antes para determinar el Copy Number las Coordenadas y eliminar
# las columnas gain y loss, que se crearon al montar el excel. Se eleminan porque las columnas Aplification y Deletion
# contienen la misma información.

for row in ListaDoc:

    select = """SELECT p.FECHA_REGISTRO, p.FECHA_RESULTADO from PETICION p join PET_PRUEBA pp on (p.IDPETICION = pp.IDPETICION)
    where pp.IDPRUEBA in (300,85) and p.PETICIONCB = '%s';"""
    query = select % row[1]
    query_df = pd.read_sql(query, GestCon)

    checkZeros = ((row[8] == 0) and (row[9] == 0))
    if checkZeros:
        row[8] = row[18]
        if row[19] > 0:
            row[9] = -row[19]
        else:
            row[9] = row[19]

    if row[8] > 100000:
        row[8] = row[8]/1000000
    if row[9] > 100000:
        row[9] = row[9]/1000000

    row[17] = str(row[3])+': '+str(row[5])+'-'+str(row[6])

    if str(row[15]) == 'nan':
        row[15] = _cnv_value(row[8], row[9], row[3])
    row[13] = abs(row[5]-row[6])/1000000

    if not query_df.empty:
        row[21] = query_df['FECHA_REGISTRO'][0]
        row[22] = query_df['FECHA_RESULTADO'][0]

    NuevoDoc.append(row)

# Devuelve el documento modificado

df = pd.DataFrame(NuevoDoc, columns=Columnas)
del df['Gain']
del df['Loss']
df.to_excel("/Users/rnavarro/Desktop/BBDD/UpdateSQL.xls", index=False)
