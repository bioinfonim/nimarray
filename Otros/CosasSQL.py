import fdb
import pandas as pd
from dbconnect.dbConnect import dbConnect
import os
import configparser


peticiones = []
peticionesTodo = []
Counter = 0
count2 = 0
count = 0

dbArrayLocal = dbConnect('LocalHost', 'root', 'Password.10', 'NIMArray')
dbArrayCerebro = dbConnect('bioinfodb.nimgenetics.com', 'nimarray', 'nimarray', 'NIMArray')
pac = dbArrayCerebro .select_df_simple('PACIENTES')
pet = dbArrayCerebro .select_df_simple('PETICIONES')
pacypet = dbArrayCerebro .select_df_simple('PACIENTE_PETICION')

path = '/Users/rnavarro/Desktop/Resultados/ConcatFiles/Todos Juntos/UnidosFormatoBueno.xls'
Excel = pd.read_excel(path, 0)

ExcelIDpeticiones = list(set(Excel['PETICIONCB'].values.tolist()))

IDsPacientes = pac['IDPACIENTE'].values.tolist()

config_parser = configparser.ConfigParser(allow_no_value=True)
bindir = os.path.abspath(os.path.dirname(__file__))
config_parser.read(bindir + "/cfg.cnf")

def list_dir(dir_path):
    #print "Searching in {} ".format(dir_path)
    for f in os.listdir(dir_path):
        full_src_path = os.path.join(dir_path, f)


        if os.path.isdir(full_src_path):
            list_dir(full_src_path)

        elif os.path.splitext(full_src_path)[1] == ".xlsx":
            all_files.append(full_src_path)

dir_srcRafa = config_parser.get("PATHS", "dir_srcRafa")
print('Imprimimos la lista de paths')
all_files = list()
list_dir(dir_srcRafa)
len_all_files = len(all_files)

print('imprimo_all_files')
all_files = [name for name in all_files if "destaca2" in str(name)]
all_files = [name for name in all_files if "~$" not in str(name)]
print(all_files)

for path in all_files:
    Counter = Counter + 1
    print(path+' '+str(Counter))

    DV_analizadas = pd.read_excel(path, sheet_name='Final')
    peticion = DV_analizadas["AberrationNo"].values[0][:9]
    peticiones.append(peticion)
    peticionTodo = DV_analizadas["AberrationNo"].values[0]
    peticionesTodo.append(peticion)


Genes = 'NIPA, GOLGA, ADAM'
listagenes = Genes
listagenes = str(listagenes)
listagenes = listagenes.split(sep = ',')

petLocal = dbArrayLocal.select_df_simple('PETICIONES')
petCerebro = dbArrayCerebro.select_df_simple('PETICIONES')

ListaLocal = petLocal['PETICIONCB'].values.tolist()
ListaCerebro = petCerebro['PETICIONCB'].values.tolist()

for x in ListaLocal:
     if x in ListaCerebro:
         ListaCerebro.remove(x)

ListaLocal


Genes = 'NIPA, GOLGA, ADAM'
listagenes = Genes
listagenes = str(listagenes)
listagenes = listagenes.split(sep = ',')

# REVISAR QUE TUVIERAN -2 -3 ETC