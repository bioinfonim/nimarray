import pandas as pd
import fdb
from termcolor import colored
import os
import configparser

def _cnv_value(amplification, deletion, chr):
    """
    Retrieves the copy number value by given amplification and deletion values

    :param amplification: the amplification value
    :param deletion: the deletion value
    :param chr: the chromosome value

    :return: the copy number value
    """
    copy_number = None
    conditions = []
    sentences = []

    if deletion == 0:
        conditions = [amplification >= 1, 0 < amplification < 1]
        sentences = [ARRAY_CNV_TRIP, ARRAY_CNV_DUP]

    elif amplification == 0:
        conditions = [deletion < -2, deletion < -3 and chr == ARRAY_CHRY, -1 <= deletion < 0, -2 <= deletion < -1]
        sentences = [ARRAY_CNV_DEL_HOMO, ARRAY_CNV_DEL_HEMI, ARRAY_CNV_DEL_HET, ARRAY_CNV_DEL_NOCLAS]

    for condition, sentence in zip(conditions, sentences):
        if condition:
            copy_number = sentence
            break

    return copy_number

newExcelList = []
newExcelLine = []

ARRAY_CNV_TRIP = 'Triplicación o Superior'
ARRAY_CNV_DUP = 'Duplicación'
ARRAY_CNV_DEL_HET = 'Deleción Heterocigota'
ARRAY_CNV_DEL_HEMI = 'Deleción Hemicigota'
ARRAY_CNV_DEL_HOMO = 'Deleción Homocigota'
ARRAY_CNV_DEL_NOCLAS = 'Deleción no clasificada'
ARRAY_CHRY = 'chrY'

Columnas = ['PETICIONCB', 'AberrationNo', 'Chr', 'Cytoband', 'PositionStart', 'PositionStop', 'ProbesNum',
            'Amplification', 'Deletion', 'pval', 'GeneNames', 'Analizado', 'Tamano', 'Categorization', 'CopyNumber',
            'Coments', 'Coordenadas', 'TRESULTADO', 'FECHA_REGISTRO', 'FECHA_RESULTADO']

# Config_parser configuration
config_parser = configparser.ConfigParser(allow_no_value=True)
bindir = os.path.abspath(os.path.dirname(__file__))
config_parser.read(bindir + "/cfg.cnf")

path_sin_cat_null = config_parser.get("PATHS", "path_sin_cat_null")
path_out = config_parser.get("PATHS", "path_out")

variant_infor = pd.read_excel(path_sin_cat_null, 0)
variant_infor = variant_infor.drop(columns=['IDPETICION', 'IDPRUEBA', 'NOMBRE_CORTO',
                            'MAGNITUD', 'NRESULTADO', 'RESULTADO'],errors='ignore')
print(variant_infor)
variant_infor = variant_infor.replace({'': 'NULL'})
variant_infor = variant_infor.replace({' ': 'NULL'})
variant_infor = variant_infor.replace({'-': 'NULL'})
variant_infor = variant_infor.replace({None: "NULL"})
variant_infor = variant_infor.replace({'"': ''})
variant_infor = variant_infor.replace({"'": ''})

Peticiones = list(set(variant_infor['PETICIONCB']))
Peticiones.sort()

variant_infor.to_excel('/Users/jgonzalez/Desktop/Instert_NIMArray/septiembre/variant_infor.xlsx')

for row in variant_infor.values.tolist():
    newExcelLine = row
    if newExcelLine[7] == '0,000000':
        newExcelLine[7] = 0.0
    if isinstance(newExcelLine[7], str):
        newExcelLine[7] = float(newExcelLine[7].replace(',', '.'))
    if float(newExcelLine[7]) > 100:
        newExcelLine[7] = newExcelLine[7]/1000000
    if newExcelLine[8] == '0,000000':
        newExcelLine[8] = 0.0
    if isinstance(newExcelLine[8], str):
        newExcelLine[8] = float(newExcelLine[8].replace(',', '.'))
    if float(newExcelLine[8]) < -100:
        newExcelLine[8] = newExcelLine[8]/1000000
    newExcelLine[14] = _cnv_value(newExcelLine[7], newExcelLine[8], newExcelLine[2])
    newExcelLine[16] = str(newExcelLine[2])+': '+str(newExcelLine[4])+'-'+str(newExcelLine[5])

    newExcelList.append(newExcelLine)

df = pd.DataFrame(newExcelList, columns=Columnas)  # Ojo el Nombre de las columnas
df.to_excel(path_out+'limpiado_sep1.xls', index=False)

