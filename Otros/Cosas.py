#!/usr/bin/env python3
# coding=utf-8

from __future__ import unicode_literals
import pandas as pd

carreras_2018 = []
carreras_2017 = []

path_2018_high = '/Users/rnavarro/Desktop/Resultados/ConcatFiles/Todos Juntos/Unidos2018Pruebas.xls'
path_2017_high = '/Users/rnavarro/Desktop/Resultados/ConcatFiles/Todos Juntos/Unidos2017Pruebas.xls'
path_2018_low = '/Users/rnavarro/Desktop/2018/Resultado_2018_insert.xls'
path_2017_low = '/Users/rnavarro/Desktop/2017/Resultado_2017_insert.xls'

datos_2018_high = pd.read_excel(path_2018_high)
datos_2017_high = pd.read_excel(path_2017_high)
datos_2018_low = pd.read_excel(path_2018_low)
datos_2017_low = pd.read_excel(path_2017_low)

datos_2018_high_list = list(set(datos_2018_high['PETICIONCB'].values.tolist()))
datos_2017_high_list = list(set(datos_2017_high['PETICIONCB'].values.tolist()))
datos_2018_low_list = list(set(datos_2018_low['PETICIONCB'].values.tolist()))
datos_2017_low_list = list(set(datos_2017_low['PETICIONCB'].values.tolist()))

for carrera in datos_2018_high_list:
    if carrera not in datos_2018_low_list:
        hay_datos_nuevos = True
        carreras_2018.append(carrera)

for carrera in datos_2017_high_list:
    if carrera not in datos_2017_low_list:
        hay_datos_nuevos = True
        carreras_2017.append(carrera)

print(carreras_2017)
print(carreras_2018)

num_2018_high = len(datos_2018_high_list)
num_2018_low = len(datos_2018_low_list)
num_2017_high = len(datos_2017_high_list)
num_2017_low = len(datos_2017_low_list)

print('casos 2018 high '+str(num_2018_high))
print('casos 2018 low '+str(num_2018_low))
print('casos 2017 high '+str(num_2017_high))
print('casos 2017 low '+str(num_2017_low))

print('hello')


