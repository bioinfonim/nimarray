import pandas as pd
import configparser
import os

def enResol(filamia, filalista):
    'Esta función comprueba si las dos alternaciones pasadas como variables están dentro de la resolución establecida.'
    filaCheck = filamia != filalista
    CheckChr = filalista[2] == filamia[2]
    CheckBand = filalista[3] == filamia[3]
    CheckStart = abs(filalista[4] - filamia[4]) < res
    CheckStop = abs(filalista[5] - filamia[5]) < res
    enRes = (filaCheck and CheckChr and CheckBand and CheckStart and CheckStop)
    return enRes

config_parser = configparser.ConfigParser(allow_no_value=True)
bindir = os.path.abspath(os.path.dirname(__file__))
config_parser.read(bindir + "/cfg.cnf")

path = config_parser.get("PATHS", "path")
path_out = config_parser.get("PATHS", "path_out")

columnasreps = ['PETICIONCB', 'AberrationNo', 'Chr', 'Cytoband', 'PositionStart', 'PositionStop', 'ProbesNum',
                'Amplification', 'Deletion', 'pval', 'GeneNames', 'Analizado', 'Tamano', 'Categorization', 'CopyNumber',
                'Coments', 'Coordenadas', 'TRESULTADO','FECHA_REGISTRO', 'FECHA_RESULTADO', 'nº reps']

variant_infor = pd.read_excel(path, 0)
res = 15000

ListaFinal = []
NuevaLista = []
Track = [[]]
ListaTrack = []
ListaPercent = []
Reps = []
Track3 = []
Track3_pato = []
listCat = []
ExcelCheck = []
ExcelOtros = []
counter = 0
cuantas = 0
count = 0

Listote_pato = variant_infor.values.tolist()

while len(Listote_pato) > 0:
    counter = counter + 1
    filamia = Listote_pato[0]
    NuevaLista = []
    NuevaLista.append(filamia)

    for filalista in Listote_pato:
        if enResol(filamia, filalista) and (
                filamia[14] == (filalista[14]) or (filalista[14] is None)):  #  same resolution and categorization
            NuevaLista.append(filalista)

    for filanueva in NuevaLista:
        Listote_pato.remove(filanueva)



    if len(NuevaLista) > 1:
        NuevaLista[0].append(len(NuevaLista))
        ListaFinal = NuevaLista + ListaFinal
        listCat = []
        for i in range(len(NuevaLista)):
            cat = NuevaLista[i][13]
            cat = cat.replace(' ', '')
            listCat.append(cat)
        if len(set(listCat)) != 1:
            ExcelCheck = ExcelCheck+NuevaLista
        else:
            ExcelOtros = ExcelOtros+NuevaLista
    else:
        NuevaLista[0].append(len(NuevaLista))
        ListaFinal = ListaFinal + NuevaLista

    #  transforms list into df and back to list (redundant; done to explore the results in excel.)

df = pd.DataFrame(ListaFinal, columns=columnasreps)
df.to_excel(path_out+'ExTrackTodos.xls')

df = pd.DataFrame(ExcelCheck, columns=columnasreps)
df.to_excel(path_out+'ExcelCheck_update.xls')

df = pd.DataFrame(ExcelOtros, columns=columnasreps)
df.to_excel(path_out+'ExcelOtros.xls')

# File to review the final result. ExtrackTodos, shows all the alterations grouped by its CNVtype, coordinates and resolution