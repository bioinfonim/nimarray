#!/usr/bin/env python3
# coding=utf-8

from __future__ import unicode_literals
import pandas as pd
import numpy as np
import io
from xlrd import XLRDError
from xlwt import Workbook
from os import listdir
from os.path import isfile, join

mypath = '/Users/rnavarro/Desktop/Pruebas NimG/ErrorUnicode/'

files = [f for f in listdir(mypath) if isfile(join(mypath, f))]
files.sort()

if '.DS_Store' in files:
    files.remove('.DS_Store')

HojaFinal = pd.DataFrame()


# df.loc[df['LastName']=='Smith'].index
counter = 0
file = files[1]
OldCols = ['PETICIONB', u'AberrationNo', u'Chr', u'Cytoband', u'Start', u'Stop', u'#Probes', u'Amplification', u'Deletion', u'pval',
         u'Gene Names', u'Analizado', u'Tama\xf1o', u'Categorization', u'Copy Number', u'Coments']

HojaFinal = pd.read_excel(pd.ExcelFile('/Users/rnavarro/Desktop/Pruebas NimG/ConcatFile.xls'), 0, encoding='utf-8')  # comentar

    # Ajusta el Raw data como lo queremos

try:
    ExFile = pd.ExcelFile(mypath + '/' + file)
except XLRDError:
    print(file + ' Corrupto o mal guardado, guardando de nuevo')
    file1 = io.open(mypath + '/' + file, "r", encoding="iso-8859-1")
    data = file1.readlines()
    xldoc = Workbook()
    sheet = xldoc.add_sheet("Sheet1", cell_overwrite_ok=True)
    for i, row in enumerate(data):
        for j, val in enumerate(row.replace('\n', '').split('\t')):
            sheet.write(i, j, val)
    xldoc.save(mypath + '/' + file)
    ExFile = pd.ExcelFile(mypath + '/' + file)

    # Formato Hoja1
Hoja1 = pd.read_excel(ExFile, 0, encoding='utf-8')
row1 = 0
try:
    row2 = Hoja1.loc[Hoja1['Genome: hg19'] == 'AberrationNo'].index[0]
except IndexError:
    row2 = Hoja1.loc[Hoja1['Genome: hg19'] == 'Index'].index[0]
except KeyError:
    try:
        row2 = Hoja1.loc[Hoja1['Aberration Algorithm: ADM-2'] == 'AberrationNo'].index[0]  # 19NR04965-2.xls
    except KeyError:
        row2 = Hoja1.loc[Hoja1['Aberration Algorithm: ADM-1'] == 'AberrationNo'].index[0]

Hoja1 = Hoja1.drop(range(row1, row2), axis=0)

try:
    row2 = Hoja1.loc[Hoja1['Genome: hg19'] == 'Number of calls per sample:'].index[0] + 2
    row1 = row2 - 4
except IndexError:
    vector = Hoja1['Genome: hg19'] == Hoja1['Genome: hg19']
    vector = vector.to_list()
    vector = [i for i, x in enumerate(vector) if x]
    count = len(vector)
    Hoja1 = Hoja1.reset_index()
    Hoja1 = Hoja1.drop('index', axis=1)
    row2 = count
    row1 = count
except KeyError:
    try:
        row2 = Hoja1.loc[Hoja1['Aberration Algorithm: ADM-2'] == 'Number of calls per sample:'].index[0] + 2
        row1 = row2 - 4
    except KeyError:
        row2 = Hoja1.loc[Hoja1['Aberration Algorithm: ADM-1'] == 'Number of calls per sample:'].index[0] + 2
        row1 = row2 - 4
Hoja1 = Hoja1.drop(range(row1, row2), axis=0)
Hoja1.insert(0, 'PETICIONB', Hoja1.iloc[1][0])
try:
    Hoja1 = Hoja1.drop(Hoja1.loc[Hoja1['Genome: hg19'] == 'AberrationNo'].index[0] + 1, axis=0)
except IndexError:
    Hoja1 = Hoja1.drop(Hoja1.loc[Hoja1['Genome: hg19'] == 'Index'].index[0] + 1, axis=0)
except KeyError:
    try:
        Hoja1 = Hoja1.drop(Hoja1.loc[Hoja1['Aberration Algorithm: ADM-2'] == 'AberrationNo'].index[0] + 1, axis=0)
    except KeyError:
        Hoja1 = Hoja1.drop(Hoja1.loc[Hoja1['Aberration Algorithm: ADM-1'] == 'AberrationNo'].index[0] + 1, axis=0)
Hoja1 = Hoja1.reset_index()
Hoja1 = Hoja1.drop('index', axis=1)
vector = Hoja1.loc[0].values  # Ajusta el título de las columnas
vector[0] = 'PETICIONB'
Hoja1.columns = vector
Hoja1 = Hoja1.drop(0, axis=0)
Hoja1 = Hoja1.reset_index()
Hoja1 = Hoja1.drop('index', axis=1)

# Formato Hoja2
Hoja2 = pd.DataFrame()
try:
    Hoja2 = pd.read_excel(ExFile, 1, encoding='utf-8')
    row = Hoja2.loc[Hoja2['Genome: hg19'] == 'AberrationNo'].index[0]
    col = 0
    Hoja2 = Hoja2.drop(range(col, row), axis=0)
    Hoja2 = Hoja2.drop(row + 1, axis=0)
    vector = Hoja2['Genome: hg19'] == Hoja2['Genome: hg19']
    vector = vector.to_list()
    vector = [i for i, x in enumerate(vector) if x]
    count = len(vector)
    Hoja2 = Hoja2.reset_index()
    Hoja2 = Hoja2.drop('index', axis=1)
    col = count
    row = Hoja2.shape[0]
    Hoja2 = Hoja2.drop(range(col, row), axis=0)
    Hoja2 = Hoja2.reset_index()
    Hoja2 = Hoja2.drop('index', axis=1)
    vector = Hoja2.loc[0].values
    Hoja2.columns = vector
    Hoja2 = Hoja2.drop(0, axis=0)
    Hoja2 = Hoja2.reset_index()
    Hoja2 = Hoja2.drop('index', axis=1)
except IndexError:
    print('el archivo ' + file + ' No tiene segunda hoja')
except KeyError:  # MUCHO CODIGO REPETIDO VER SI SE PUEDE ELIMINAR ALGO
    try:
        if (file == '19NR06116_destaca2.xlsx') or (file == '19NR06761_destaca2.xlsx'):
            print('saltado')
        Hoja2 = pd.read_excel(ExFile, 1, encoding='utf-8')
        row1 = 0
        row2 = Hoja2.loc[Hoja2['Aberration Algorithm: ADM-2'] == 'AberrationNo'].index[0]
        Hoja2 = Hoja2.drop(range(row1, row2), axis=0)
        Hoja2 = Hoja2.drop(row2 + 1, axis=0)
        vector = Hoja2['Aberration Algorithm: ADM-2'] == Hoja2['Aberration Algorithm: ADM-2']
        vector = vector.to_list()
        vector = [i for i, x in enumerate(vector) if x]
        count = len(vector)
        Hoja2 = Hoja2.reset_index()
        Hoja2 = Hoja2.drop('index', axis=1)
        row1 = count
        row2 = Hoja2.shape[0]
        Hoja2 = Hoja2.drop(range(row1-2, row2), axis=0)
        Hoja2 = Hoja2.reset_index()
        Hoja2 = Hoja2.drop('index', axis=1)
        vector = Hoja2.loc[0].values
        Hoja2.columns = vector
        Hoja2 = Hoja2.drop(0, axis=0)
        Hoja2 = Hoja2.reset_index()
        Hoja2 = Hoja2.drop('index', axis=1)
    except KeyError:  # MUCHO CODIGO REPETIDO VER SI SE PUEDE ELIMINAR ALGO # Excepción para el 19NR06116_destaca2.xlsx
        if (file == '19NR06116_destaca2.xlsx') or (file == '19NR06761_destaca2.xlsx'):
            print('saltado')
        Hoja2 = pd.read_excel(ExFile, 1, encoding='utf-8')
        row = Hoja2.loc[Hoja2['Aberration Algorithm: ADM-1'] == 'AberrationNo'].index[0]
        col = 0
        Hoja2 = Hoja2.drop(range(col, row), axis=0)
        Hoja2 = Hoja2.drop(row + 1, axis=0)
        vector = Hoja2['Aberration Algorithm: ADM-1'] == Hoja2['Aberration Algorithm: ADM-1']
        vector = vector.to_list()
        vector = [i for i, x in enumerate(vector) if x]
        count = len(vector)
        Hoja2 = Hoja2.reset_index()
        Hoja2 = Hoja2.drop('index', axis=1)
        col = count
        row = Hoja2.shape[0]
        Hoja2 = Hoja2.drop(range(col, row), axis=0)
        Hoja2 = Hoja2.reset_index()
        Hoja2 = Hoja2.drop('index', axis=1)
        vector = Hoja2.loc[0].values
        Hoja2.columns = vector
        Hoja2 = Hoja2.drop(0, axis=0)
        Hoja2 = Hoja2.reset_index()
        Hoja2 = Hoja2.drop('index', axis=1)

    # --------------------------------------------UNIR LAS DOS HOJAS----------------------------------------------------#
    # Igual es mejor quitar las columnas antes

try:
    result = pd.merge(Hoja1, Hoja2, how='outer')
    result = result.sort_values(by=['AberrationNo'])
except KeyError:
    result = Hoja1
    print('el archivo ' + file + ' No se puede unir por AberrationNo o falla por Unicode 1')
except UnicodeDecodeError:
    result = Hoja1
    print('el archivo ' + file + ' No se puede unir por AberrationNo o falla por Unicode 2')
except UnicodeEncodeError:
    result = Hoja1
    print('el archivo ' + file + ' No se puede unir por AberrationNo o falla por Unicode 3')



#if (OldCols == cols):

result.insert(11, 'Analizado', True)
result['Analizado'] = np.nan
# result['Analizado'] = result['Coordenadas']
result['Analizado'] = result.iloc[:, -2]
result['Analizado'] = np.where(result['Analizado'] == result['Analizado'], 'Analizado', result['Analizado'])
result = result.reset_index()
result = result.drop('index', axis=1)

cols = result.columns.tolist()

result.to_excel('/Users/rnavarro/Desktop/Pruebas NimG/Pruebas.xls', sheet_name='Pruebas')

try:
    HojaFinal = pd.concat([HojaFinal, result], sort=False)
except ValueError:
    result.columns = OldCols
    HojaFinal = pd.concat([HojaFinal, result], sort=False)
#HojaFinal.to_excel('/Users/rnavarro/Desktop/Resultados/File1.xls', sheet_name='Concatenadas')

# for file in files:
#    ExFile = pd.ExcelFile('/Users/rnavarro/Desktop/Pruebas NimG/'+files[0])
#    hoja1 = ExFile.parse(ExFile.sheet_names[0])
#    Hoja2 = pd.DataFrame()
#    if len(ExFile.sheet_names) > 1:
#        hoja2 = ExFile.parse(ExFile.sheet_names[1])
#    hoja3 = pd.concat([hoja1, hoja2], axis=1)
#    HojaFinal = pd.concat([HojaFinal, hoja3])

HojaFinal.to_excel('/Users/rnavarro/Desktop/Pruebas NimG/concatFile.xls', sheet_name='Concatenadas')
