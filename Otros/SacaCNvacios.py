# Este script conecta con la base de datos y saca en un excel las alternaciones analizadas que no tienen añadido
# el CopyNumber

from __future__ import unicode_literals
import pandas as pd
from dbconnect.dbConnect import dbConnect
from nimapidb.db import dbConnect

db = dbConnect.dbConnect('LocalHost', 'root', 'Password.10', 'NIMArray')  #  Conecta a la Base de Datos
dbcursor = db.db.cursor()
sql_command = """SELECT * FROM VARIANTES_INFORMADAS where Analizado = 'Analizado' AND isnull(CopyNumber)"""  # Filtra
dbcursor.execute(sql_command)
variant_infor = pd.read_sql_query(sql_command, db.db)
variant_infor = variant_infor.drop(['TestType'], axis=1)
variant_infor = variant_infor.values.tolist()

df = pd.DataFrame(variant_infor, columns=['PETICIONCB', 'AberrationNo', 'Chr', 'Cytoband', 'PositionStart', 'PositionStop',
                           'ProbesNum', 'Amplification', 'Deletion', 'pval', 'GeneNames', 'Analizado', 'Tamano',
                           'Categorization', 'CopyNumber',
                           'Coments', 'Coordenadas', 'Gain', 'Loss', 'TRESULTADO', 'FECHA_REGISTRO', 'FECHA_RESULTADO'])
df.to_excel("/Users/rnavarro/Desktop/CNempty.xls") # saca el excel