# Este script cruza el documento obtenido de Gestlab con la base de datos. El documento de Gestlab incluye el tipo
# de análisis según el ID. El script añade esta información en una nueva columna a la base de datos.
# This field is also calculated when adding the alterations from the excels or inserting the destaca_2 cases.

import pandas as pd
from dbconnect.dbConnect import dbConnect

lista_errores = []
Count = 0

Tipos = pd.read_excel('/Users/rnavarro/Desktop/Arrays/Genomica_Gestlab_Interpretación_Tipo copia.xlsx')
Tipos = Tipos.values.tolist()
#dbArrayLocal = dbConnect('LocalHost', 'root', 'Password.10', 'NIMArray')
dbArrayCerebro = dbConnect('bioinfodb.nimgenetics.com', 'nimarray', 'nimarray', 'NIMArray')
dbcursor = dbArrayCerebro.db.cursor()
sql_command = """SELECT count(*)
                 FROM information_schema.columns 
                 WHERE table_name = 'VARIANTES_INFORMADAS';"""
dbcursor.execute(sql_command)
cols = dbcursor.fetchall()[0][0]

# Añadade una columna a la base de datos si el numero de de columnas es igual a 22

if cols == 19:
    sql_command = """ALTER TABLE VARIANTES_INFORMADAS
                     ADD TestType varchar(20);"""
    dbcursor.execute(sql_command)

# El documento obtenido de gestlab, guardado en Tipo, es cruzado con la base de datos para añadir a esta el tipo
# de análisis que se realiza (Autismo, 180K, 60K, prenantal, 400K)

for row in Tipos:
    where = ('PETICIONCB = %s' % ("'" + row[0] + "'"))
    df = dbArrayCerebro.select_df('VARIANTES_INFORMADAS', '*', where)   # Saco un dataframe de la base de datos filtrando por PETICIONCB
    df['TestType'] = row[4]                                 # Al dataframe le añado una columna tipo con el tipo de analisis
                                                            # El resto del código actualiza la base de datos con la nueva columna
    df = df.replace({'': 'NULL'})
    df = df.replace({' ': 'NULL'})
    df = df.replace({'-': 'NULL'})
    df = df.replace({None: "NULL"})
    df = df.replace({'"': ''})
    df = df.replace({"'": ''})

    for fila in df.values.tolist():
        values = fila
        values = tuple(values)

        PETICIONCB = values[0]
        if PETICIONCB != "NULL":
            PETICIONCB = "'" + PETICIONCB + "'"
        AberrationNo = values[1]
        if AberrationNo != "NULL":
            AberrationNo = "'" + str(AberrationNo) + "'"
        Chr = values[2]
        if Chr != "NULL":
            Chr = "'" + Chr + "'"
        Cytoband = values[3]
        if Cytoband != "NULL":
            Cytoband = "'" + Cytoband + "'"
        PositionStart = values[4]
        if PositionStart != "NULL":
            PositionStart = "'" + str(PositionStart) + "'"
        PositionStop = values[5]
        if PositionStop != "NULL":
            PositionStop = "'" + str(PositionStop) + "'"
        ProbesNum = values[6]
        if ProbesNum != "NULL":
            ProbesNum = "'" + str(ProbesNum) + "'"
        Amplification = values[7]
        if Amplification != "NULL":
            Amplification = "'" + str(Amplification) + "'"
        Deletion = values[8]
        if Deletion != "NULL":
            Deletion = "'" + str(Deletion) + "'"
        pval = values[9]
        if pval != "NULL":
            pval = "'" + str(pval) + "'"
        GeneNames = values[10]
        if GeneNames != "NULL":
            GeneNames = "'" + GeneNames + "'"
        Analizado = values[11]
        if Analizado != "NULL":
            Analizado = "'" + Analizado + "'"
        Tamano = values[12]
        if Tamano != "NULL":
            Tamano = "'" + str(Tamano) + "'"
        Categorization = values[13]
        if Categorization != "NULL":
            Categorization = "'" + Categorization + "'"
        CopyNumber = values[14]
        if CopyNumber != "NULL":
            CopyNumber = "'" + str(CopyNumber) + "'"
        Coments = values[15]
        if Coments != "NULL":
            Coments = "'" + str(Coments) + "'"
        Coordenadas = values[16]
        if Coordenadas != "NULL":
            Coordenadas = "'" + Coordenadas + "'"
        FECHA_REGISTRO = values[17]
        if FECHA_REGISTRO != "NULL":
            FECHA_REGISTRO = "'" + str(FECHA_REGISTRO) + "'"
        FECHA_RESULTADO = values[18]
        if FECHA_RESULTADO != "NULL":
            FECHA_RESULTADO = "'" + str(FECHA_RESULTADO) + "'"
        TestType = values[19]
        if TestType != "NULL":
            TestType = "'" + str(TestType) + "'"

        whereUpdate = ("TestType = %s" % (TestType))

        fields = (
        'PETICIONCB', 'AberrationNo', 'Chr', 'Cytoband', 'PositionStart', 'PositionStop', 'ProbesNum', 'Amplification',
        'Deletion', 'pval', 'GeneNames', 'Analizado', 'Tamano', 'Categorization', 'CopyNumber', 'Coments', 'Coordenadas',
        'FECHA_REGISTRO', 'FECHA_RESULTADO', 'TestType')

        value_list = "(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)" % (
            PETICIONCB, AberrationNo, Chr, Cytoband, PositionStart, PositionStop, ProbesNum, Amplification,
            Deletion, pval, GeneNames, Analizado, Tamano, Categorization, CopyNumber, Coments, Coordenadas,
            FECHA_REGISTRO, FECHA_RESULTADO, TestType)

        insert = dbArrayCerebro.insert_update('VARIANTES_INFORMADAS', fields, value_list, whereUpdate)
        Count = Count+1

        dbArrayCerebro.commit()
        print(insert)
        if insert == -1:
            lista_errores.append(PETICIONCB)

        dbArrayCerebro.commit()

print(lista_errores)
print('Numero de inserts hechos: '+str(Count))
lista_errores_df = pd.DataFrame(lista_errores)
lista_errores_df.to_excel("/Users/rnavarro/Desktop/ResultadosSQL/ListaErroresTipo.xls")

