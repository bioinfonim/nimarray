# Este script conecta con la base de datos NIMArray y saca un .bed final que se puede cargar en Cytogenomics como Track

from __future__ import unicode_literals
import pandas as pd
import numpy as np
import dbConnect

res = 15000  # RESOLUCIÓN PARA BENIGNAS

Pruebas = ([15000,'Autismo_Patogenicas','(104)'],[60000,'60K_Patogenicas','(98)'],[180000,'180K_Patogenicas','(353)'],[250000,'Prenatal_Patogenicas','(21)'],[400000,'400K_Patogenicas','(42)'],[15000,'Benignas','(104,98,353,21,42)']) #Resolucion,NombreTrack,CódigoGestlab

db = dbConnect.dbConnect('bioinfodb.nimgenetics.com', 'nimarray', 'nimarray', 'NIMArray')  # Conecta a la Base de Datos

ListaFinal = []
NuevaLista = []
Track = [[]]
ListaTrack = []
ListaPercent = []
Reps = []
columnas = ['PETICIONCB', 'AberrationNo', 'Chr', 'Cytoband', 'PositionStart', 'PositionStop', 'ProbesNum',
            'Amplification',
            'Deletion', 'pval', 'GeneNames', 'Analizado', 'Tamano', 'Categorization', 'CopyNumber', 'Coments',
            'Coordenadas', 'TRESULTADO', 'FECHA_REGISTRO', 'FECHA_RESULTADO']
Track3 = []
Track3_pato = []
counter = 0
cuantas = 0
count = 0


def enResol(filamia, filalista):
    'Esta función comprueba si las dos alternaciones pasadas como variables están dentro de la resolución establecida.'

    filaCheck = filamia != filalista
    CheckChr = filalista[2] == filamia[2]
    CheckBand = filalista[3] == filamia[3]
    CheckStart = abs(filalista[4] - filamia[4]) < res
    CheckStop = abs(filalista[5] - filamia[5]) < res
    enRes = (filaCheck and CheckChr and CheckBand and CheckStart and CheckStop)

    return enRes
def decidecolor(color, number, ListaPercent):
    'Esta función decide el color final que va a llevar la alteración en el track'

    ListaNoDup = list(set(ListaPercent))
    ListaNoDup.sort()
    top = max(ListaNoDup)
    bottom = min(ListaNoDup)
    p66 = np.percentile(ListaNoDup, 66)
    p33 = np.percentile(ListaNoDup, 33)

    if bottom <= number < p33:
        color = color + 'Claro'

    if p33 <= number < p66:
        color = color + 'Intermedio'

    if p66 <= number <= top:
        color = color + 'Oscuro'

    color = int(DicColor[color])

    return color


columnasTrackV5 = ['Chromosome', 'Start', 'Stop', 'Name', 'score', 'strand', 'thick_start', 'thick_end', 'item_rgb',
                   'block_count',
                   'block_size', 'block_starts', 'exp_count', 'exp_Ids', 'exp_scores']
columnasTrackV5Limpias = [u'title', u'', u'', u'', u'', u'', u'', u'', u'', u'percent']
columnasTrackV5LimpiasFinal = [u'title', u'', u'', u'', u'', u'', u'', u'', u'', u'']
columnasTrackV5AgilentDos = [u'chromosome', u'start', u'stop', u'name', u'score', u'strand', u'thick_start', u'thick_end',
                          u'item_rgb', u'block_count', u'block_size', u'block_starts', u'exp_count']
columnasTrackV5Agilent = [u'chromosome', u'start', u'stop', u'name', u'score', u'strand', u'thick_start', u'thick_end',
                          u'item_rgb', u'block_count', u'block_size', u'block_starts', u'exp_count', u'exp_ids',
                          u'exp_scores']
DicCategorias = {
    'Patogénica': u'Rojo',
    'Probablemente Patogenica': u'Naranja',
    'VUS': u'Amarillo',
    'Probablemente Benigna': u'Verde',
    'Benigna': u'Azul',
    'Basurilla-No Real': u'Morado',
    'Hallazgo incidental': u'Granate',
    'Heredado': u'AzulNav',
    'None': u'Gris'
}
DicCopyNumber = {
    'Deleción Hemicigota': u'Rojo',
    'Deleción Heterocigota': u'Rojo',
    'Deleción no clasificada':u'Rojo',
    'Deleción Homocigota': u'Rojo',
    'Duplicación': u'Azul',
    'Triplicación o Superior': u'Azul',
    'Deleción no clasificada': u'Gris'
}
DicColor = {
    'RojoClaro': u'16751001',
    'RojoIntermedio': u'16724787',
    'RojoOscuro': u'10027008',
    'NaranjaClaro': u'16764057',
    'NaranjaIntermedio': u'16750899',
    'NaranjaOscuro': u'13395456',
    'AmarilloClaro': u'16777113',
    'AmarilloIntermedio': u'16777011',
    'AmarilloOscuro': u'13421568',
    'VerdeClaro': u'10092441',
    'VerdeIntermedio': u'39168',
    'VerdeOscuro': u'26112',
    'AzulClaro': u'10079487',
    'AzulIntermedio': u'3381759',
    'AzulOscuro': u'19609',
    'MoradoClaro': u'13408767',
    'MoradoIntermedio': u'10040319',
    'MoradoOscuro': u'4980889',
    'GranateClaro': u'16751052',
    'GranateIntermedio': u'16711807',
    'GranateOscuro': u'6684723',
    'AzulNavClaro': u'10066431',
    'AzulNavIntermedio': u'3355647',
    'AzulNavOscuro': u'153',
    'GrisClaro': u'12632256',
    'GrisIntermedio': u'8421504',
    'GrisOscuro': u'4210752'
}


<<<<<<< HEAD:Otros/TrackGeneratorV5Viejo.py
db = dbConnect.dbConnect('bioinfodb.nimgenetics.com', 'nimarray', 'nimarray', 'NIMArray')
#db = dbConnect.dbConnect('LocalHost', 'root', 'Password.10', 'NIMArray')  #  Conecta a la Base de Datos; IP Rafa Varía
dbcursor = db.db.cursor()
sql_command =  """SELECT * FROM VARIANTES_INFORMADAS where Analizado = 'Analizado' AND TestType = '104' AND 
    (Categorization = 'Patogénica' OR Categorization = 'Probablemente Patogénica' )"""   # CAMBIOS AQUI PARA FILTRAR
dbcursor.execute(sql_command)
variant_infor = pd.read_sql_query(sql_command, db.db)
variant_infor = variant_infor.drop(['TestType'], axis=1)
=======
# dbcursor = db.db.cursor()
# sql_command = """SELECT * FROM VARIANTES_INFORMADAS where Analizado = 'Analizado' AND
# (Categorization = 'Benigna' OR Categorization = 'Probablemente Benigna')"""  # CAMBIOS AQUI PARA FILTRAR
# dbcursor.execute(sql_command)
# variant_infor = pd.read_sql_query(sql_command, db.db)
# variant_infor = variant_infor.drop(['TestType'], axis=1)


>>>>>>> eb3e9b38533471245fbf81a686afc4f9fdc84412:TrackGeneratorV5.py

sql_command = """SELECT DISTINCT PETICIONCB FROM NIMArray.VARIANTES_INFORMADAS;"""
total = pd.read_sql_query(sql_command, db.db)
total = total.PETICIONCB.tolist()
total = list(set(total))
total = len(total)

# variant_infor = db.select_df_simple('VARIANTES_INFORMADASDOS') # Saco la Tabla
# Analizado = variant_infor['Analizado'] == 'Analizado' # Filtro por Analizado
# variant_infor = variant_infor[Analizado]

<<<<<<< HEAD:Otros/TrackGeneratorV5Viejo.py
Listote = variant_infor.values.tolist()

while len(Listote) > 0:
    filamia = Listote[0]
    Listote.remove(filamia)
    NuevaLista = []
    NuevaLista.append(filamia)

    for filalista in Listote:
        if enResol(filamia, filalista) and (filamia[14] == (filalista[14]) or (
                filalista[14] == '')):  #  Que esté en resolución y tenga la misma categorización
            NuevaLista.append(filalista)
            Listote.remove(filalista)

    if len(NuevaLista) > 1:
        NuevaLista[0].append(len(NuevaLista))
        ListaFinal = NuevaLista + ListaFinal
    else:
        NuevaLista[0].append(len(NuevaLista))
        ListaFinal = ListaFinal + NuevaLista

    counter = counter + 1
    print('cuentas: ' + str(counter))

df = pd.DataFrame(ListaFinal, columns=['PETICIONCB', 'AberrationNo', 'Chr', 'Cytoband', 'PositionStart', 'PositionStop',
                                       'ProbesNum', 'Amplification', 'Deletion', 'pval', 'GeneNames', 'Analizado',
                                       'Tamano', 'Categorization', 'CopyNumber',
                                       'Coments', 'Coordenadas', 'FECHA_REGISTRO',
                                       'FECHA_RESULTADO', 'nº reps'])

#  Convierto la Lista en df y luego la vuelvo a convertir en lista (redundante; Lo hago por pasarlo a excel y verlo bien

ExTrack = df.values.tolist()
ExTrackCopia = ExTrack.copy()

df = pd.DataFrame(ExTrackCopia,
                  columns=['PETICIONCB', 'AberrationNo', 'Chr', 'Cytoband', 'PositionStart', 'PositionStop',
                           'ProbesNum', 'Amplification', 'Deletion', 'pval', 'GeneNames', 'Analizado', 'Tamano',
                           'Categorization', 'CopyNumber',
                           'Coments', 'Coordenadas', 'FECHA_REGISTRO', 'FECHA_RESULTADO',
                           'nº reps'])
df.to_excel("/Users/rnavarro/Desktop/Resultados/ExTrackTodos.xls")

for row in ExTrack:
    if str(row[19]) == 'nan':
        ExTrackCopia.remove(row)

df = pd.DataFrame(ExTrackCopia,
                  columns=['PETICIONCB', 'AberrationNo', 'Chr', 'Cytoband', 'PositionStart', 'PositionStop',
                           'ProbesNum', 'Amplification', 'Deletion', 'pval', 'GeneNames', 'Analizado', 'Tamano',
                           'Categorization', 'CopyNumber',
                           'Coments', 'Coordenadas', 'FECHA_REGISTRO', 'FECHA_RESULTADO',
                           'nº reps'])
df.to_excel("/Users/rnavarro/Desktop/Resultados/ExTrackSolos.xls")

for row in ExTrackCopia:
    percent = round((row[19] / total) * 100, 2)
    ListaPercent.append(percent)
for row in ExTrackCopia:

    ListaTrack = []

    CNVType = row[14]

    if str(row[10]) == 'None':
        GEN = ''
    else:
        GEN = row[10]

    if str(row[15]) == 'None':
        coments = '---'
    else:
        coments = row[15]

    if len(row[2]) == 4:
        chrom = row[2][:3] + '0' + row[2][3]
        if (chrom[4] == 'X') or (chrom[4] == 'Y'):
            chrom = row[2][:3] + '9' + row[2][3]
    else:
        chrom = row[2]

    percent = round((row[19] / total) * 100, 2)
    Color = decidecolor(DicCopyNumber[CNVType], percent, ListaPercent)

    CNVType = CNVType.replace('ó', 'o')
    CNVType = CNVType.replace('é', 'e')

    # Track Version CYTOGENOMICS 5.0

    ListaTrack.append(chrom)
    ListaTrack.append(row[4])
    ListaTrack.append(row[5])
    # ListaTrack.append('Cs:'+str(row[21])+' Pr:'+str(percent)+' '+CNVType[:3])
    ListaTrack.append('Coordinates: ' + str(row[2]) + ':' + str(row[4]) + '-' + str(row[5]) + '|Size: ' + str(
        abs(row[4] - row[5])) + '|CNVType: ' + CNVType + '|OMIM: ' + GEN + '|Ocurrence (' + str(
        int(row[19])) + '/' + str(total) + '): ' + str(percent) + '% |' + 'coments: '+'--|')
    ListaTrack.append('1000')
    ListaTrack.append('+')
    ListaTrack.append(row[4])
    ListaTrack.append(row[5])
    ListaTrack.append(Color)
    ListaTrack.append('1')
    ListaTrack.append(str(abs(row[4] - row[5]))+'.0')
    ListaTrack.append('0.0')
    ListaTrack.append(percent)
    ListaTrack.append('')
    ListaTrack.append('')
    Track.append(ListaTrack)

Track.pop(0)

df = pd.DataFrame(Track, columns=columnasTrackV5Agilent)
#df = df.sort_values(by=[u'chromosome', u'exp_count'], ascending=[True, True])
df = df.sort_values(by=[u'start'])
df[u'exp_count'] = ''

Track2 = df.values.tolist()

for row in Track2:
    if (row[0][3] == '0') or (row[0][3] == '9'):
        row[0] = row[0][:3] + row[0][4]
        Track3.append(row)
    else:
        Track3.append(row)

# ListaTrack = []
# ListaTrack.append('')
# ListaTrack.append('')
# ListaTrack.append('')
# ListaTrack.append('')
# ListaTrack.append('')
# ListaTrack.append('')
# ListaTrack.append('')
# ListaTrack.append('')
# ListaTrack.append('')
=======
# Listote = variant_infor.values.tolist()

>>>>>>> eb3e9b38533471245fbf81a686afc4f9fdc84412:TrackGeneratorV5.py
#
# while len(Listote) > 0:
#     filamia = Listote[0]
#     Listote.remove(filamia)
#     NuevaLista = []
#     NuevaLista.append(filamia)
#
#     for filalista in Listote:
#         if enResol(filamia, filalista) and (filamia[14] == (filalista[14]) or (
#                 filalista[14] == '')):  #  Que esté en resolución y tenga la misma categorización
#             NuevaLista.append(filalista)
#             Listote.remove(filalista)
#
#     if len(NuevaLista) > 1:
#         NuevaLista[0].append(len(NuevaLista))
#         ListaFinal = NuevaLista + ListaFinal
#     else:
#         NuevaLista[0].append(len(NuevaLista))
#         ListaFinal = ListaFinal + NuevaLista
#
#     counter = counter + 1
#     print('cuentas: ' + str(counter))
#
# df = pd.DataFrame(ListaFinal, columns=['PETICIONCB', 'AberrationNo', 'Chr', 'Cytoband', 'PositionStart', 'PositionStop',
#                                        'ProbesNum', 'Amplification', 'Deletion', 'pval', 'GeneNames', 'Analizado',
#                                        'Tamano', 'Categorization', 'CopyNumber',
#                                        'Coments', 'Coordenadas','FECHA_REGISTRO',
#                                        'FECHA_RESULTADO', 'nº reps'])
#
# #  Convierto la Lista en df y luego la vuelvo a convertir en lista (redundante; Lo hago por pasarlo a excel y verlo bien
#
# ExTrack = df.values.tolist()
# ExTrackCopia = ExTrack.copy()
#
# df = pd.DataFrame(ExTrackCopia,
#                   columns=['PETICIONCB', 'AberrationNo', 'Chr', 'Cytoband', 'PositionStart', 'PositionStop',
#                            'ProbesNum', 'Amplification', 'Deletion', 'pval', 'GeneNames', 'Analizado', 'Tamano',
#                            'Categorization', 'CopyNumber',
#                            'Coments', 'Coordenadas', 'FECHA_REGISTRO', 'FECHA_RESULTADO',
#                            'nº reps'])
# df.to_excel("/Users/jgonzalez/Desktop/ExTrackTodos.xls")
#
# for row in ExTrack:
#     if str(row[19]) == 'nan':
#         ExTrackCopia.remove(row)
#
# df = pd.DataFrame(ExTrackCopia,
#                   columns=['PETICIONCB', 'AberrationNo', 'Chr', 'Cytoband', 'PositionStart', 'PositionStop',
#                            'ProbesNum', 'Amplification', 'Deletion', 'pval', 'GeneNames', 'Analizado', 'Tamano',
#                            'Categorization', 'CopyNumber',
#                            'Coments', 'Coordenadas', 'FECHA_REGISTRO', 'FECHA_RESULTADO',
#                            'nº reps'])
# df.to_excel("/Users/jgonzalez/Desktop/ExTrackSolos.xls")
#
# for row in ExTrackCopia:
#     percent = round((row[19] / total) * 100, 2)
#     ListaPercent.append(percent)
# for row in ExTrackCopia:
#
#     ListaTrack = []
#
#     CNVType = row[14]
#
#
#     if str(row[10]) == 'None':
#         GEN = ''
#     else:
#         GEN = row[10]
#
#     if str(row[15]) == 'None':
#         coments = '---'
#     else:
#         coments = row[15]
#
#     if len(row[2]) == 4:
#         chrom = row[2][:3] + '0' + row[2][3]
#         if (chrom[4] == 'X') or (chrom[4] == 'Y'):
#             chrom = row[2][:3] + '9' + row[2][3]
#     else:
#         chrom = row[2]
#
#     percent = round((row[19] / total) * 100, 2)
#     Color = decidecolor(DicCopyNumber[CNVType], percent, ListaPercent)
#
#     CNVType = CNVType.replace('','')
#     CNVType = CNVType.replace('ó', 'o')
#     CNVType = CNVType.replace('é', 'o')
#
#     # Track Version CYTOGENOMICS 5.0
#
#     ListaTrack.append(chrom)
#     ListaTrack.append(row[4])
#     ListaTrack.append(row[5])
#     # ListaTrack.append('Cs:'+str(row[21])+' Pr:'+str(percent)+' '+CNVType[:3])
#     ListaTrack.append('Coordinates: ' + str(row[2]) + ':' + str(row[4]) + '-' + str(row[5]) + '|Size: ' + str(
#         abs(row[4] - row[5])) + '|CNVType: ' + CNVType + '|OMIM: ' + GEN + '|Ocurrence (' + str(
#         int(row[19])) + '/' + str(total) + '): ' + str(percent) + '% |' + 'coments: '+'--|')
#     ListaTrack.append('1000')
#     ListaTrack.append('+')
#     ListaTrack.append(row[4])
#     ListaTrack.append(row[5])
#     ListaTrack.append(Color)
#     ListaTrack.append('1')
#     ListaTrack.append(str(abs(row[4] - row[5]))+'.0')
#     ListaTrack.append('0.0')
#     ListaTrack.append(percent)
#     ListaTrack.append('')
#     ListaTrack.append('')
#     Track.append(ListaTrack)
#
# Track.pop(0)
#
# df = pd.DataFrame(Track, columns=columnasTrackV5Agilent)
# #df = df.sort_values(by=[u'chromosome', u'exp_count'], ascending=[True, True])
# df = df.sort_values(by=[u'start'])
# df[u'exp_count'] = ''
#
# Track2 = df.values.tolist()
#
# for row in Track2:
#     if (row[0][3] == '0') or (row[0][3] == '9'):
#         row[0] = row[0][:3] + row[0][4]
#         Track3.append(row)
#     else:
#         Track3.append(row)
#
# # ListaTrack = []
# # ListaTrack.append('')
# # ListaTrack.append('')
# # ListaTrack.append('')
# # ListaTrack.append('')
# # ListaTrack.append('')
# # ListaTrack.append('')
# # ListaTrack.append('')
# # ListaTrack.append('')
# # ListaTrack.append('')
# #
# # Track3.insert(0, ListaTrack)
#
# df = pd.DataFrame(Track3, columns=columnasTrackV5Agilent)
#
# df.to_csv("/Users/jgonzalez/Desktop/TrackBenignas_V5.bed", sep='\t', index=False, encoding='latin-1')

for n in Pruebas:
    res = n[0]
    tipo = n[1]
    codigo = n[2]

    sql_command_pato = """SELECT * FROM VARIANTES_INFORMADAS where Analizado = 'Analizado' AND TestType IN %s AND 
    (Categorization = 'Patogénica' OR Categorization = 'Probablemente Patogénica' )"""  # CAMBIOS AQUI PARA FILTRAR
    query = sql_command_pato % codigo
    variant_infor_pato = pd.read_sql_query(query, db.db)
    print(variant_infor_pato)
    variant_infor_pato = variant_infor_pato.drop(['TestType'], axis=1)

    Listote_pato = variant_infor_pato.values.tolist()

    while len(Listote_pato) > 0:
        filamia = Listote_pato[0]
        Listote_pato.remove(filamia)
        NuevaLista = []
        NuevaLista.append(filamia)

        for filalista in Listote_pato:
            if enResol(filamia, filalista) and (filamia[14] == (filalista[14]) or (
                    filalista[14] == '')):  #  Que esté en resolución y tenga la misma categorización
                NuevaLista.append(filalista)
                Listote_pato.remove(filalista)

        if len(NuevaLista) > 1:
            NuevaLista[0].append(len(NuevaLista))
            ListaFinal = NuevaLista + ListaFinal
        else:
            NuevaLista[0].append(len(NuevaLista))
            ListaFinal = ListaFinal + NuevaLista

        counter = counter + 1
        print('cuentas: ' + str(counter))

    df_pato = pd.DataFrame(ListaFinal, columns=['PETICIONCB', 'AberrationNo', 'Chr', 'Cytoband', 'PositionStart', 'PositionStop',
                                           'ProbesNum', 'Amplification', 'Deletion', 'pval', 'GeneNames', 'Analizado',
                                           'Tamano', 'Categorization', 'CopyNumber',
                                           'Coments', 'Coordenadas','FECHA_REGISTRO',
                                           'FECHA_RESULTADO', 'nº reps'])

    #  Convierto la Lista en df y luego la vuelvo a convertir en lista (redundante; Lo hago por pasarlo a excel y verlo bien

    ExTrack = df_pato.values.tolist()
    ExTrackCopia = ExTrack.copy()

    df_pato = pd.DataFrame(ExTrackCopia,
                      columns=['PETICIONCB', 'AberrationNo', 'Chr', 'Cytoband', 'PositionStart', 'PositionStop',
                               'ProbesNum', 'Amplification', 'Deletion', 'pval', 'GeneNames', 'Analizado', 'Tamano',
                               'Categorization', 'CopyNumber',
                               'Coments', 'Coordenadas', 'FECHA_REGISTRO', 'FECHA_RESULTADO',
                               'nº reps'])
    df_pato.to_excel("/Users/jgonzalez/Desktop/ExTrackTodos.xls")

    for row in ExTrack:
        if str(row[19]) == 'nan':
            ExTrackCopia.remove(row)

    df_pato = pd.DataFrame(ExTrackCopia,
                      columns=['PETICIONCB', 'AberrationNo', 'Chr', 'Cytoband', 'PositionStart', 'PositionStop',
                               'ProbesNum', 'Amplification', 'Deletion', 'pval', 'GeneNames', 'Analizado', 'Tamano',
                               'Categorization', 'CopyNumber',
                               'Coments', 'Coordenadas', 'FECHA_REGISTRO', 'FECHA_RESULTADO',
                               'nº reps'])
    df_pato.to_excel("/Users/jgonzalez/Desktop/ExTrackSolos.xls")

    for row in ExTrackCopia:
        percent = round((row[19] / total) * 100, 2)
        ListaPercent.append(percent)
    for row in ExTrackCopia:

        ListaTrack = []

        CNVType = row[14]

<<<<<<< HEAD:Otros/TrackGeneratorV5Viejo.py
df.to_csv("/Users/rnavarro/Desktop/Tracks/TrackAutismovsJulV5pruebas.bed", sep='\t', index=False, encoding='latin-1')
print('END')
=======
        if str(row[10]) == 'None':
            GEN = ''
        else:
            GEN = row[10]

        if str(row[15]) == 'None':
            coments = '---'
        else:
            coments = row[15]

        if len(row[2]) == 4:
            chrom = row[2][:3] + '0' + row[2][3]
            if (chrom[4] == 'X') or (chrom[4] == 'Y'):
                chrom = row[2][:3] + '9' + row[2][3]
        else:
            chrom = row[2]

        percent = round((row[19] / total) * 100, 2)
        Color = decidecolor(DicCopyNumber[CNVType], percent, ListaPercent)

        CNVType = CNVType.replace('', '')
        CNVType = CNVType.replace('ó', 'o')
        CNVType = CNVType.replace('é', 'o')
        # Track Version CYTOGENOMICS 5.0

        ListaTrack.append(chrom)
        ListaTrack.append(row[4])
        ListaTrack.append(row[5])
        # ListaTrack.append('Cs:'+str(row[21])+' Pr:'+str(percent)+' '+CNVType[:3])
        ListaTrack.append('Coordinates: ' + str(row[2]) + ':' + str(row[4]) + '-' + str(row[5]) + '|Size: ' + str(
            abs(row[4] - row[5])) + '|CNVType: ' + CNVType + '|OMIM: ' + GEN + '|Ocurrence (' + str(
            int(row[19])) + '/' + str(total) + '): ' + str(percent) + '% |' + 'coments: '+'--|')
        ListaTrack.append('1000')
        ListaTrack.append('+')
        ListaTrack.append(row[4])
        ListaTrack.append(row[5])
        ListaTrack.append(Color)
        ListaTrack.append('1')
        ListaTrack.append(str(abs(row[4] - row[5]))+'.0')
        ListaTrack.append('0.0')
        ListaTrack.append(percent)
        ListaTrack.append('')
        ListaTrack.append('')
        Track.append(ListaTrack)

    Track.pop(0)

    df_pato = pd.DataFrame(Track, columns=columnasTrackV5Agilent)
    #df = df.sort_values(by=[u'chromosome', u'exp_count'], ascending=[True, True])
    df_pato = df_pato.sort_values(by=[u'chromosome',u'start'])
    df_pato[u'exp_count'] = ''

    Track2_pato = df_pato.values.tolist()

    for row in Track2_pato:
        if (row[0][3] == '0') or (row[0][3] == '9'):
            row[0] = row[0][:3] + row[0][4]
            Track3_pato.append(row)
        else:
            Track3_pato.append(row)


    df_pato = pd.DataFrame(Track3_pato, columns=columnasTrackV5Agilent)
    path ="/Users/jgonzalez/Desktop/Track_%s_V5.bed" % tipo
    df_pato.to_csv(path, sep='\t', index=False, encoding='latin-1')
    print('END')
>>>>>>> eb3e9b38533471245fbf81a686afc4f9fdc84412:TrackGeneratorV5.py
