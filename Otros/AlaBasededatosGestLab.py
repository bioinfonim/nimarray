import fdb
import pandas as pd
from nimapidb.db import dbConnect

lista_errores = []
Count = 0

dbArrayLocal = dbConnect.dbConnect('LocalHost', 'root', 'Password.10', 'NIMArray')
dbArrayCerebro = dbConnect.dbConnect('bioinfodb.nimgenetics.com', 'nimarray', 'nimarray', 'NIMArray')

variant_infor = pd.read_excel("/Users/rnavarro/Desktop/2019/InsertSQL.xls", 'Sheet1')
Peticiones = pd.DataFrame(list(set(variant_infor['PETICIONCB'])))
Peticiones = Peticiones.rename(columns={0: 'PETICIONCB'})

# FALTAN 4 QUE SALEN NAN 19NR00981 (escrito '19NR00981 ') en gestlab?; 19NR05256; 19NR05237; 19NR00071;
# PeticionesTabla = pd.merge(Peticiones, tabla1, on='PETICIONCB')

# sql_command = """SELECT DISTINCT PETICIONCB FROM NIMArray.VARIANTES_INFORMADAS;"""
# Peticiones = pd.read_sql_query(sql_command, dbArrayLocal.db)

GestCon = fdb.connect(dsn='192.168.30.224:E:\GESTLAB.GLDB', user='BIOINFO', password='1234', charset='ISO8859_1')

#select = """select distinct p.PETICIONCB, p.IDPETICION, pp.IDPRUEBA, pru.NOMBRE_CORTO, pp.IDPRUEBA_LISTA, pru.MAGNITUD, pp.NRESULTADO,
#        pp.RESULTADO, pp.TRESULTADO, p.FECHA_REGISTRO, p.FECHA_RESULTADO from peticion p join pet_prueba pp on (p.IDPETICION = pp.IDPETICION)
#        join prueba pru on (pp.IDPRUEBA = pru.IDPRUEBA) where pp.IDPRUEBA in (300)"""

query1 = """select p.PETICIONCB, pru.IDPRUEBA, p.IDPETICION, p.IDLABORATORIO, p.IDPACIENTE, p.FECHA_TOMA, p.FECHA_ALTA, 
p.COMENTARIO, p.IDPROCEDENCIA, pp.TRESULTADO from PETICION p join PET_PRUEBA pp on (p.IDPETICION = pp.IDPETICION)
join PRUEBA pru on (pp.IDPRUEBA = pru.IDPRUEBA) where pp.IDPRUEBA in (300)"""

query2 = """select pac.IDPACIENTE, pf.CLAVE, pac.APELLIDOS, pac.NOMBRE, pac.FECHA_NACIMIENTO, pac.SEXO,
(p.FECHA - pac.FECHA_NACIMIENTO)/365 as EDAD, pac.NIF, pac.IDPACIENTEFAMILIA, p.FECHA from PETICION p
join PACIENTE pac on (p.IDPACIENTE = pac.IDPACIENTE) full outer join PACIENTE_FAMILIA pf on (pac.IDPACIENTEFAMILIA = pf.IDPACIENTEFAMILIA)"""

tabla1 = pd.read_sql(query1, GestCon)
tabla2 = pd.read_sql(query2, GestCon)

PeticionesTabla = pd.merge(Peticiones, tabla1, on='PETICIONCB', how='inner')
PacientesTabla = pd.merge(PeticionesTabla[['IDPACIENTE']].drop_duplicates(), tabla2, on='IDPACIENTE', how='inner')
PacientesTabla = PacientesTabla.drop_duplicates()
PacyPet = PeticionesTabla[['PETICIONCB', 'IDPACIENTE']]

PacientesTabla = PacientesTabla.replace({'': 'NULL'})
PacientesTabla = PacientesTabla.replace({' ': 'NULL'})
PacientesTabla = PacientesTabla.replace({'-': 'NULL'})
PacientesTabla = PacientesTabla.replace({None: "NULL"})
PacientesTabla = PacientesTabla.replace({'*': "NULL"})
PacientesTabla = PacientesTabla.replace({'"': ''})
PacientesTabla = PacientesTabla.replace({"'": ''})

for row in PacientesTabla.values.tolist():

    values = row
    values = tuple(values)

    IDPACIENTE = values[0]
    if IDPACIENTE != "NULL":
        IDPACIENTE = "'" + str(IDPACIENTE) + "'"
    IDEXTERNO = values[1]
    if IDEXTERNO != "NULL":
        IDEXTERNO = "'" + IDEXTERNO + "'"
    APELLIDOS = values[2]
    if APELLIDOS != "NULL":
        APELLIDOS = "'" + str(APELLIDOS) + "'"
    NOMBRE = values[3]
    if NOMBRE != "NULL":
        NOMBRE = "'" + str(NOMBRE) + "'"
    FECHA_NACIMIENTO = values[4]
    if FECHA_NACIMIENTO != "NULL":
        FECHA_NACIMIENTO = "'" + str(FECHA_NACIMIENTO) + "'"
    SEXO = values[5]
    if SEXO != "NULL":
        SEXO = "'" + SEXO + "'"
    EDAD = values[6]
    if EDAD != "NULL":
        EDAD = "'" + str(EDAD) + "'"
    NIF = values[7]
    if NIF != "NULL":
        NIF = "'" + NIF + "'"
    idFAMILIA = values[8]
    if idFAMILIA != "NULL":
        idFAMILIA = "'" + str(idFAMILIA) + "'"
    FECHA_RECEPCION = values[9]
    if FECHA_RECEPCION != "NULL":
        FECHA_RECEPCION = "'" + str(FECHA_RECEPCION) + "'"

    where = ("IDPACIENTE = %s, APELLIDOS = %s, NOMBRE = %s" % (IDPACIENTE, APELLIDOS, NOMBRE))
    fields = ('IDPACIENTE', 'IDEXTERNO', 'APELLIDOS', 'NOMBRE', 'NIF', 'SEXO', 'EDAD', 'FECHA_NACIMIENTO', 'FECHA_RECEPCION',
              'idFAMILIA')

    value_list = "(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)" % (IDPACIENTE, IDEXTERNO, APELLIDOS, NOMBRE, NIF,
    SEXO, EDAD, FECHA_NACIMIENTO, FECHA_RECEPCION, idFAMILIA)

    insert = dbArrayLocal.insert_update('PACIENTES', fields, value_list, where)
    Count = Count+1

    dbArrayLocal.commit()
    print(insert)
    if insert == -1:
        lista_errores.append(IDPACIENTE)

    dbArrayLocal.commit()

print(lista_errores)
print('Numero de inserts hechos: '+str(Count))
lista_errores_df = pd.DataFrame(lista_errores)
lista_errores_df.to_excel("/Users/rnavarro/Desktop/ResultadosSQL/ListaErroresPaci.xls")

PeticionesTabla = PeticionesTabla.replace({'': 'NULL'})
PeticionesTabla = PeticionesTabla.replace({' ': 'NULL'})
PeticionesTabla = PeticionesTabla.replace({'-': 'NULL'})
PeticionesTabla = PeticionesTabla.replace({None: "NULL"})
PeticionesTabla = PeticionesTabla.replace({'*': "NULL"})
PeticionesTabla = PeticionesTabla.replace({'"': ''})
PeticionesTabla = PeticionesTabla.replace({"'": ''})

for row in PeticionesTabla.values.tolist():

    values = row
    values = tuple(values)

    PETICIONCB = values[0]
    if PETICIONCB != "NULL":
        PETICIONCB = "'" + PETICIONCB + "'"
    IDPRUEBA = values[1]
    if IDPRUEBA != "NULL":
        IDPRUEBA = "'" + str(IDPRUEBA) + "'"
    IDPETICION = values[2]
    if IDPETICION != "NULL":
        IDPETICION = "'" + str(IDPETICION) + "'"
    IDLABEXTERNO = values[3]
    if IDLABEXTERNO != "NULL":
        IDLABEXTERNO = "'" + str(IDLABEXTERNO) + "'"
    IDPACIENTE = values[4]
    if IDPACIENTE != "NULL":
        IDPACIENTE = "'" + str(IDPACIENTE) + "'"
    FECHA_TOMA = values[5]
    if FECHA_TOMA != "NULL":
        FECHA_TOMA = "'" + str(FECHA_TOMA) + "'"
    FECHA_ALTA = values[6]
    if FECHA_ALTA != "NULL":
        FECHA_ALTA = "'" + str(FECHA_ALTA) + "'"
    COMENTARIO = values[7]
    if COMENTARIO != "NULL":
        COMENTARIO = COMENTARIO.replace("'", "")
        COMENTARIO = "'" + COMENTARIO + "'"
    PROCEDENCIAid = values[8]
    if PROCEDENCIAid != "NULL":
        PROCEDENCIAid = "'" + str(PROCEDENCIAid) + "'"
    TRESULTADO = values[9]
    if TRESULTADO != "NULL":
        TRESULTADO = TRESULTADO.replace("'", "")
        TRESULTADO = "'" + TRESULTADO + "'"

    # Where y whereUpdate son dos vairiables que deternminan que se va a modificar en el update

    where = ("PETICIONCB = %s, IDPETICION = %s, FECHA_TOMA =%s, FECHA_ALTA = %s" % (
        PETICIONCB, IDPETICION, FECHA_TOMA, FECHA_ALTA))
    fields = ('PETICIONCB', 'IDPRUEBA', 'IDPETICION', 'IDLABEXTERNO', 'IDPACIENTE', 'FECHA_TOMA', 'FECHA_ALTA', 'COMENTARIO',
              'PROCEDENCIAid', 'TRESULTADO')

    value_list = "(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)" % (
        PETICIONCB, IDPRUEBA, IDPETICION, IDLABEXTERNO, IDPACIENTE, FECHA_TOMA, FECHA_ALTA, COMENTARIO, PROCEDENCIAid,
        TRESULTADO)

    insert = dbArrayLocal.insert_update('PETICIONES', fields, value_list, where)
    Count = Count+1

    dbArrayLocal.commit()
    print(insert)
    if insert == -1:
        lista_errores.append(PETICIONCB)

    dbArrayLocal.commit()

print(lista_errores)
print('Numero de inserts hechos: '+str(Count))
lista_errores_df = pd.DataFrame(lista_errores)
lista_errores_df.to_excel("/Users/rnavarro/Desktop/ResultadosSQL/PeticionesTablas.xls")

PacyPet = PacyPet.replace({'': 'NULL'})
PacyPet = PacyPet.replace({' ': 'NULL'})
PacyPet = PacyPet.replace({'-': 'NULL'})
PacyPet = PacyPet.replace({None: "NULL"})
PacyPet = PacyPet.replace({'*': "NULL"})
PacyPet = PacyPet.replace({'"': ''})
PacyPet = PacyPet.replace({"'": ''})

for row in PacyPet.values.tolist():

    values = row
    values = tuple(values)

    PETICIONCB = values[0]
    if PETICIONCB != "NULL":
        PETICIONCB = "'" + PETICIONCB + "'"
    IDPACIENTE = values[1]
    if IDPACIENTE != "NULL":
        IDPACIENTE = "'" + str(IDPACIENTE) + "'"

    where = ("PETICIONCB = %s, IDPACIENTE = %s" % (PETICIONCB, IDPACIENTE))
    fields = ('PETICIONCB', 'IDPACIENTE')

    value_list = "(%s,%s)" % (PETICIONCB, IDPACIENTE)

    insert = dbArrayLocal.insert_update('PACIENTE_PETICION', fields, value_list, where)
    Count = Count+1

    dbArrayLocal.commit()
    print(insert)
    if insert == -1:
        lista_errores.append(PETICIONCB)

    dbArrayLocal.commit()

print(lista_errores)
print('Numero de inserts hechos: '+str(Count))
lista_errores_df = pd.DataFrame(lista_errores)
lista_errores_df.to_excel("/Users/rnavarro/Desktop/ResultadosSQL/ListaErroresPacyPet.xls")




