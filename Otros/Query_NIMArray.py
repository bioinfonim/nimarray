import mysql.connector
import pandas as pd

connection = mysql.connector.connect(host='bioinfodb.nimgenetics.com',user='nimarray', password='nimarray',database='NIMArray')


# select = """
# SELECT * FROM NIMArray.VARIANTES_INFORMADAS WHERE TestType = 21 and Analizado = 'Analizado' and PETICIONCB = '18NR9456'
# """
# select = """
# SELECT distinct PETICIONCB as total_pacientes from VARIANTES_INFORMADAS
# """

# select = """
# SELECT COUNT(DISTINCT PETICIONCB) FROM NIMArray.VARIANTES_INFORMADAS WHERE TestType = 353
# """
select = """
SELECT * FROM VARIANTES_INFORMADAS where Analizado = 'Analizado' AND TestType = 104 AND (Categorization = 'Patogénica' OR Categorization = 'Probablemente Patogénica');
"""

query_nimarray = select
cursor = connection.cursor()
cursor.execute(query_nimarray)
rows = cursor.fetchall()
names = [x[0] for x in cursor.description]
query_nimarray = pd.DataFrame(rows,columns=names)
print(query_nimarray)


#
query_nimarray.to_excel('/Users/jgonzalez/Desktop/Instert_NIMArray/septiembre/Autismo_Patogenicas.xlsx')
# resultado = pd.DataFrame()
#
# for row in query_nimarray.values.tolist():
#     values = row
#     values = tuple(values)
#
#     start = values[4]
#     print(start)
#     start = int(start)
#     stop = values[5]
#     stop = int(stop)
#     print(stop)
#     amp = values[7]
#     amp = float(amp)
#     del_ = values[6]
#     del_ = float(del_)
#     print(del_)
#
#     select1 = """
#     SELECT * FROM NIMArray.VARIANTES_INFORMADAS WHERE PositionStart = %s and PositionStop = %s and Amplification = %s and ProbesNum= %s
#     """
#
#     query_nimarray = select1 % (start,stop,amp, del_)
#     cursor = connection.cursor()
#     cursor.execute(query_nimarray)
#     rows = cursor.fetchall()
#     names = [x[0] for x in cursor.description]
#     query_nimarray = pd.DataFrame(rows,columns=names)
#     print(query_nimarray)
#     resultado = resultado.append(query_nimarray)
#
#
# resultado.to_excel('/Users/jgonzalez/Desktop/Instert_NIMArray/prenatales_nimarray_comparativa_del_.xlsx')
#
