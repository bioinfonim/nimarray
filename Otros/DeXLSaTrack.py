# Este script carga un excel con los resultados de ArrayCGH (Formato similar a destacados) y devuelve un Track
# que se puede cargar en cytogenomics.

import pandas as pd
from nimapidb.db import dbConnect

res = 15000
count = 0
Track = [[]]
Track3 = []
ListaTrack = []
ListaFinal = []
ListaPercent = []
ColumnasTrackLaura = [u'title', u'start', u'', u'', u'', u'', u'', u'', u'', u'', u'', u'']

columnas = ['Chr', 'Start', 'End', 'NAME', 'Syndrome|OMIM|Inheritance|CNVType', 'Chr_', 'Start_', 'End_',
            'Score', 'Strand', 'Thick_start', 'Thick_end', 'ITEM_RGB', 'Block_count', 'Block_size', 'Block_starts',
            'Exp_count', 'Exp_Ids', 'Exp_scores']
def enResol(filamia, filalista):

    CheckChr = filalista[3] == filamia[3]
    CheckBand = filalista[4] == filamia[4]
    CheckStart = (filalista[5] > filamia[5] - res) and (filalista[5] < filamia[5] + res)
    CheckStop = (filalista[6] > filamia[6] - res) and (filalista[6] < filamia[6] + res)
    enRes = (CheckChr and CheckBand and CheckStart and CheckStop)

    return enRes
def decidecolor(color, number):
    if color != 'Exception':
        if 0.01 <= number <= 0.19:
            color = color+'Claro'

        if 0.2 <= number <= 0.43:
            color = color+'Intermedio'

        if 0.44 <= number <= 1.69:
            color = color+'Oscuro'

    color = int(DicColor[color])

    return color
DicCategorias = {
    'Patogenica': u'Rojo',
    'Probablemente Patogenica': u'Naranja',
    'VUS': u'Amarillo',
    'Probablemente Benigna': u'Verde',
    'Benigna': u'Azul',
    'Basurilla-No Real': u'Morado',
    'Hallazgo incidental': u'Granate',
    'Heredado': u'AzulNav',
    'nan': u'Gris',
    'Exception': u'Exception'
}
DicColor = {
    'RojoClaro':u'16751001',
    'RojoIntermedio':u'16724787',
    'RojoOscuro':u'10027008',
    'NaranjaClaro':u'16764057',
    'NaranjaIntermedio':u'16750899',
    'NaranjaOscuro':u'13395456',
    'AmarilloClaro':u'16777113',
    'AmarilloIntermedio':u'16777011',
    'AmarilloOscuro':u'13421568',
    'VerdeClaro':u'10092441',
    'VerdeIntermedio':u'39168',
    'VerdeOscuro':u'26112',
    'AzulClaro':u'10079487',
    'AzulIntermedio':u'3381759',
    'AzulOscuro':u'19609',
    'MoradoClaro':u'13408767',
    'MoradoIntermedio':u'10040319',
    'MoradoOscuro':u'4980889',
    'GranateClaro':u'16751052',
    'GranateIntermedio':u'16711807',
    'GranateOscuro':u'6684723',
    'AzulNavClaro':u'10066431',
    'AzulNavIntermedio':u'3355647',
    'AzulNavOscuro':u'153',
    'GrisClaro':u'12632256',
    'GrisIntermedio':u'8421504',
    'GrisOscuro':u'4210752',
    'Exception':u'9672999'
}

ExTrack = pd.read_excel("/Users/rnavarro/Desktop/Archive/Resultados/DuplicadosRevRafayLaura.xls", 'Excepciones')
ExTrack = ExTrack.values.tolist()
ExTrackCopia = ExTrack.copy()

db = dbConnect.dbConnect('LocalHost', 'root', 'Password.10', 'NIMArray')  # Conecta a la Base de Datos; IP Rafa Varía
variant_infor = db.select_df_simple('VARIANTES_INFORMADASDOS')            # Saco la Tabla
Analizado = variant_infor['Analizado'] == 'Analizado'                     # Filtro por Analizado
variant_infor = variant_infor[Analizado]
ListaTodos = variant_infor.values.tolist()

while len(ExTrackCopia) > 0:
    filamia = ExTrackCopia[0]
    ExTrackCopia.remove(filamia)
    NuevaLista = []
    NuevaLista.append(filamia)

    for filalista in ListaTodos:
        if enResol(filamia, filalista):
            NuevaLista.append(filalista)
            ListaTodos.remove(filalista)

    if len(NuevaLista) > 1:
        NuevaLista[0].append(len(NuevaLista))
        ListaFinal = NuevaLista+ListaFinal
    else:
        NuevaLista[0].append(len(NuevaLista))
        ListaFinal = ListaFinal+NuevaLista

df = pd.DataFrame(ListaFinal, columns=['PETICIONCB', 'AberrationNo', 'Chr', 'Cytoband', 'PositionStart', 'PositionStop',
                           'ProbesNum', 'Amplification', 'Deletion', 'pval', 'GeneNames', 'Analizado', 'Tamano', 'Categorization', 'CopyNumber',
                           'Coments', 'Coordenadas', 'Gain', 'Loss', 'TRESULTADO', 'FECHA_REGISTRO', 'FECHA_RESULTADO', 'nº reps'])

ExcepTrack = df.values.tolist()
ExcepTrackCopia = ExcepTrack.copy()

for row in ExcepTrack:
    if str(row[22]) == 'nan':
        ExcepTrackCopia.remove(row)
    else:
        ExcepTrackCopia.remove(row)
        row[13] = 'Exception'
        ExcepTrackCopia.append(row)

df = pd.DataFrame(ExcepTrackCopia, columns=['PETICIONCB', 'AberrationNo', 'Chr', 'Cytoband', 'PositionStart', 'PositionStop',
                           'ProbesNum', 'Amplification', 'Deletion', 'pval', 'GeneNames', 'Analizado', 'Tamano',
                           'Categorization', 'CopyNumber',
                           'Coments', 'Coordenadas', 'Gain', 'Loss', 'TRESULTADO', 'FECHA_REGISTRO', 'FECHA_RESULTADO',
                           'nº reps'])
df.to_excel("/Users/rnavarro/Desktop/Archive/Resultados/ExcepTrack.xls", index=False)

for row in ExcepTrackCopia:

    ListaTrack = []

    if (str(row[14]) == 'None') or (str(row[14]) == 'nan'):
        CNVType = ''
    else:
        CNVType = row[14]

    if (str(row[10]) == 'None') or (str(row[10]) == 'nan'):
        GEN = ''
    else:
        GEN = row[10]

    if (str(row[15]) == 'None') or (str(row[15]) =='nan'):
        coments = '---'
    else:
        coments = row[15]

    percent = round((row[22] / 8437) * 100, 2)
    Color = decidecolor(DicCategorias[str(row[13])], percent)

    # Track Version como me lo han pasado Laura y Sara

    ListaTrack.append(row[2])
    ListaTrack.append(row[4])
    ListaTrack.append(row[5])
    ListaTrack.append('Coordinates: ' + str(row[2]) + ':' + str(row[4]) + '-' + str(row[5]) + '|Size: ' + str(
        abs(row[4] - row[5])) + '|CNVType: ' + CNVType + '|OMIM: ' + GEN + '|Ocurrence (8437): ' + str(
        percent) + '% |' + 'coments: '+coments+' |')
    ListaTrack.append(1000)
    ListaTrack.append('+')
    ListaTrack.append(row[4])
    ListaTrack.append(row[5])
    ListaTrack.append(Color)
    ListaTrack.append('1')
    ListaTrack.append(str(abs(row[4] - row[5])))
    ListaTrack.append('0')

    Track.append(ListaTrack)
    ListaPercent.append(percent)

Track.pop(0)

df = pd.DataFrame(Track, columns=ColumnasTrackLaura)
df = df.sort_values(by=[u'title', u'start'])

Track2 = df.values.tolist()

for row in Track2:
    if row[0][3] == '0':
        row[0] = row[0][:3]+row[0][4]
        Track3.append(row)
    else:
        Track3.append(row)

ListaTrack = []
ListaTrack.append('chr1')
ListaTrack.append('')
ListaTrack.append('')
ListaTrack.append('Null')
ListaTrack.append('')
ListaTrack.append('')
ListaTrack.append('')
ListaTrack.append('')
ListaTrack.append('')
ListaTrack.append('')
ListaTrack.append('')
ListaTrack.append('')

Track3.insert(0, ListaTrack)

ListaTrack = []
ListaTrack.append('')
ListaTrack.append('')
ListaTrack.append('')
ListaTrack.append('')
ListaTrack.append('')
ListaTrack.append('')
ListaTrack.append('')
ListaTrack.append('')
ListaTrack.append('')
ListaTrack.append('')
ListaTrack.append('')
ListaTrack.append('')

Track3.insert(0, ListaTrack)

df = pd.DataFrame(Track3, columns=[u'title', u'', u'', u'', u'', u'', u'', u'', u'', u'', u'', u''])

df.to_csv("/Users/rnavarro/Desktop/Archive/Resultados/ExcepTrack.bed", sep='\t', index=False, encoding='latin-1')

print('END')



