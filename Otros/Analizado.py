from __future__ import unicode_literals
import pandas as pd
import numpy as np
import io
from xlrd import XLRDError
from xlwt import Workbook
from os import listdir
from os.path import isfile, join

mypath = '/Users/rnavarro/Desktop/Resultados/ConcatFiles/Todos Juntos'

ExFile = pd.ExcelFile(mypath + '/' + 'TodosJuntosPruebas.xls')

HojaFinal = pd.read_excel(ExFile, 0, encoding='utf-8')

HojaFinal['Analizado'] = HojaFinal['Categorization']
HojaFinal['Analizado'] = np.where(HojaFinal['Analizado'] == HojaFinal['Analizado'], 'Analizado', HojaFinal['Analizado'])

HojaFinal.to_excel('/Users/rnavarro/Desktop/Resultados/ConcatFiles/Todos Juntos/TodosJuntosPruebas.xls',
                   sheet_name='TodosJuntos')