# -*- coding: utf-8 -*-

import pandas as pd

df_nimdb = pd.read_excel('/Users/rnavarro/Desktop/Resultados/ConcatFiles/Todos Juntos/Unidos2015Pruebas.xls',
                         'TodosJuntos')

df_anotation = pd.read_excel('/Users/rnavarro/Desktop/Arrays/Genomica_Gestlab_Interpretación.xlsx', 'Sheet1')

df_merge = df_nimdb.merge(df_anotation, on=['PETICIONCB'], how='outer')

df_merge.drop_duplicates(keep='first')

df_merge.to_excel('/Users/rnavarro/Desktop/Resultados/Resultado2015.xls', index=None, encoding='utf-8')


