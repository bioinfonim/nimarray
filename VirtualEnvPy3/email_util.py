import smtplib, ssl
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

message = MIMEMultipart("alternative")
message["Subject"] = "Nueva Versión de los Track de Genómica"


# Create the plain-text and HTML version of your message
html = """
<meta charset="UTF-8">
<h3>NUEVA VERSIÓN DE LOS TRACKS DE  GENÓMICA DISPONIBLE &#x1F600;&#x1F389;</h3>

<p><span style="text-decoration: underline;"><strong></strong></span></p>

<p style="padding-left: 30px;"><strong>&#x1F52C;&ensp;<span style="color: #68a2d8;"><em>Tracks actualizados para CytoGenomics Software 4.0 y CytoGenomics Software 5.0:</em>
</span></strong></p>
<ul style="line-height: 170%; padding-left: 100px;">
	<li>Track variantes Benignas</li>
	<li>Track variantes Patogénicas y Probablementa Patogénicas detectadas en arrays de Autismo</li>
	<li>Track variantes Patogénicas y Probablementa Patogénicas detectadas en arrays 60K</li>
	<li>Track variantes Patogénicas y Probablementa Patogénicas detectadas en arrays 180K</li>
	<li>Track variantes Patogénicas y Probablementa Patogénicas detectadas en arrays 400K</li>
	<li>Track variantes Patogénicas y Probablementa Patogénicas detectadas en arrays Prenatal</li>
</ul>

<p style="padding-left: 30px;"><strong>&ensp;<span style="color:#69b5b5;"><em>Descarga de los tracks:
</em></span></strong></p>
<ul style="line-height: 170%; padding-left: 100px;"><a href='http://dashboard.nimgenetics.com/' target='_blank'>http://dashboard.nimgenetics.com/</a></ul>

    <em><sup><b>*</b></sup>Los tracks de variantes genómicas son actualizados con una periodicidad de 6 meses (Enero y Julio). Para cualquiero consulta pónganse en contacto con bioinfo@nimgenetics.com</em>
    <br /><br />
</p>
"""

# Turn these into plain/html MIMEText objects
part2 = MIMEText(html, "html")

# Add HTML/plain-text parts to MIMEMultipart message
# The email client will try to render the last part first
message.attach(part2)

# Create secure connection with server and send email
context = ssl.create_default_context()
with smtplib.SMTP_SSL('smtp.office365.com', 587, context=context) as server:
    server.login('bioinfo@nimgenetics.com', 'Password.10')
    server.sendmail('bioinfo@nimgenetics.com', 'jgonzalez@nimgenetics.com', message.as_string())
 subject=self._email_subject)