# Este script carga cada excel de la carpeta de Arrays de 2015 y genera un documento final, en donde se recopilan
# todos los casos de la carpeta con el formato definido.

#!/usr/bin/env python3
# coding=utf-8

from __future__ import unicode_literals
import pandas as pd
import numpy as np
import io
import math
from xlrd import XLRDError
from xlwt import Workbook
from os import listdir
from os.path import isfile, join
import random

def TamCheck(Columna):
    """TamCheck es una función que determina si la columna pasada como variable es una columna que contenga el tamaño de
    una alteración. """
    suma = 0
    for x in range(Columna.shape[0]):
        if isinstance(Columna[x], float)and(not(math.isnan(Columna[x])))and(math.modf(Columna[x])[0] != 0):
            suma = suma+1
    try:
        return float(suma)/float(Columna.shape[0])
    except ZeroDivisionError:
        return 0
def CatCheck(Columna):
    """CatCheck es una función que determina si la columna pasada como variable es una columna que contenga el tipo de
    categoría. """
    suma = 0
    CatBank = [u'VOUS', u'BASURA', u'sign incierto', u'CNV', u'cnv', u'Polimorfismo', u'polimorfismo', u'benigna', u'Tama\xf1o', u'basurilla', u'POLIMORFISMO', u'basura']
    for x in range(Columna.shape[0]):
        if Columna[x] in CatBank:
            suma = suma+1
    try:
        return float(suma)/float(Columna.shape[0])
    except ZeroDivisionError:
        return 0
def CoordCheck(Columna):
    """CoordCheck es una función que determina si la columna pasada como variable es una columna que contenga las
    coordenadas de la alteración. """
    suma = 0
    for x in range(Columna.shape[0]):
        if isinstance(Columna[x], str)and(Columna[x][:3] == u'chr'):
            suma = suma+1
    try:
        return float(suma)/float(Columna.shape[0])
    except ZeroDivisionError:
        return 0
# Diccionario con el nombre de las columnas
DicNamCol = {
    '1': u'Tama\xf1o',
    '2': u'Categorization',
    '3': u'Coordenadas'
}
def Nombrar_Columnas(Hoja2):
    """Nombrar_Columnas utiliza las funciones definidas previamente para nombrar la columna pasada como variable. """
    Cnames = []
    bul = True
    H2Array = Hoja2.values

    for x in range(1, H2Array.shape[1]-1):
        Columna = H2Array[:, x]
        probs = [0, TamCheck(Columna), CatCheck(Columna), CoordCheck(Columna)]
        if (max(probs) > 0.25) and (max(probs) < 0.33):
            print('Caso Raro '+file)
        if max(probs) > 0.34:
            top = probs.index(max(probs))
        else:
            top = 0
        if top != 0:
            Cnames.append(DicNamCol[str(top)])
        elif bul:
            Cnames.append('Coments')
            bul = False
    for x in range(1, Hoja2.shape[1]-1):
        Hoja2.columns.values[x] = Cnames[x-1]

mypath = '/Users/rnavarro/Desktop/Duplicados y Distribuidos/Arrays2015Distribuidos'

files = [f for f in listdir(mypath) if isfile(join(mypath, f))]
files.sort()

if '.DS_Store' in files:
    files.remove('.DS_Store')

HojaFinal = pd.DataFrame()

# df.loc[df['LastName']=='Smith'].index
counter1 = 0
countNo2 = 0
countNo3 = 0

ListaEmpty = []
ListaSin2Hoja = []
ListaConcat = []
ListaEnElExcel = []
ListaExtranos = []

OldCols1 = ['PETICIONCB', u'AberrationNo', u'Chr', u'Cytoband', u'Start', u'Stop', u'#Probes', u'Amplification',
            u'Deletion', u'pval', u'Gene Names', u'Analizado', u'Tama\xf1o', u'Categorization', u'Copy Number',
            u'Coments', u'Coordenadas']
Compara = [u'AberrationNo', u'Tama\xf1o', u'Coordenadas', float('nan'), float('nan'), float('nan'), u'Analizado']

for file in files:

    counter1 = counter1 + 1
    print(file + ' file nª ' + str(counter1))

    # Ajusta el Raw data como lo queremos

    try:
        ExFile = pd.ExcelFile(mypath + '/' + file)
    except XLRDError:
        print(file + ' Corrupto o mal guardado, guardando de nuevo')
        file1 = io.open(mypath + '/' + file, "r", encoding="iso-8859-1")
        data = file1.readlines()
        xldoc = Workbook()
        sheet = xldoc.add_sheet("Sheet1", cell_overwrite_ok=True)
        for i, row in enumerate(data):
            for j, val in enumerate(row.replace('\n', '').split('\t')):
                sheet.write(i, j, val)
        xldoc.save(mypath + '/' + file)
        ExFile = pd.ExcelFile(mypath + '/' + file)

        # Esta sección de código le da formato definido a la priemra hoja del excel, el raw data
        # Utiliza la estructura de try/except para adaptarse al formato de cada excel.

    Hoja1 = pd.read_excel(ExFile, 0, encoding='utf-8')

    if len(ExFile.sheet_names) == 1:
        countNo2 = countNo2 + 1
        print('el archivo ' + file + ' No tiene segunda hoja: ' + str(countNo2))
        ListaSin2Hoja.append(file)
        continue

    if Hoja1.columns.to_list()[0] == u'Use In Export':  # PROVISIONAL FORMATO EXTRAÑO
        print('el archivo ' + file + ' Tiene un formato extraño')
        ListaExtranos.append(file)
        continue

    if (file == 'Report 3063.xls'): # FORMATO RARO
        continue

    row1 = 0
    try:
        row2 = Hoja1.loc[Hoja1['Genome: hg19'] == 'AberrationNo'].index[0]
    except IndexError:
        row2 = Hoja1.loc[Hoja1['Genome: hg19'] == 'Index'].index[0]
    except KeyError:
        try:
            row2 = Hoja1.loc[Hoja1['Aberration Algorithm: ADM-2'] == 'AberrationNo'].index[0]  # 19NR04965-2.xls
        except KeyError:
            row2 = Hoja1.loc[Hoja1['Aberration Algorithm: ADM-1'] == 'AberrationNo'].index[0]

    Hoja1 = Hoja1.drop(range(row1, row2), axis=0)
    try:
        row2 = Hoja1.loc[Hoja1['Genome: hg19'] == 'Number of calls per sample:'].index[0] + 2
        row1 = row2 - 4
    except IndexError:
        vector = Hoja1['Genome: hg19'] == Hoja1['Genome: hg19']
        vector = vector.to_list()
        vector = [i for i, x in enumerate(vector) if x]
        count = len(vector)
        Hoja1 = Hoja1.reset_index()
        Hoja1 = Hoja1.drop('index', axis=1)
        row2 = count
        row1 = count
    except KeyError:
        try:
            row2 = Hoja1.loc[Hoja1['Aberration Algorithm: ADM-2'] == 'Number of calls per sample:'].index[0] + 2
            row1 = row2 - 4
        except KeyError:
            row2 = Hoja1.loc[Hoja1['Aberration Algorithm: ADM-1'] == 'Number of calls per sample:'].index[0] + 2
            row1 = row2 - 4
    Hoja1 = Hoja1.drop(range(row1, row2), axis=0)
    Hoja1.insert(0, 'PETICIONCB', Hoja1.iloc[1][0])
    try:
        Hoja1 = Hoja1.drop(Hoja1.loc[Hoja1['Genome: hg19'] == 'AberrationNo'].index[0] + 1, axis=0)
    except IndexError:
        Hoja1 = Hoja1.drop(Hoja1.loc[Hoja1['Genome: hg19'] == 'Index'].index[0] + 1, axis=0)
    except KeyError:
        try:
            Hoja1 = Hoja1.drop(Hoja1.loc[Hoja1['Aberration Algorithm: ADM-2'] == 'AberrationNo'].index[0] + 1, axis=0)
        except KeyError:
            Hoja1 = Hoja1.drop(Hoja1.loc[Hoja1['Aberration Algorithm: ADM-1'] == 'AberrationNo'].index[0] + 1, axis=0)
    Hoja1 = Hoja1.reset_index()
    Hoja1 = Hoja1.drop('index', axis=1)
    vector = Hoja1.loc[0].values
    vector[0] = 'PETICIONCB'
    Hoja1.columns = vector
    Hoja1 = Hoja1.drop(0, axis=0)
    Hoja1 = Hoja1.reset_index()
    Hoja1 = Hoja1.drop('index', axis=1)

    # Esta sección le da el formato definido a la segunda hoja del excel, los resultados analizados.
    # Para la segunda hoja, el proceso es parecido. Se ecuentrar ciertos elementos en el excel y según estos puntos
    # las filas que sobran se eliminan.

    Hoja2 = pd.DataFrame()
    try:
        Hoja2 = pd.read_excel(ExFile, 1, encoding='utf-8')
        if Hoja2.values[15][0] == u'Observaciones':
            Orden = Hoja2.values[15][:].tolist()
            Hoja2.columns = Orden
            Orden.append(Orden.pop(Orden.index('Observaciones')))
            Hoja2 = Hoja2[Orden]
        HojaHelp = pd.read_excel(ExFile, 0, encoding='utf-8')
        if (Hoja2.columns.to_list()[0] != HojaHelp.columns.to_list()[0])or(Hoja2.columns.to_list()[0] == u'AberrationNo'):
            Hoja2.rename(columns={Hoja2.columns[0]: HojaHelp.columns[0]}, inplace=True)
        if u'Number of calls per sample:' in Hoja2['Genome: hg19'].to_list():
            row1 = Hoja2.loc[Hoja2['Genome: hg19'] == 'Number of calls per sample:'].index[0]
            row2 = row1 + 2
            Hoja2 = Hoja2.drop(range(row1, row2), axis=0)
            Hoja2 = Hoja2.reset_index()
            Hoja2 = Hoja2.drop('index', axis=1)
        row1 = 0
        row2 = Hoja2.loc[Hoja2['Genome: hg19'] == 'AberrationNo'].index[0]
        Hoja2 = Hoja2.drop(range(row1, row2), axis=0)
        Hoja2 = Hoja2.drop(row2 + 1, axis=0)
        vector = Hoja2['Genome: hg19'] == Hoja2['Genome: hg19']
        vector = vector.to_list()
        vector = [i for i, x in enumerate(vector) if x]
        count = len(vector)
        Hoja2 = Hoja2.reset_index()
        Hoja2 = Hoja2.drop('index', axis=1)
        row1 = count
        row2 = Hoja2.shape[0]
        Hoja2 = Hoja2.drop(range(row1, row2), axis=0)
        Hoja2 = Hoja2.reset_index()
        Hoja2 = Hoja2.drop('index', axis=1)
        vector = Hoja2.loc[0].values
        Hoja2.columns = vector
        Hoja2 = Hoja2.drop(0, axis=0)
        Hoja2 = Hoja2.reset_index()
        Hoja2 = Hoja2.drop('index', axis=1)
    except KeyError:
        try:
            Hoja2 = pd.read_excel(ExFile, 1, encoding='utf-8')
            if Hoja2.values[15][0] == u'Observaciones':
                Orden = Hoja2.values[15][:].tolist()
                Hoja2.columns = Orden
                Orden.append(Orden.pop(Orden.index('Observaciones')))
                Hoja2 = Hoja2[Orden]
            HojaHelp = pd.read_excel(ExFile, 0, encoding='utf-8')
            if (Hoja2.columns.to_list()[0] != HojaHelp.columns.to_list()[0])or(Hoja2.columns.to_list()[0] == u'AberrationNo'):
                Hoja2.rename(columns={Hoja2.columns[0]: HojaHelp.columns[0]}, inplace=True)
            if u'Number of calls per sample:' in Hoja2['Aberration Algorithm: ADM-2'].to_list():
                row1 = Hoja2.loc[Hoja2['Aberration Algorithm: ADM-2'] == 'Number of calls per sample:'].index[0]
                row2 = row1 + 2
                Hoja2 = Hoja2.drop(range(row1, row2), axis=0)
                Hoja2 = Hoja2.reset_index()
                Hoja2 = Hoja2.drop('index', axis=1)
            row1 = 0
            row2 = Hoja2.loc[Hoja2['Aberration Algorithm: ADM-2'] == 'AberrationNo'].index[0]
            Hoja2 = Hoja2.drop(range(row1, row2), axis=0)
            Hoja2 = Hoja2.drop(row2 + 1, axis=0)
            vector = Hoja2['Aberration Algorithm: ADM-2'] == Hoja2['Aberration Algorithm: ADM-2']
            vector = vector.to_list()
            vector = [i for i, x in enumerate(vector) if x]
            count = len(vector)
            Hoja2 = Hoja2.reset_index()
            Hoja2 = Hoja2.drop('index', axis=1)
            row1 = count
            row2 = Hoja2.shape[0]
            Hoja2 = Hoja2.drop(range(row1, row2), axis=0)
            Hoja2 = Hoja2.reset_index()
            Hoja2 = Hoja2.drop('index', axis=1)
            vector = Hoja2.loc[0].values
            Hoja2.columns = vector
            Hoja2 = Hoja2.drop(0, axis=0)
            Hoja2 = Hoja2.reset_index()
            Hoja2 = Hoja2.drop('index', axis=1)
        except KeyError:
            Hoja2 = pd.read_excel(ExFile, 1, encoding='utf-8')
            if Hoja2.values[15][0] == u'Observaciones':
                Orden = Hoja2.values[15][:].tolist()
                Hoja2.columns = Orden
                Orden.append(Orden.pop(Orden.index('Observaciones')))
                Hoja2 = Hoja2[Orden]
            HojaHelp = pd.read_excel(ExFile, 0, encoding='utf-8')
            if (Hoja2.columns.to_list()[0] != HojaHelp.columns.to_list()[0])or(Hoja2.columns.to_list()[0] == u'AberrationNo'):
                Hoja2.rename(columns={Hoja2.columns[0]: HojaHelp.columns[0]}, inplace=True)
            if u'Number of calls per sample:' in Hoja2['Aberration Algorithm: ADM-1'].to_list():
                row1 = Hoja2.loc[Hoja2['Aberration Algorithm: ADM-1'] == 'Number of calls per sample:'].index[0]
                row2 = row1 + 2
                Hoja2 = Hoja2.drop(range(row1, row2), axis=0)
                Hoja2 = Hoja2.reset_index()
                Hoja2 = Hoja2.drop('index', axis=1)
            row1 = 0
            row2 = Hoja2.loc[Hoja2['Aberration Algorithm: ADM-1'] == 'AberrationNo'].index[0]
            Hoja2 = Hoja2.drop(range(row1, row2), axis=0)
            Hoja2 = Hoja2.drop(row2 + 1, axis=0)
            vector = Hoja2['Aberration Algorithm: ADM-1'] == Hoja2['Aberration Algorithm: ADM-1']
            vector = vector.to_list()
            vector = [i for i, x in enumerate(vector) if x]
            count = len(vector)
            Hoja2 = Hoja2.reset_index()
            Hoja2 = Hoja2.drop('index', axis=1)
            row1 = count
            row2 = Hoja2.shape[0]
            Hoja2 = Hoja2.drop(range(row1, row2), axis=0)
            Hoja2 = Hoja2.reset_index()
            Hoja2 = Hoja2.drop('index', axis=1)
            vector = Hoja2.loc[0].values
            Hoja2.columns = vector
            Hoja2 = Hoja2.drop(0, axis=0)
            Hoja2 = Hoja2.reset_index()
            Hoja2 = Hoja2.drop('index', axis=1)

    # En este punto, se unen las dos hojas con el formato final
    # Elimino las columnas de la hoja2 y me facilito el merge


    H1cols = Hoja1.columns.to_list()
    del H1cols[0:2]
    H1cols = list(set(Hoja2.columns.to_list()).intersection(H1cols))
    Hoja2 = Hoja2.drop(H1cols, axis=1)
    Hoja2.insert(Hoja2.shape[1], 'Analizado', 'Analizado')

    # Une las dos hojas utilizando la función Nombrar_Columnas para detemrinar cuál es cada columna y que sea más
    # sencillo después unir ambas hojas.

    try:
        if isinstance(Hoja1['Start'][0], str):
            Hoja2 = Hoja2.drop(['Start', 'Stop'], axis=1)
            print('Start en Letras')
        elif (Hoja2.shape[1] == 7) and (str(Hoja2.columns.to_list()) == str(Compara)):
            Hoja2.columns.values[1] = 'Tamaño'
            Hoja2.columns.values[2] = 'Coordenadas'
            Hoja2.columns.values[3] = 'Categorization'
            Hoja2.columns.values[4] = 'Coments'
            Hoja2.columns.values[5] = 'Coments2'
            Hoja2['Coments'] = Hoja2['Coments']+' / '+Hoja2['Coments2']  # Modificación para 2018
            Hoja2 = Hoja2.drop('Coments2', axis=1)
        elif Hoja2.shape[1] < 7:
            Nombrar_Columnas(Hoja2)
        result = pd.merge(Hoja1, Hoja2, how='outer')
        result = result.sort_values(by=['AberrationNo'])
        print('unido en tryMerge')
    except IndexError:
        result = pd.merge(Hoja1, Hoja2, how='outer')
        result = result.sort_values(by=['AberrationNo'])
        print('Merge en el except Index')
        if (Hoja2.values.size == 0)and(Hoja1.values.size == 0):
            print('Hoja 1 y 2 vacía')
            ListaEmpty.append(file)

        # Una vez las dos hojas está unidas (result), se concatenan con la hojas previas en HojaFinal

    Anadir = list(set(OldCols1) - set(result.columns.to_list()))
    for x in Anadir:
        result.insert(result.shape[1], x, '')
    result = result[OldCols1]
    HojaFinal = pd.concat([HojaFinal, result], sort=False)
    print('Concat En el < 17')
    ListaConcat.append(file)

print('Sin segunda Hoja: ' + str(countNo2))
print('Con segunda Hoja pero vacía: ' + str(countNo3))

HojaFinal.to_excel('/Users/rnavarro/Desktop/Resultados/ConcatFiles/Todos Juntos/Unidos2015Pruebas.xls',
                   sheet_name='TodosJuntos')

# --------------------------------------AQUÍ TERMINA EL PROCESO --------------------------------------------------
# EL resto del código comprueba el número de archivos unidos para determinar si todos los de la carpeta son incluidos

ListaEnElExcel = HojaFinal[HojaFinal.columns[0]]
ListaEnElExcel = ListaEnElExcel.to_list()
ListaEnElExcel = list(dict.fromkeys(ListaEnElExcel))
ListaEnElExcel.sort()

ListaConcat = list(dict.fromkeys(ListaConcat))
ListaConcat.sort()

ListaConcatTrans = []
ListaConcatTrans2 = []

for x in ListaConcat:
    if x[-11:] == ' report.xls':
        x = x[:-11]
        ListaConcatTrans.append(x)
    elif x[-9:] == '.xls.xlsx':
        x = x[:-9]
        ListaConcatTrans.append(x)
    elif x[-9:] == '.txt.xlsx':
        x = x[:-9]
        ListaConcatTrans.append(x)
    elif x[-8:] == '.txt.xls':
        x = x[:-8]
        ListaConcatTrans.append(x)
    elif x[-5:] == '.xlsx':
        x = x[:-5]
        ListaConcatTrans.append(x)
    elif x[-4:] == '.xls':
        x = x[:-4]
        ListaConcatTrans.append(x)
    else:
        ListaConcatTrans.append(x)

for x in ListaConcatTrans:
    if x[:9] == 'Report 18':
        x = x[7:]
        ListaConcatTrans2.append(x)
    elif x[:7] == 'Report ':
        x = '19NR' + x[7:]
        ListaConcatTrans2.append(x)
    else:
        ListaConcatTrans2.append(x)

ListaConcatTrans2.sort()

IntersectionList = list(set(ListaConcatTrans2) - set(ListaEnElExcel))
IntersectionList.sort()
print('Concatenados menos Excel: ')
print(IntersectionList)

ListaCony2h = ListaConcat + ListaSin2Hoja
ListaCony2h.sort()

mypath1 = '/Users/rnavarro/Desktop/Duplicados y Distribuidos/Arrays2015Distribuidos'
ListaFiles = [f for f in listdir(mypath1) if isfile(join(mypath1, f))]
ListaFiles.sort()

print('longitud ListaEnElExcel ' + str(len(ListaEnElExcel)))
print('longitud lista Concatenados ' + str(len(ListaConcat)))
print('longitud lista Unidos ' + str(len(ListaCony2h)))
print('longitud lista Todos los Archivos ' + str(len(ListaFiles)))
print('longitud lista vacíos ' + str(len(ListaEmpty)))
print('longitud lista sin 2 Hoja '+str(len(ListaSin2Hoja)))

IList = list(set(ListaFiles) - set(ListaCony2h))
print('longitud lista IList ' + str(len(IList)))
print(IList)
print('longitud lista vacíos ' + str(len(ListaEmpty)))
print(ListaEmpty)

sample = random.sample(ListaFiles, k=25)

print('----------FINAL 2015------------------')