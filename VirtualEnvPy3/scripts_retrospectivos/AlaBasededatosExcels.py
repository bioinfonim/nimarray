# Este Script carga los valores del excel leido en variant_infor a la basde de datos de NIMArray
# This script loads the excel aberrations read in variant_infor to the data base . It also goes over all PETICIONCB on the
# excel, finds in GestLab the patient and petition information and adds it to NIMArray.

import fdb
import pandas as pd
from dbconnect.dbConnect import dbConnect
from termcolor import colored
import os
import configparser

ERRORES = pd.DataFrame()
ERRORES['Fecha'] = ""
Count = 0

config_parser = configparser.ConfigParser(allow_no_value=True)
bindir = os.path.abspath(os.path.dirname(__file__))
config_parser.read(bindir + "/cfg.cnf")

nimarray_excels_logs = config_parser.get("PATHS", "nimarray_excels_logs")
path = '/Users/rnavarro/Desktop/2019/UpdateSQLLaura.xls'
insertPath = '/Users/rnavarro/Desktop/2019/InsertSQL.xls'

# Paths to the excels to include in the database
# Excelinsert: /Users/rnavarro/Desktop/2019/InsertSQL.xls
# ExcelUpdate: /Users/rnavarro/Desktop/2019/UpdateSQL.xls
# ExcelRevisadoUpdate: /Users/rnavarro/Desktop/2019/UpdateSQLLaura.xls

# DataBase Connection
host = config_parser.get("DB", "hostname")
db = config_parser.get("DB", "db")
user = config_parser.get("DB", "user")
password = config_parser.get("DB", "password")
localhost = config_parser.get("LOCAL DB", "hostname")
localdb = config_parser.get("LOCAL DB", "db")
localuser = config_parser.get("LOCAL DB", "user")
localPassword = config_parser.get("LOCAL DB", "password")
dsn_gestlab = config_parser.get("GESTLAB", "dsn")
user_gestlab = config_parser.get("GESTLAB", "user")
password_gestlab = config_parser.get("GESTLAB", "password")

# dbArrayCerebro = dbConnect(host, user, password, db)
dbArrayLocal = dbConnect(localhost, localuser, localPassword, localdb)
GestCon = fdb.connect(dsn=dsn_gestlab, user=user_gestlab, password=password_gestlab, charset='ISO8859_1')

# Choose excel to get data from

variant_infor = pd.read_excel(path, 'Sheet1')
# variant_infor = pd.read_excel(path, 'Sheet1')
# variant_infor = pd.read_excel(path, 'Sheet1')

# Modifies elements in the table so its format is SQL compatible. Done at this point to accelerate the process and
# avoid the repetition if these calls.

variant_infor = variant_infor.replace({'': 'NULL'})
variant_infor = variant_infor.replace({' ': 'NULL'})
variant_infor = variant_infor.replace({'-': 'NULL'})
variant_infor = variant_infor.replace({None: "NULL"})
variant_infor = variant_infor.replace({'"': ''})
variant_infor = variant_infor.replace({"'": ''})

# Obtains number of columns from the VARIANTES_INFORMADAS, if it is 19 adds the TestType column.
# Included since when the first time the database was built, TestType was not incorporated. This way when adding
# the alterations from the yearly excels it is ensured that the column is added.

sql_command = """SELECT count(*) FROM information_schema.columns WHERE table_name = 'VARIANTES_INFORMADAS';"""
dbcursor = dbArrayLocal.db.cursor()
dbcursor.execute(sql_command)
cols = dbcursor.fetchall()[0][0]

if cols == 19:
    sql_command = """ALTER TABLE NIMArray.VARIANTES_INFORMADAS ADD TestType varchar(20);"""
    dbArrayLocal.execute(sql_command)

# Obtains petitionlist from the excel

Peticiones = list(set(variant_infor['PETICIONCB']))
Peticiones.sort()

# For each PETICIONCB adds the patient and the petition to the database searching for it in GestLab. Afterwards adds
# the alterations from the excel to the database.

for peticion in Peticiones:

    print(peticion)

    select1 = """select p.PETICIONCB, pru.IDPRUEBA, p.IDPETICION, p.IDLABORATORIO, p.IDPACIENTE, p.FECHA_TOMA, p.FECHA_ALTA, 
    p.COMENTARIO, p.IDPROCEDENCIA, pp.TRESULTADO from PETICION p join PET_PRUEBA pp on (p.IDPETICION = pp.IDPETICION)
    join PRUEBA pru on (pp.IDPRUEBA = pru.IDPRUEBA) where pp.IDPRUEBA in (300,85) and p.PETICIONCB = '%s'"""
    query1 = select1 % peticion

    select2 = """select pac.IDPACIENTE, pf.CLAVE, pac.APELLIDOS, pac.NOMBRE, pac.FECHA_NACIMIENTO, pac.SEXO,
    (p.FECHA - pac.FECHA_NACIMIENTO)/365 as EDAD, pac.NIF, pac.IDPACIENTEFAMILIA, p.FECHA from PETICION p
    join PACIENTE pac on (p.IDPACIENTE = pac.IDPACIENTE) full outer join PACIENTE_FAMILIA pf on 
    (pac.IDPACIENTEFAMILIA = pf.IDPACIENTEFAMILIA) where p.PETICIONCB = '%s'"""
    query2 = select2 % peticion

    PeticionesTabla = pd.read_sql(query1, GestCon)
    PacientesTabla = pd.read_sql(query2, GestCon)
    PacientesTabla = PacientesTabla.drop_duplicates(subset="IDPACIENTE")
    PacyPet = PeticionesTabla[['PETICIONCB', 'IDPACIENTE']]

    #     ###########################################################################################################
    #                  ## UPLOADS DATA TO THE TABLE PACIENTES ###
    #     ###########################################################################################################

    PacientesTabla = PacientesTabla.replace({'': 'NULL'})
    PacientesTabla = PacientesTabla.replace({' ': 'NULL'})
    PacientesTabla = PacientesTabla.replace({'-': 'NULL'})
    PacientesTabla = PacientesTabla.replace({None: "NULL"})
    PacientesTabla = PacientesTabla.replace({'*': "NULL"})
    PacientesTabla = PacientesTabla.replace({'"': ''})
    PacientesTabla = PacientesTabla.replace({"'": ''})

    if len(PacientesTabla) > 1:
        mensaje = peticion+' Varios pacientes con la misma PETICIONCB'
        mensajecolor = colored(mensaje, 'red')
        print(mensajecolor)
        ERRORES.loc[peticion, 'Fecha'] = pd.to_datetime('today')
        ERRORES.loc[peticion, 'ERRORES GRAVES'] = mensaje
        continue

    if PacientesTabla.empty:
        mensaje = peticion+' Problema al buscar el paciente en GestLab. No se incluye nada en la BBDD. Comproar si ' \
                           'PETICIONCB es correcto.'
        mensajecolor = colored(mensaje, 'red')
        print(mensajecolor)
        ERRORES.loc[peticion, 'Fecha'] = pd.to_datetime('today')
        ERRORES.loc[peticion,'ERRORES GRAVES'] = mensaje
        continue
    else:
        values = tuple(PacientesTabla.values.tolist()[0])

    IDPACIENTE = values[0]
    if IDPACIENTE != "NULL":
        IDPACIENTE = "'" + str(IDPACIENTE) + "'"
    IDEXTERNO = values[1]
    if IDEXTERNO != "NULL":
        IDEXTERNO = "'" + IDEXTERNO + "'"
    APELLIDOS = values[2]
    if APELLIDOS != "NULL":
        APELLIDOS = "'" + str(APELLIDOS) + "'"
    NOMBRE = values[3]
    if NOMBRE != "NULL":
        NOMBRE = "'" + str(NOMBRE) + "'"
    FECHA_NACIMIENTO = values[4]
    if FECHA_NACIMIENTO != "NULL":
        FECHA_NACIMIENTO = "'" + str(FECHA_NACIMIENTO) + "'"
    SEXO = values[5]
    if SEXO != "NULL":
        SEXO = "'" + SEXO + "'"
    EDAD = values[6]
    if EDAD != "NULL":
        EDAD = "'" + str(EDAD) + "'"
    NIF = values[7]
    if NIF != "NULL":
        NIF = "'" + NIF + "'"
    idFAMILIA = values[8]
    if idFAMILIA != "NULL":
        idFAMILIA = "'" + str(idFAMILIA) + "'"
    FECHA_RECEPCION = values[9]
    if FECHA_RECEPCION != "NULL":
        FECHA_RECEPCION = "'" + str(FECHA_RECEPCION) + "'"

    where = ("IDPACIENTE = %s, APELLIDOS = %s, NOMBRE = %s" % (IDPACIENTE, APELLIDOS, NOMBRE))
    fields = (
    'IDPACIENTE', 'IDEXTERNO', 'APELLIDOS', 'NOMBRE', 'NIF', 'SEXO', 'EDAD', 'FECHA_NACIMIENTO', 'FECHA_RECEPCION',
    'idFAMILIA')

    value_list = "(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)" % (IDPACIENTE, IDEXTERNO, APELLIDOS, NOMBRE, NIF,
                                                      SEXO, EDAD, FECHA_NACIMIENTO, FECHA_RECEPCION, idFAMILIA)
    insert = dbArrayLocal.insert_update('PACIENTES', fields, value_list, where)
    Count = Count + 1

    dbArrayLocal.commit()
    print(str(insert)+' insert pacientes')

    if insert == -1:
        mensaje = ('Error al subir elemento a PACIENTES')
        print(mensaje)
        ERRORES.loc[peticion, 'Fecha'] = pd.to_datetime('today')
        ERRORES.loc[peticion, 'ERRORES GRAVES'] = mensaje

    dbArrayLocal.commit()


#     ###########################################################################################################
#                  ## UPLOADS DATA TO THE TABLE PETICIONES ###
#     ###########################################################################################################

    PeticionesTabla = PeticionesTabla.replace({'': 'NULL'})
    PeticionesTabla = PeticionesTabla.replace({' ': 'NULL'})
    PeticionesTabla = PeticionesTabla.replace({'-': 'NULL'})
    PeticionesTabla = PeticionesTabla.replace({None: "NULL"})
    PeticionesTabla = PeticionesTabla.replace({'*': "NULL"})
    PeticionesTabla = PeticionesTabla.replace({'"': ''})
    PeticionesTabla = PeticionesTabla.replace({"'": ''})

    if PeticionesTabla.empty:
        mensaje = peticion + ' Problema al buscar la peticion en GestLab. No se incluye nada en la BBDD. Comproar si ' \
                             'PETICIONCB es correcto.'
        mensajecolor = colored(mensaje, 'red')
        print(mensajecolor)
        ERRORES.loc[peticion, 'Fecha'] = pd.to_datetime('today')
        ERRORES.loc[peticion, 'ERRORES GRAVES'] = mensaje
        continue
    else:
        values = tuple(PeticionesTabla.values.tolist()[0])

    PETICIONCB = values[0]
    if PETICIONCB != "NULL":
        PETICIONCB = "'" + PETICIONCB + "'"
    IDPRUEBA = values[1]
    if IDPRUEBA != "NULL":
        IDPRUEBA = "'" + str(IDPRUEBA) + "'"
    IDPETICION = values[2]
    if IDPETICION != "NULL":
        IDPETICION = "'" + str(IDPETICION) + "'"
    IDLABEXTERNO = values[3]
    if IDLABEXTERNO != "NULL":
        IDLABEXTERNO = "'" + str(IDLABEXTERNO) + "'"
    IDPACIENTE = values[4]
    if IDPACIENTE != "NULL":
        IDPACIENTE = "'" + str(IDPACIENTE) + "'"
    FECHA_TOMA = values[5]
    if FECHA_TOMA != "NULL":
        FECHA_TOMA = "'" + str(FECHA_TOMA) + "'"
    FECHA_ALTA = values[6]
    if FECHA_ALTA != "NULL":
        FECHA_ALTA = "'" + str(FECHA_ALTA) + "'"
    COMENTARIO = values[7]
    if COMENTARIO != "NULL":
        COMENTARIO = COMENTARIO.replace("'", "")
        COMENTARIO = COMENTARIO.replace("\r", "")
        COMENTARIO = COMENTARIO.replace("\n", "")
        COMENTARIO = "'" + str(COMENTARIO) + "'"
    PROCEDENCIAid = values[8]
    if PROCEDENCIAid != "NULL":
        PROCEDENCIAid = "'" + str(PROCEDENCIAid) + "'"
    TRESULTADO = values[9]
    if TRESULTADO != "NULL":
        TRESULTADO = TRESULTADO.replace("'", "")
        TRESULTADO = TRESULTADO.replace("\r", "")
        TRESULTADO = TRESULTADO.replace("\n", "")
        TRESULTADO = "'" + TRESULTADO + "'"

    where = ("PETICIONCB = %s, IDPACIENTE = %s, FECHA_TOMA =%s, FECHA_ALTA = %s" % (
        PETICIONCB, IDPACIENTE, FECHA_TOMA, FECHA_ALTA))
    fields = (
    'PETICIONCB', 'IDPRUEBA', 'IDPETICION', 'IDLABEXTERNO', 'IDPACIENTE', 'FECHA_TOMA', 'FECHA_ALTA', 'COMENTARIO',
    'PROCEDENCIAid', 'TRESULTADO')

    value_list = "(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)" % (
        PETICIONCB, IDPRUEBA, IDPETICION, IDLABEXTERNO, IDPACIENTE, FECHA_TOMA, FECHA_ALTA, COMENTARIO, PROCEDENCIAid,
        TRESULTADO)

    insert = dbArrayLocal.insert_update('PETICIONES', fields, value_list, where)
    Count = Count + 1

    dbArrayLocal.commit()
    print(str(insert)+' insert en peticiones')
    if insert == -1:
        mensaje = ('Error al subir elemento a PETICIONES')
        print(mensaje)
        ERRORES.loc[peticion, 'Fecha'] = pd.to_datetime('today')
        ERRORES.loc[peticion, 'ERRORES GRAVES'] = mensaje

    dbArrayLocal.commit()

    #     ###########################################################################################################
    #                  ## UPLOADS DATA TO THE TABLE PACIENTE_PETICION ###
    #     ###########################################################################################################

    PacyPet = PacyPet.replace({'': 'NULL'})
    PacyPet = PacyPet.replace({' ': 'NULL'})
    PacyPet = PacyPet.replace({'-': 'NULL'})
    PacyPet = PacyPet.replace({None: "NULL"})
    PacyPet = PacyPet.replace({'*': "NULL"})
    PacyPet = PacyPet.replace({'"': ''})
    PacyPet = PacyPet.replace({"'": ''})

    if PacyPet.empty:
        mensaje = peticion + ' Problema al crear la tabla PacyPet en el script de python. No se incluye nada en la BBDD. Comproar si ' \
                             'PETICIONCB es correcto.'
        mensajecolor = colored(mensaje, 'red')
        print(mensajecolor)
        ERRORES.loc[peticion, 'Fecha'] = pd.to_datetime('today')
        ERRORES.loc[peticion, 'ERRORES GRAVES'] = mensaje
        continue
    else:
        values = tuple(PacyPet.values.tolist()[0])

    PETICIONCB = values[0]
    if PETICIONCB != "NULL":
        PETICIONCB = "'" + PETICIONCB + "'"
    IDPACIENTE = values[1]
    if IDPACIENTE != "NULL":
        IDPACIENTE = "'" + str(IDPACIENTE) + "'"

    where = ("PETICIONCB = %s, IDPACIENTE = %s" % (PETICIONCB, IDPACIENTE))
    fields = ('PETICIONCB', 'IDPACIENTE')

    value_list = "(%s,%s)" % (PETICIONCB, IDPACIENTE)

    insert = dbArrayLocal.insert_update('PACIENTE_PETICION', fields, value_list, where)
    Count = Count + 1

    dbArrayLocal.commit()
    print(str(insert)+' insert en pacypet')
    if insert == -1:
        mensaje = ('Error al subir elemento a PACIENTE_PETICION')
        print(mensaje)
        ERRORES.loc[peticion, 'Fecha'] = pd.to_datetime('today')
        ERRORES.loc[peticion, 'ERRORES GRAVES'] = mensaje

    dbArrayLocal.commit()

    select = """SELECT pp.IDPRUEBA_LISTA from PETICION p join PET_PRUEBA pp on (p.IDPETICION = pp.IDPETICION) where
     pp.IDPRUEBA in (300,85) and p.PETICIONCB = '%s';"""
    query = select % peticion
    query_df = pd.read_sql(query, GestCon)

    # selects variants to insert in variants_infor_pet and erases them from variants_infor for a faster search

    variants = variant_infor['PETICIONCB'] == peticion
    variant_infor_pet = variant_infor[variants]
    variant_infor_pet['TestType'] = query_df.iloc[0, 0]
    variant_infor[variants].dropna()

    for row in variant_infor_pet.values.tolist():
        values = row
        values = tuple(values)

        if ((values[12] == 'Analizado') and (values[14] == 'NULL')) and (path != insertPath):
            mensaje = peticion+' Caso con alteración analizada sin categorizar'
            mensajecolor = colored(mensaje, 'red')
            print(mensajecolor)
            ERRORES.loc[peticion, 'Fecha'] = pd.to_datetime('today')
            ERRORES.loc[peticion, 'ERRORES GRAVES'] = mensaje
            continue

        PETICIONCB = values[1]
        if PETICIONCB != "NULL":
            PETICIONCB = "'" + PETICIONCB + "'"
        AberrationNo = values[2]
        if AberrationNo != "NULL":
            AberrationNo = "'" + str(AberrationNo) + "'"
        Chr = values[3]
        if Chr != "NULL":
            Chr = "'" + Chr + "'"
        Cytoband = values[4]
        if Cytoband != "NULL":
            Cytoband = "'" + Cytoband + "'"
        PositionStart = values[5]
        if PositionStart != "NULL":
            PositionStart = "'" + str(PositionStart) + "'"
        PositionStop = values[6]
        if PositionStop != "NULL":
            PositionStop = "'" + str(PositionStop) + "'"
        ProbesNum = values[7]
        if ProbesNum != "NULL":
            ProbesNum = "'" + str(ProbesNum) + "'"
        Amplification = values[8]
        if Amplification != "NULL":
            Amplification = "'" + str(Amplification) + "'"
        Deletion = values[9]
        if Deletion != "NULL":
            Deletion = "'" + str(Deletion) + "'"
        pval = values[10]
        if pval != "NULL":
            pval = "'" + str(pval) + "'"
        GeneNames = values[11]
        if GeneNames != "NULL":
            GeneNames = "'" + GeneNames + "'"
        Analizado = values[12]
        if Analizado != "NULL":
            Analizado = "'" + Analizado + "'"
        Tamano = values[13]
        if Tamano != "NULL":
            Tamano = "'" + str(Tamano) + "'"
        if values[14] == 'Basurilla-No Real':
            values = list(values)
            values[14] = 'Basura-No Real'
            values = tuple(values)
        Categorization = values[14]
        if Categorization != "NULL":
            Categorization = "'" + Categorization + "'"
        CopyNumber = values[15]
        if CopyNumber != "NULL":
            CopyNumber = "'" + str(CopyNumber) + "'"
        Coments = values[16]
        if Coments != "NULL":
            Coments = Coments.replace("'", "")
            Coments = Coments.replace("\r", "")
            Coments = Coments.replace("\n", "")
            Coments = "'" + str(Coments) + "'"
        Coordenadas = values[17]
        if Coordenadas != "NULL":
            Coordenadas = "'" + Coordenadas + "'"
        FECHA_REGISTRO = values[19]
        if FECHA_REGISTRO != "NULL":
            FECHA_REGISTRO = "'" + str(FECHA_REGISTRO) + "'"
        FECHA_RESULTADO = values[20]
        if FECHA_RESULTADO != "NULL":
            FECHA_RESULTADO = "'" + str(FECHA_RESULTADO) + "'"
        TestType = values[21]
        if TestType != "NULL":
            TestType = "'" + str(TestType) + "'"

        # Where y whereUpdate son dos vairiables que deternminan que se va a modificar en el update

        where = ("PETICIONCB = %s, AberrationNo = %s, FECHA_REGISTRO =%s, FECHA_RESULTADO = %s" % (
            PETICIONCB, AberrationNo, FECHA_REGISTRO, FECHA_RESULTADO))
        whereUpdate = ("Categorization = %s, CopyNumber = %s, Coments =%s, Analizado = %s" % (Categorization, CopyNumber, Coments, Analizado))

        fields = (
        'PETICIONCB', 'AberrationNo', 'Chr', 'Cytoband', 'PositionStart', 'PositionStop', 'ProbesNum', 'Amplification',
        'Deletion', 'pval', 'GeneNames', 'Analizado', 'Tamano', 'Categorization', 'CopyNumber', 'Coments', 'Coordenadas',
        'FECHA_REGISTRO', 'FECHA_RESULTADO', 'TestType')

        # Valores que se van a subir a la base de datos

        value_list = "(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s, %s)" % (
            PETICIONCB, AberrationNo, Chr, Cytoband, PositionStart, PositionStop, ProbesNum, Amplification,
            Deletion, pval, GeneNames, Analizado, Tamano, Categorization, CopyNumber, Coments, Coordenadas,
            FECHA_REGISTRO, FECHA_RESULTADO, TestType)

        # Ejecuta insert_update de la libreria dbconnect para realizar la actualización de la tabla de la base de datos

        insert = dbArrayLocal.insert_update('VARIANTES_INFORMADAS', fields, value_list, whereUpdate)
        Count = Count + 1

        dbArrayLocal.commit()
        print(str(insert)+' Insert en VI')
        if insert == -1:
            mensaje = ('Error al subir elemento a VARIANTES_INFORMADAS')
            print(mensaje)
            ERRORES.loc[peticion, 'Fecha'] = pd.to_datetime('today')
            ERRORES.loc[peticion, 'ERRORES GRAVES'] = mensaje
        dbArrayLocal.commit()

    # imprime en un excel los elementos que han sido determinados como erróneos

ERRORES.to_csv(nimarray_excels_logs, mode='a')
