# Este script carga cada excel de la carpeta de Arrays de 2019 y genera un documento final, en donde se recopilan
# todos los casos de la carpeta con el formato definido.

#!/usr/bin/env python3
# coding=utf-8

from __future__ import unicode_literals
import pandas as pd
import numpy as np
import io
import math
from xlrd import XLRDError
from xlwt import Workbook
from os import listdir
from os.path import isfile, join
import random

counter1 = 0
countNo2 = 0
countNo3 = 0
ListaEmpty = []
Lista66 = []
Lista1512 = []
Lista1502 = []
OldCols1 = ['PETICIONCB', u'AberrationNo', u'Chr', u'Cytoband', u'Start', u'Stop', u'#Probes', u'Amplification',
            u'Deletion', u'pval', u'Gene Names', u'Analizado', u'Tama\xf1o', u'Categorization', u'Copy Number',
            u'Coments', u'Coordenadas']
OldCols2 = ['PETICIONCB', u'AberrationNo', u'Chr', u'Cytoband', u'Start', u'Stop', u'#Probes', u'Amplification',
            u'Deletion', u'pval', u'Gene Names', u'Analizado', u'Tama\xf1o', u'Categorization', u'Copy Number']

Compara = [ u'AberrationNo', u'Tama\xf1o', u'Categorization', u'Copy Number',u'Coments', u'Coordenadas', u'Analizado']

mypath = '/Users/rnavarro/Desktop/Arrays2019Distribuidos/Todos Juntos'

files = [f for f in listdir(mypath) if isfile(join(mypath, f))]  # Genera la lista de archivos de la carpeta a recorrer
files.sort()

if '.DS_Store' in files:                                        # Elimina este archivo de la lista que aparece en mac
    files.remove('.DS_Store')

HojaFinal = pd.DataFrame()

for file in files:

    counter1 = counter1 + 1
    print(file + ' file nª ' + str(counter1))

    # Ajusta el Raw data como lo queremos

    try:
        ExFile = pd.ExcelFile(mypath + '/' + file)
    except XLRDError:
        print(file + ' Corrupto o mal guardado, guardando de nuevo')
        file1 = io.open(mypath + '/' + file, "r", encoding="iso-8859-1")
        data = file1.readlines()
        xldoc = Workbook()
        sheet = xldoc.add_sheet("Sheet1", cell_overwrite_ok=True)
        for i, row in enumerate(data):
            for j, val in enumerate(row.replace('\n', '').split('\t')):
                sheet.write(i, j, val)
        xldoc.save(mypath + '/' + file)
        ExFile = pd.ExcelFile(mypath + '/' + file)

    # Esta sección de código le da formato definido a la priemra hoja del excel, el raw data
    # Utiliza la estructura de try/except para adaptarse al formato de cada excel.

    Hoja1 = pd.read_excel(ExFile, 0, encoding='utf-8')
    row1 = 0
    try:
        row2 = Hoja1.loc[Hoja1['Genome: hg19'] == 'AberrationNo'].index[0]
    except IndexError:
        row2 = Hoja1.loc[Hoja1['Genome: hg19'] == 'Index'].index[0]
    except KeyError:
        try:
            row2 = Hoja1.loc[Hoja1['Aberration Algorithm: ADM-2'] == 'AberrationNo'].index[0]
        except KeyError:
            row2 = Hoja1.loc[Hoja1['Aberration Algorithm: ADM-1'] == 'AberrationNo'].index[0]

    Hoja1 = Hoja1.drop(range(row1, row2), axis=0)    # Encuentro el valor genome hg19 o Aberration...
                                                     # y recorta las filas de la hoja
    try:
        row2 = Hoja1.loc[Hoja1['Genome: hg19'] == 'Number of calls per sample:'].index[0] + 2
        row1 = row2 - 4
    except IndexError:
        vector = Hoja1['Genome: hg19'] == Hoja1['Genome: hg19']
        vector = vector.to_list()
        vector = [i for i, x in enumerate(vector) if x]
        count = len(vector)
        Hoja1 = Hoja1.reset_index()
        Hoja1 = Hoja1.drop('index', axis=1)
        row2 = count
        row1 = count
    except KeyError:
        try:
            row2 = Hoja1.loc[Hoja1['Aberration Algorithm: ADM-2'] == 'Number of calls per sample:'].index[0] + 2
            row1 = row2 - 4
        except KeyError:
            row2 = Hoja1.loc[Hoja1['Aberration Algorithm: ADM-1'] == 'Number of calls per sample:'].index[0] + 2
            row1 = row2 - 4
                                                            # De nuevo encuentra ciertos elementos en el excel
                                                            # Elimina las filas según esos puntos
    Hoja1 = Hoja1.drop(range(row1, row2), axis=0)
    Hoja1.insert(0, 'PETICIONCB', Hoja1.iloc[1][0])

    try:
        Hoja1 = Hoja1.drop(Hoja1.loc[Hoja1['Genome: hg19'] == 'AberrationNo'].index[0] + 1, axis=0)
    except IndexError:
        Hoja1 = Hoja1.drop(Hoja1.loc[Hoja1['Genome: hg19'] == 'Index'].index[0] + 1, axis=0)
    except KeyError:
        try:
            Hoja1 = Hoja1.drop(Hoja1.loc[Hoja1['Aberration Algorithm: ADM-2'] == 'AberrationNo'].index[0] + 1, axis=0)
        except KeyError:
            Hoja1 = Hoja1.drop(Hoja1.loc[Hoja1['Aberration Algorithm: ADM-1'] == 'AberrationNo'].index[0] + 1, axis=0)

                                                            # Ecuentra nuevos puntos y recorta la hoja
    Hoja1 = Hoja1.reset_index()
    Hoja1 = Hoja1.drop('index', axis=1)
    vector = Hoja1.loc[0].values
    vector[0] = 'PETICIONCB'
    Hoja1.columns = vector
    Hoja1 = Hoja1.drop(0, axis=0)
    Hoja1 = Hoja1.reset_index()
    Hoja1 = Hoja1.drop('index', axis=1)

    # Esta sección le da el formato definido a la segunda hoja del excel, los resultados analizados.
    # Para la segunda hoja, el proceso es parecido. Se ecuentrar ciertos elementos en el excel y según estos puntos
    # las filas que sobran se eliminan.

    Hoja2 = pd.DataFrame()
    try:
        if len(ExFile.sheet_names) == 1:
            countNo2 = countNo2 + 1
            print('el archivo ' + file + ' No tiene segunda hoja: ' + str(countNo2))
            Lista66.append(file)
            continue
        else:
            Hoja2 = pd.read_excel(ExFile, 1, encoding='utf-8')
            row1 = 0
            row2 = Hoja2.loc[Hoja2['Genome: hg19'] == 'AberrationNo'].index[0]
            Hoja2 = Hoja2.drop(range(row1, row2), axis=0)
            Hoja2 = Hoja2.drop(row2 + 1, axis=0)
            vector = Hoja2['Genome: hg19'] == Hoja2['Genome: hg19']
            vector = vector.to_list()
            vector = [i for i, x in enumerate(vector) if x]
            count = len(vector)
            Hoja2 = Hoja2.reset_index()
            Hoja2 = Hoja2.drop('index', axis=1)
            row1 = count
            row2 = Hoja2.shape[0]
            Hoja2 = Hoja2.drop(range(row1, row2), axis=0)
            Hoja2 = Hoja2.reset_index()
            Hoja2 = Hoja2.drop('index', axis=1)
            vector = Hoja2.loc[0].values
            Hoja2.columns = vector
            Hoja2 = Hoja2.drop(0, axis=0)
            Hoja2 = Hoja2.reset_index()
            Hoja2 = Hoja2.drop('index', axis=1)
            try:
                if math.isnan(Hoja2.values[Hoja2.shape[0] - 1][0]) and not (math.isnan(Hoja2.values[0][0])):
                    Hoja2 = Hoja2.drop(range(Hoja2.shape[0] - 2, Hoja2.shape[0]), axis=0)
                if (Hoja2.values[0][Hoja2.shape[1] - 1] == u':-') and (
                        Hoja2.values[1][Hoja2.shape[1] - 1] == u':-'):
                    Hoja2 = Hoja2.iloc[0:0]
                    print('Hoja 2 Vaciada')
                if (math.isnan(Hoja2.values[0][Hoja2.shape[1] - 1])) and (math.isnan(Hoja2.values[Hoja2.shape[0]-1][0])):
                    Hoja2 = Hoja2.iloc[0:0]
                    print('Hoja 2 Vaciada')
            except TypeError:
                if isinstance((Hoja2.values[Hoja2.shape[0] - 1][0]), str) and not(math.isnan(float(Hoja2.values[0][0]))):
                    Hoja2 = Hoja2.drop(range(Hoja2.shape[0] - 2, Hoja2.shape[0]), axis=0)
            except IndexError:
                countNo3 = countNo3 + 1
                print('Index Error en Hoja 2. Hoja 2 vacía, nº vacíos: ' + str(countNo3))

    except KeyError:
        try:
            Hoja2 = pd.read_excel(ExFile, 1, encoding='utf-8')
            row1 = 0
            row2 = Hoja2.loc[Hoja2['Aberration Algorithm: ADM-2'] == 'AberrationNo'].index[0]
            Hoja2 = Hoja2.drop(range(row1, row2), axis=0)
            Hoja2 = Hoja2.drop(row2 + 1, axis=0)
            vector = Hoja2['Aberration Algorithm: ADM-2'] == Hoja2['Aberration Algorithm: ADM-2']
            vector = vector.to_list()
            vector = [i for i, x in enumerate(vector) if x]
            count = len(vector)
            Hoja2 = Hoja2.reset_index()
            Hoja2 = Hoja2.drop('index', axis=1)
            row1 = count
            row2 = Hoja2.shape[0]
            Hoja2 = Hoja2.drop(range(row1 - 2, row2), axis=0)
            Hoja2 = Hoja2.reset_index()
            Hoja2 = Hoja2.drop('index', axis=1)
            vector = Hoja2.loc[0].values
            Hoja2.columns = vector
            Hoja2 = Hoja2.drop(0, axis=0)
            Hoja2 = Hoja2.reset_index()
            Hoja2 = Hoja2.drop('index', axis=1)
        except KeyError:
            Hoja2 = pd.read_excel(ExFile, 1, encoding='utf-8')
            row1 = 0
            row2 = Hoja2.loc[Hoja2['Aberration Algorithm: ADM-1'] == 'AberrationNo'].index[0]
            Hoja2 = Hoja2.drop(range(row1, row2), axis=0)
            Hoja2 = Hoja2.drop(row2 + 1, axis=0)
            vector = Hoja2['Aberration Algorithm: ADM-1'] == Hoja2['Aberration Algorithm: ADM-1']
            vector = vector.to_list()
            vector = [i for i, x in enumerate(vector) if x]
            count = len(vector)
            Hoja2 = Hoja2.reset_index()
            Hoja2 = Hoja2.drop('index', axis=1)
            row1 = count
            row2 = Hoja2.shape[0]
            Hoja2 = Hoja2.drop(range(row1 - 2, row2), axis=0)
            Hoja2 = Hoja2.reset_index()
            Hoja2 = Hoja2.drop('index', axis=1)
            vector = Hoja2.loc[0].values
            Hoja2.columns = vector
            Hoja2 = Hoja2.drop(0, axis=0)
            Hoja2 = Hoja2.reset_index()
            Hoja2 = Hoja2.drop('index', axis=1)

    # En este punto, se unen las dos hojas con el formato final
    # Elimino las columnas de la hoja2 y me facilito el merge

    H1cols = Hoja1.columns.to_list()
    del H1cols[0:2]
    H1cols = list(set(Hoja2.columns.to_list()).intersection(H1cols))
    Hoja2 = Hoja2.drop(H1cols, axis=1)                            # Elimino las columnas de la hoja 2 (Facilita el merge)
    Hoja2.insert(Hoja2.shape[1], 'Analizado', 'Analizado')        # Añado una columna que determina si la alteración ha
                                                                  # sido analizada, es decir que esté en la 2º hoja

    # Une las dos hojas

    try:
        if isinstance(Hoja1['Start'][0], str):
            Hoja2 = Hoja2.drop(['Start', 'Stop'], axis=1)
            print('Start en Letras')
        elif (Hoja2.shape[1] == 7)and(not(Hoja2.columns.to_list() == Compara)):  # según el número de columnas y según su nombre
            Hoja2.columns.values[1] = 'Tamaño'                                   # se hacen ciertas modificaciones para que se pueda
            Hoja2.columns.values[2] = 'Categorization'                           # realizar la unión
            Hoja2.columns.values[3] = 'Copy Number'
            Hoja2.columns.values[4] = 'nan'
            Hoja2.columns.values[5] = 'comments'
            Hoja2 = Hoja2.drop('nan', axis=1)
        elif Hoja2.shape[1] == 6:
            Hoja2.columns.values[1] = 'Tamaño'
            Hoja2.columns.values[2] = 'Categorization'
            Hoja2.columns.values[4] = 'comments'
        elif Hoja2.shape[1] == 5:
            Hoja2.columns.values[1] = 'Tamaño'
            Hoja2.columns.values[2] = 'Categorization'
            Hoja2.columns.values[3] = 'Copy Number'
        elif Hoja2.shape[1] == 4:
            Hoja2.columns.values[1] = 'Tamaño'
            Hoja2.columns.values[2] = 'Categorization'
            Hoja2.columns.values[3] = 'Copy Number'
        if file == 'Report 03992.xls':
            Hoja2P = Hoja2
            Cols = Hoja2.columns.to_list()
            Cols = Cols[:11]
            del Cols[0]
            Hoja2P = Hoja2P.drop(Cols, axis=1)
            result = pd.merge(Hoja1, Hoja2P, how='outer')
        result = pd.merge(Hoja1, Hoja2, how='outer')
        result = result.sort_values(by=['AberrationNo'])
        print('unido en tryMerge')
    except KeyError:
        result = pd.merge(Hoja1, Hoja2, how='outer')
        result = result.sort_values(by=['AberrationNo'])
        print('Unido Cols1')
    except UnicodeDecodeError:
        if Hoja2.shape[1] == 7:
            Hoja2.columns.values[1] = 'Tamaño'
            Hoja2.columns.values[2] = 'Categorization'
            Hoja2.columns.values[3] = 'Copy Number'
            Hoja2.columns.values[4] = 'nan'
            Hoja2.columns.values[5] = 'comments'
            Hoja2.drop('nan', axis=1)
        elif Hoja2.shape[1] == 6:
            Hoja2.columns.values[1] = 'Tamaño'
            Hoja2.columns.values[2] = 'Categorization'
            Hoja2.columns.values[4] = 'comments'
            if file == 'Report 03966.xls':
                Hoja2.columns.values[1] = 'Comments'
                Hoja2.columns.values[2] = 'Tamaño'
                Hoja2.columns.values[3] = 'Categorization'
                Hoja2.columns.values[4] = 'Copy Number'
        result = pd.merge(Hoja1, Hoja2, how='outer')
        result = result.sort_values(by=['AberrationNo'])
        print('Unido Cols2')
    except IndexError:
        result = pd.merge(Hoja1, Hoja2, how='outer')
        result = result.sort_values(by=['AberrationNo'])
        print('Las dos hojas vacías')
        ListaEmpty.append(file)

    # Una vez las dos hojas está unidas (result), se concatenan con la hojas previas en HojaFinal
    # Se añaden o eliminan columnas de result para que se pueda concatenar con facilidad.

    if result.shape[1] == 19:
        Orden = result.columns.to_list()
        Orden.append(Orden.pop(Orden.index('Gain')))
        Orden.append(Orden.pop(Orden.index('Loss')))
        OldCols1.append('Gain')
        OldCols1.append('Loss')
        result = result[OldCols1]
        HojaFinal = pd.concat([HojaFinal, result], sort=False)
        Lista1512.append(file)
        OldCols1.remove('Gain')
        OldCols1.remove('Loss')
        print('En el if 19')
    elif result.shape[1] == 17:
        result = result[OldCols1]
        HojaFinal = pd.concat([HojaFinal, result], sort=False)
        Lista1512.append(file)
        print('En el if 17')
    elif result.shape[1] < 17:
        Anadir = list(set(OldCols1)-set(result.columns.to_list()))
        for x in Anadir:
            result.insert(result.shape[1], x, '')
        result = result[OldCols1]
        HojaFinal = pd.concat([HojaFinal, result], sort=False)
        Lista1512.append(file)
    else:
        HojaFinal = pd.concat([HojaFinal, result], sort=False)
        Lista1512.append(file)
        print('En el else 17')

print('Sin segunda Hoja: ' + str(countNo2))
print('Con segunda Hoja pero vacía: ' + str(countNo3))

HojaFinal.to_excel('/Users/rnavarro/Desktop/Resultados/ConcatFiles/Todos Juntos/UnidosFormatoBueno.xls',
                   sheet_name='TodosJuntos')   # aqui sale el excel final

# --------------------------------------AQUÍ TERMINA EL PROCESO --------------------------------------------------
# EL resto del código comprueba el número de archivos unidos para determinar si todos los de la carpeta son incluidos

Lista1502 = HojaFinal[HojaFinal.columns[0]]
Lista1502 = Lista1502.to_list()
Lista1502 = list(dict.fromkeys(Lista1502))
Lista1502.sort()

Lista1512 = list(dict.fromkeys(Lista1512))
Lista1512.sort()

Lista1512Trans = []
Lista1512Trans2 = []

# QUITO LAS EXTENSIOENS DE LA LISTA

for x in Lista1512:
    if x[-11:] == ' report.xls':
        x = x[:-11]
        Lista1512Trans.append(x)
    elif x[-9:] == '.xls.xlsx':
        x = x[:-9]
        Lista1512Trans.append(x)
    elif x[-9:] == '.txt.xlsx':
        x = x[:-9]
        Lista1512Trans.append(x)
    elif x[-8:] == '.txt.xls':
        x = x[:-8]
        Lista1512Trans.append(x)
    elif x[-5:] == '.xlsx':
        x = x[:-5]
        Lista1512Trans.append(x)
    elif x[-4:] == '.xls':
        x = x[:-4]
        Lista1512Trans.append(x)
    else:
        Lista1512Trans.append(x)

for x in Lista1512Trans:
    if x[:9] == 'Report 19':
        x = x[7:]
        Lista1512Trans2.append(x)
    elif x[:7] == 'Report ':
        x = '19NR' + x[7:]
        Lista1512Trans2.append(x)
    else:
        Lista1512Trans2.append(x)

Lista1512Trans2.sort()

IntersectionList = list(set(Lista1512Trans2) - set(Lista1502))
IntersectionList.sort()

Malos = []

Malos = [u'19NR03953', u'19NR06646', u'19NR03931', u'19NR04636', u'19NR05948', u'19NR06761', u'19NR06389', u'19NR06390',
         u'19NR06116', u'19NR04637', u'19NR03735', u'19NR03810', u'19NR02139', u'19NR06627']

Lista1578 = Lista1512 + Lista66
Lista1578.sort()

mypath1 = '/Users/rnavarro/Desktop/Arrays2019Distribuidos/Todos Juntos'
Lista1584 = [f for f in listdir(mypath1) if isfile(join(mypath1, f))]
Lista1584.sort()

print('longitud lista 1502 ' + str(len(Lista1502)))
print('longitud lista 1512 ' + str(len(Lista1512)))
print('longitud lista 1578 ' + str(len(Lista1578)))
print('longitud lista 1584 ' + str(len(Lista1584)))

IList = list(set(Lista1584) - set(Lista1578))
print('longitud lista IList ' + str(len(IList)))
print(IList)
print('longitud lista IList ' + str(len(ListaEmpty)))
print(ListaEmpty)

sample = random.sample(Lista1584, k=25
)
