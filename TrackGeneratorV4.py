# Este script conecta con la base de datos NIMArray y saca un .bed final que se puede cargar en Cytogenomics como Track

from __future__ import unicode_literals
import pandas as pd
import numpy as np
from dbconnect.dbConnect import dbConnect
from datetime import date
import os
import configparser
from email_nimarray import email_nimarray
from nimmail.nimmail import nimmail

def enResol(filamia, filalista):
    'Esta función comprueba si las dos alternaciones pasadas como variables están dentro de la resolución establecida.'
    filaCheck = filamia != filalista
    CheckChr = filalista[2] == filamia[2]
    CheckBand = filalista[3] == filamia[3]
    CheckStart = abs(filalista[4] - filamia[4]) < res
    CheckStop = abs(filalista[5] - filamia[5]) < res
    enRes = (filaCheck and CheckChr and CheckBand and CheckStart and CheckStop)

    return enRes

def decidecolor(color, number, ListaPercent):
    'Esta función decide el color final que va a llevar la alteración en el track'

    ListaNoDup = list(set(ListaPercent))
    ListaNoDup.sort()
    top = max(ListaNoDup)
    bottom = min(ListaNoDup)
    p66 = np.percentile(ListaNoDup, 66)
    p33 = np.percentile(ListaNoDup, 33)

    if p66 < number <= top:
        color = color+'Oscuro'

    if p33 < number <= p66:
        color = color+'Intermedio'

    if bottom <= number <= p33:
        color = color+'Claro'

    color = DicColor[color]

    return color

columnasTrackV4 = [u'title', u'start', u'', u'', u'', u'', u'', u'', u'', u'', u'', u'cases']
columnasTrackV4Limpias = [u'title', u'', u'', u'', u'', u'', u'', u'', u'', u'', u'', u'']
columnasreps = ['PETICIONCB', 'AberrationNo', 'Chr', 'Cytoband', 'PositionStart', 'PositionStop', 'ProbesNum',
                'Amplification', 'Deletion', 'pval', 'GeneNames', 'Analizado', 'Tamano', 'Categorization', 'CopyNumber',
                'Coments', 'Coordenadas', 'FECHA_REGISTRO','FECHA_RESULTADO', 'nº reps']

DicCategorias = {
    'Patogénica': u'Rojo',
    'Probablemente Patogénica': u'Naranja',
    'VUS': u'Amarillo',
    'Probablemente Benigna': u'Verde',
    'Benigna': u'Azul',
    'Basurilla-No Real': u'Morado',
    'Hallazgo incidental': u'Granate',
    'Heredado': u'AzulNav',
    'None': u'Gris'
}
DicCopyNumber = {
    'Deleción Hemicigota': u'Rojo',
    'Deleción Heterocigota': u'Rojo',
    'Deleción Homocigota': u'Rojo',
    'Deleción no determinada': u'Rojo',
    'Duplicación': u'Azul',
    'Triplicación o Superior': u'Azul',
    'Deleción no clasificada': u'Rojo'
}
DicColor = {
    'RojoClaro':u'16751001',
    'RojoIntermedio':u'16724787',
    'RojoOscuro':u'10027008',
    'NaranjaClaro':u'16764057',
    'NaranjaIntermedio':u'16750899',
    'NaranjaOscuro':u'13395456',
    'AmarilloClaro':u'16777113',
    'AmarilloIntermedio':u'16777011',
    'AmarilloOscuro':u'13421568',
    'VerdeClaro':u'10092441',
    'VerdeIntermedio':u'39168',
    'VerdeOscuro':u'26112',
    'AzulClaro':u'10079487',
    'AzulIntermedio':u'3381759',
    'AzulOscuro':u'19609',
    'MoradoClaro':u'13408767',
    'MoradoIntermedio':u'10040319',
    'MoradoOscuro':u'4980889',
    'GranateClaro':u'16751052',
    'GranateIntermedio':u'16711807',
    'GranateOscuro':u'6684723',
    'AzulNavClaro':u'10066431',
    'AzulNavIntermedio':u'3355647',
    'AzulNavOscuro':u'153',
    'GrisClaro':u'12632256',
    'GrisIntermedio':u'8421504',
    'GrisOscuro':u'4210752'
}

ListaFinal = []
NuevaLista = []
Track = [[]]
ListaTrack = []
ListaPercent = []
Reps = []
Track3 = []
Track3_pato = []
counter = 0
cuantas = 0
count = 0

# Resolucion,NombreTrack,CódigoGestlab

Pruebas = ([15000,'Autismo_Patogénicas','(104)'],[100000,'60K_Patogénicas','(98)'],[40000,'180K_Patogénicas','(353)'],
           [250000,'Prenatal_Patogénicas','(21)'],[25000,'400K_Patogénicas','(42)'],[15000,'Benignas','(103,114,122,141,'
            '142,147,185,104,98,353,21,42,2497,2540,2721,2817,3030,318,319,353,48,69,98)'])


db = dbConnect('bioinfodb.nimgenetics.com', 'nimarray', 'nimarray', 'NIMArray')

config_parser = configparser.ConfigParser(allow_no_value=True)
bindir = os.path.abspath(os.path.dirname(__file__))
config_parser.read(bindir + "/cfg.cnf")

#Conexion a bases de datos
host = config_parser.get("DB", "hostname")
db = config_parser.get("DB", "db")
user = config_parser.get("DB", "user")
password = config_parser.get("DB", "password")

#Paths
track_Path = config_parser.get("PATHS", "track_Path")
email_Path = config_parser.get("PATHS", "path_email")

db = dbConnect(host, user, password, db)

sql_command = """SELECT DISTINCT PETICIONCB FROM NIMArray.VARIANTES_INFORMADAS;"""
total = pd.read_sql_query(sql_command, db.db)
total = len(total.PETICIONCB.tolist())
estadisticas = pd.DataFrame()

for n in Pruebas:
    #res es el dato de resolución del array correspondiente
    res = n[0]
    #tipo es el tipo de track que vamos a generar
    tipo = n[1]
    #codigo es el código/codigos de gestlab del array o arrays
    codigo = n[2]

    counter = 0
    Track = []
    ListaFinal = []
    Track3_pato = []
    df_pato = pd.DataFrame()

    #Query para obtener el número total de peticiones de este tipo

    sql_command_number = """SELECT COUNT(DISTINCT PETICIONCB) FROM NIMArray.VARIANTES_INFORMADAS WHERE TestType IN %s"""

    query = sql_command_number % codigo
    petition_number = pd.read_sql_query(query, db.db)


    if tipo == 'Benignas':
        sql_command = """SELECT * FROM VARIANTES_INFORMADAS where Analizado = 'Analizado' AND TestType IN %s AND 
            Categorization = 'Benigna' """
    else:
        sql_command = """SELECT * FROM VARIANTES_INFORMADAS where Analizado = 'Analizado' AND TestType IN %s AND 
            (Categorization = 'Patogénica' OR Categorization = 'Probablemente Patogénica' )"""

    query = sql_command % codigo
    variant_infor = pd.read_sql_query(query, db.db)
    variant_infor_pato = variant_infor.drop(['TestType'], axis=1)


    Listote_pato = variant_infor_pato.values.tolist()
    estadisticas.loc[tipo, 'Casos'] = int(petition_number['COUNT(DISTINCT PETICIONCB)'].values[0])
    estadisticas.loc[tipo,'Variantes'] = int(len(Listote_pato))

    while len(Listote_pato) > 0:
        counter = counter + 1
        filamia = Listote_pato[0]
        NuevaLista = []
        NuevaLista.append(filamia)

        for filalista in Listote_pato:
            if enResol(filamia, filalista) and (
                    filamia[14] == (filalista[14]) or (filalista[14] is None)):  #  same resolution and categorization
                NuevaLista.append(filalista)

        for filanueva in NuevaLista:
            Listote_pato.remove(filanueva)

        if len(NuevaLista) > 1:
            NuevaLista[0].append(len(NuevaLista))
            ListaFinal = NuevaLista + ListaFinal
        else:
            NuevaLista[0].append(len(NuevaLista))
            ListaFinal = ListaFinal + NuevaLista

    df = pd.DataFrame(ListaFinal,
                      columns=columnasreps)

    # transforms list into df and back to list (redundant; done to explore the results in excel.)

    ExTrack = df.values.tolist()
    ExTrackCopia = ExTrack.copy()

    df = pd.DataFrame(ExTrackCopia,
                      columns=['PETICIONCB', 'AberrationNo', 'Chr', 'Cytoband',
                               'PositionStart', 'PositionStop',
                               'ProbesNum', 'Amplification', 'Deletion',
                               'pval', 'GeneNames', 'Analizado', 'Tamano',
                               'Categorization', 'CopyNumber', 'Coments',
                               'Coordenadas', 'FECHA_REGISTRO',
                               'FECHA_RESULTADO',
                               'nº reps'])
    #df.to_excel("/Users/rnavarro/Desktop/ExTrackTodos.xls")

    # df = pd.DataFrame(ExTrackCopia, columns=columnasreps)
    # df.to_excel("/Users/rnavarro/Desktop/ExTrackTodos.xls")
    # File to review the final result. ExtrackTodos, shows all the alterations grouped by its CNVtype, coordinates and resolution


    for row in ExTrack:
        if str(row[19]) == 'nan':
            ExTrackCopia.remove(row)

    df = pd.DataFrame(ExTrackCopia,
                      columns=['PETICIONCB', 'AberrationNo', 'Chr', 'Cytoband',
                               'PositionStart', 'PositionStop',
                               'ProbesNum', 'Amplification', 'Deletion',
                               'pval', 'GeneNames', 'Analizado', 'Tamano',
                               'Categorization', 'CopyNumber', 'Coments',
                               'Coordenadas', 'FECHA_REGISTRO',
                               'FECHA_RESULTADO',
                               'nº reps'])
    #df.to_excel("/Users/rnavarro/Desktop/ExTrackSolos.xls")

    # df = pd.DataFrame(ExTrackCopia, columns=columnasreps)
    # df.to_excel("/Users/rnavarro/Desktop/ExTrackSolos.xls")
    # File to review the final result. ExtrackSolos, Erased elements with out the frequency number.

    for row in ExTrackCopia:
        percent = round((row[19] / total) * 100, 2)
        ListaPercent.append(percent)

    ListaNoDup = list(set(ListaPercent))
    ListaNoDup.sort()
    top = max(ListaNoDup)
    bottom = min(ListaNoDup)
    p66 = np.percentile(ListaNoDup, 66)
    p33 = np.percentile(ListaNoDup, 33)

    for row in ExTrackCopia:

        ListaTrack = []

        CNVType = row[14]
        print(CNVType)

        if str(row[10]) == 'None':
            GEN = ''
        else:
            GEN = row[10]

        if str(row[13]) == 'None':
            coments = '---'
        else:
            coments = row[13]

        if len(row[2]) == 4:
            chrom = row[2][:3] + '0' + row[2][3]
            if (chrom[4] == 'X') or (chrom[4] == 'Y'):
                chrom = row[2][:3] + '9' + row[2][3]
        else:
            chrom = row[2]

        percent = round((row[19] / total) * 100, 2)
        Color = decidecolor(DicCopyNumber[CNVType], percent, ListaPercent)

        CNVType = CNVType.replace('', '')
        CNVType = CNVType.replace('ó', 'o')
        CNVType = CNVType.replace('é', 'o')

        # Track Version CYTOGENOMICS 4.0

        ListaTrack.append(chrom)
        ListaTrack.append(row[4])
        ListaTrack.append(row[5])
        ListaTrack.append(
            'Coordinates: ' + str(row[2]) + ':' + str(row[4]) + '-' + str(
                row[5]) + '|Size: ' + str(
                abs(row[4] - row[
                    5])) + '|CNVType: ' + CNVType + '|OMIM: ' + GEN + '|Ocurrence (' + str(
                int(row[19])) + '/' + str(total) + '): ' + str(
                percent) + '% |')
        ListaTrack.append(1000)
        ListaTrack.append('+')
        ListaTrack.append(row[4])
        ListaTrack.append(row[5])
        ListaTrack.append(Color)
        ListaTrack.append('1')
        ListaTrack.append(str(abs(row[4] - row[5])) + '.0')
        ListaTrack.append(int(row[19]))

        Track.append(ListaTrack)

    Track.pop(0)

    df = pd.DataFrame(Track, columns=columnasTrackV4)
    df = df.sort_values(by=[u'title', u'cases'], ascending=[True, True])
    df['cases'] = '0.0'

    Track2 = df.values.tolist()

    for row in Track2:
        if (row[0][3] == '0') or (row[0][3] == '9'):
            row[0] = row[0][:3] + row[0][4]
            Track3_pato.append(row)
        else:
            Track3_pato.append(row)

    ListaTrack = []
    ListaTrack.append('')
    ListaTrack.append('')
    ListaTrack.append('')
    ListaTrack.append('')
    ListaTrack.append('')
    ListaTrack.append('')
    ListaTrack.append('')
    ListaTrack.append('')
    ListaTrack.append('')
    ListaTrack.append('')
    ListaTrack.append('')
    ListaTrack.append('')

    Track3.insert(0, ListaTrack)

    fecha = str(date.today())
    df = pd.DataFrame(Track3_pato, columns=columnasTrackV4Limpias)
    print(tipo,fecha)
    print(df)

    path ="/Volumes/Analysis/nimarray/TrackGenerator/Track_%s_V4" %tipo+"_"+fecha+".bed"
    df.to_csv(path, sep='\t', index=False, encoding='latin-1')


estadisticas["Casos"] = estadisticas["Casos"].astype(int)
estadisticas["Variantes"] = estadisticas["Variantes"].astype(int)

print (estadisticas)

#with open(email_Path, 'r') as f:
#    template = f.read()
subject='[Tracks Genómica] [Nuevos Tracks Disponibles]'
titulo= 'Generación de Tracks de Arrays para Cytogenomics v4 y v5'
frase_body = 'El departamento de Bioinformática y Desarrollo informa de que ya se encuentran  disponibles los tracks de este semestre.'
mail_contacto='bioinfo@nimgenetics.com'
lista_body = estadisticas.to_html()

email_html ={'titulo': titulo, 'frase_body': frase_body, 'mail_contacto': mail_contacto, 'lista_body':lista_body}

nimmail().send(body_msg=email_html,to='jgonzalez@nimgenetics.com,bioinfo@nimgenetics.com,scomin@nimgenetics.com,lros@nimgenetics.com',subject=subject, template=email_Path)

print('END')