# Este script conecta con la base de datos NIMArray y saca un .bed final que se puede cargar en Cytogenomics como Track

from __future__ import unicode_literals
import pandas as pd
import numpy as np
from dbconnect.dbConnect import dbConnect
from datetime import date
import os
import configparser
from email_nimarray import email_nimarray_V5


def enResol(filamia, filalista):
    'Esta función comprueba si las dos alternaciones pasadas como variables están dentro de la resolución establecida.'

    filaCheck = filamia != filalista
    CheckChr = filalista[2] == filamia[2]
    CheckBand = filalista[3] == filamia[3]
    CheckStart = abs(filalista[4] - filamia[4]) < res
    CheckStop = abs(filalista[5] - filamia[5]) < res
    enRes = (filaCheck and CheckChr and CheckBand and CheckStart and CheckStop)

    return enRes

def decidecolor(color, number, ListaPercent):
    'Esta función decide el color final que va a llevar la alteración en el track'

    ListaNoDup = list(set(ListaPercent))
    ListaNoDup.sort()
    top = max(ListaNoDup)
    bottom = min(ListaNoDup)
    p66 = np.percentile(ListaNoDup, 66)
    p33 = np.percentile(ListaNoDup, 33)

    if bottom <= number < p33:
        color = color + 'Claro'

    if p33 <= number < p66:
        color = color + 'Intermedio'

    if p66 <= number <= top:
        color = color + 'Oscuro'

    color = int(DicColor[color])

    return color

columnasTrackV5 = ['Chromosome', 'Start', 'Stop', 'Name', 'score', 'strand', 'thick_start', 'thick_end', 'item_rgb',
                   'block_count',
                   'block_size', 'block_starts', 'exp_count', 'exp_Ids', 'exp_scores']
columnasTrackV5Limpias = [u'title', u'', u'', u'', u'', u'', u'', u'', u'', u'percent']
columnasTrackV5LimpiasFinal = [u'title', u'', u'', u'', u'', u'', u'', u'', u'', u'']
columnasTrackV5AgilentDos = [u'chromosome', u'start', u'stop', u'name', u'score', u'strand', u'thick_start', u'thick_end',
                          u'item_rgb', u'block_count', u'block_size', u'block_starts', u'exp_count']
columnasTrackV5Agilent = [u'chromosome', u'start', u'stop', u'name', u'score', u'strand', u'thick_start', u'thick_end',
                          u'item_rgb', u'block_count', u'block_size', u'block_starts', u'exp_count', u'exp_ids',
                          u'exp_scores']
DicCategorias = {
    'Patogénica': u'Rojo',
    'Probablemente Patogénica': u'Naranja',
    'VUS': u'Amarillo',
    'Probablemente Benigna': u'Verde',
    'Benigna': u'Azul',
    'Basurilla-No Real': u'Morado',
    'Hallazgo incidental': u'Granate',
    'Heredado': u'AzulNav',
    'None': u'Gris'
}
DicCopyNumber = {
    'Deleción Hemicigota': u'Rojo',
    'Deleción Heterocigota': u'Rojo',
    'Deleción Homocigota': u'Rojo',
    'Deleción no determinada': u'Rojo',
    'Duplicación': u'Azul',
    'Triplicación o Superior': u'Azul',
    'Deleción no clasificada': u'Rojo'
}
DicColor = {
    'RojoClaro': u'16751001',
    'RojoIntermedio': u'16724787',
    'RojoOscuro': u'10027008',
    'NaranjaClaro': u'16764057',
    'NaranjaIntermedio': u'16750899',
    'NaranjaOscuro': u'13395456',
    'AmarilloClaro': u'16777113',
    'AmarilloIntermedio': u'16777011',
    'AmarilloOscuro': u'13421568',
    'VerdeClaro': u'10092441',
    'VerdeIntermedio': u'39168',
    'VerdeOscuro': u'26112',
    'AzulClaro': u'10079487',
    'AzulIntermedio': u'3381759',
    'AzulOscuro': u'19609',
    'MoradoClaro': u'13408767',
    'MoradoIntermedio': u'10040319',
    'MoradoOscuro': u'4980889',
    'GranateClaro': u'16751052',
    'GranateIntermedio': u'16711807',
    'GranateOscuro': u'6684723',
    'AzulNavClaro': u'10066431',
    'AzulNavIntermedio': u'3355647',
    'AzulNavOscuro': u'153',
    'GrisClaro': u'12632256',
    'GrisIntermedio': u'8421504',
    'GrisOscuro': u'4210752'
}

ListaFinal = []
NuevaLista = []
Track = [[]]
ListaTrack = []
ListaPercent = []
Reps = []
columnasreps = ['PETICIONCB', 'AberrationNo', 'Chr', 'Cytoband', 'PositionStart', 'PositionStop', 'ProbesNum',
                'Amplification', 'Deletion', 'pval', 'GeneNames', 'Analizado', 'Tamano', 'Categorization', 'CopyNumber',
                'Coments', 'Coordenadas', 'FECHA_REGISTRO','FECHA_RESULTADO', 'nº reps']
Track3 = []
Track3_pato = []
counter = 0
cuantas = 0
count = 0

# Resolucion,NombreTrack,CódigoGestlab
Pruebas = ([15000,'Autismo_Patogenicas','(104)'],[100000,'60K_Patogenicas','(98)'],[40000,'180K_Patogenicas','(353)'],
           [250000,'Prenatal_Patogenicas','(21)'],[25000,'400K_Patogenicas','(42)'],[15000,'Benignas','(104,98,353,21,42)'])

config_parser = configparser.ConfigParser(allow_no_value=True)
bindir = os.path.abspath(os.path.dirname(__file__))
config_parser.read(bindir + "/cfg.cnf")

#Conexion a bases de datos
host = config_parser.get("DB", "hostname")
db = config_parser.get("DB", "db")
user = config_parser.get("DB", "user")
password = config_parser.get("DB", "password")

#Paths
track_Path = config_parser.get("PATHS", "track_Path")


db = dbConnect(host, user, password, db)  # Conecta a la Base de Datos

sql_command = """SELECT DISTINCT PETICIONCB FROM NIMArray.VARIANTES_INFORMADAS;"""
total = pd.read_sql_query(sql_command, db.db)
total = len(total.PETICIONCB.tolist())

for n in Pruebas:
    res = n[0]
    tipo = n[1]
    codigo = n[2]

    counter = 0
    Track = []
    ListaFinal = []
    Track3_pato = []
    df_pato = pd.DataFrame()

    if tipo == 'Benignas':
        sql_command_pato = """SELECT * FROM VARIANTES_INFORMADAS where Analizado = 'Analizado' AND TestType IN %s AND 
            (Categorization = 'Benigna')"""
    else:
        sql_command_pato = """SELECT * FROM VARIANTES_INFORMADAS where Analizado = 'Analizado' AND TestType IN %s AND 
            (Categorization = 'Patogénica' OR Categorization = 'Probablemente Patogénica' )"""

    query = sql_command_pato % codigo
    variant_infor_pato = pd.read_sql_query(query, db.db)
    variant_infor_pato = variant_infor_pato.drop(['TestType'], axis=1)

    Listote_pato = variant_infor_pato.values.tolist()

    while len(Listote_pato) > 0:
        filamia = Listote_pato[0]
        Listote_pato.remove(filamia)
        NuevaLista = []
        NuevaLista.append(filamia)

        for filalista in Listote_pato:
            if enResol(filamia, filalista) and (filamia[14] == (filalista[14]) or (
                    filalista[14] == '')):  #  Que esté en resolución y tenga la misma categorización
                NuevaLista.append(filalista)
                Listote_pato.remove(filalista)

        if len(NuevaLista) > 1:
            NuevaLista[0].append(len(NuevaLista))
            ListaFinal = NuevaLista + ListaFinal
        else:
            NuevaLista[0].append(len(NuevaLista))
            ListaFinal = ListaFinal + NuevaLista

        counter = counter + 1

    df_pato = pd.DataFrame(ListaFinal, columns=columnasreps)

    #  transforms list into df and back to list (redundant; done to explore the results in excel.)

    ExTrack = df_pato.values.tolist()
    ExTrackCopia = ExTrack.copy()


    df_pato = pd.DataFrame(ExTrackCopia,
                      columns=['PETICIONCB', 'AberrationNo', 'Chr', 'Cytoband', 'PositionStart', 'PositionStop',
                               'ProbesNum', 'Amplification', 'Deletion', 'pval', 'GeneNames', 'Analizado', 'Tamano',
                               'Categorization', 'CopyNumber',
                               'Coments', 'Coordenadas', 'FECHA_REGISTRO', 'FECHA_RESULTADO',
                               'nº reps'])
    #df_pato.to_excel("/Users/jgonzalez/Desktop/ExTrackTodos.xls")

    # df_pato = pd.DataFrame(ExTrackCopia, columns = columnasreps)
    # df_pato.to_excel(track_Path+"/ExTrackTodos.xls")
    # File to review the final result. ExtrackTodos, shows all the alterations grouped by its CNVtype, coordinates and resolution


    for row in ExTrack:
        if str(row[19]) == 'nan':
            ExTrackCopia.remove(row)


    df_pato = pd.DataFrame(ExTrackCopia,
                      columns=['PETICIONCB', 'AberrationNo', 'Chr', 'Cytoband', 'PositionStart', 'PositionStop',
                               'ProbesNum', 'Amplification', 'Deletion', 'pval', 'GeneNames', 'Analizado', 'Tamano',
                               'Categorization', 'CopyNumber',
                               'Coments', 'Coordenadas', 'FECHA_REGISTRO', 'FECHA_RESULTADO',
                               'nº reps'])
    #df_pato.to_excel("/Users/jgonzalez/Desktop/ExTrackSolos.xls")

    # df_pato = pd.DataFrame(ExTrackCopia, columns = columnasreps)
    # df_pato.to_excel(track_Path+"/ExTrackSolos.xls")
    # File to review the final result. ExtrackSolos, Erased elements with out the frequency number.


    for row in ExTrackCopia:
        percent = round((row[19] / total) * 100, 2)
        ListaPercent.append(percent)
    for row in ExTrackCopia:

        ListaTrack = []

        CNVType = row[14]

        if str(row[10]) == 'None':
            GEN = ''
        else:
            GEN = row[10]

        if str(row[15]) == 'None':
            coments = '---'
        else:
            coments = row[15]

        if len(row[2]) == 4:
            chrom = row[2][:3] + '0' + row[2][3]
            if (chrom[4] == 'X') or (chrom[4] == 'Y'):
                chrom = row[2][:3] + '9' + row[2][3]
        else:
            chrom = row[2]

        percent = round((row[19] / total) * 100, 2)
        Color = decidecolor(DicCopyNumber[CNVType], percent, ListaPercent)

        CNVType = CNVType.replace('', '')
        CNVType = CNVType.replace('ó', 'o')
        CNVType = CNVType.replace('é', 'o')

        # Track Version CYTOGENOMICS 5.0

        ListaTrack.append(chrom)
        ListaTrack.append(row[4])
        ListaTrack.append(row[5])
        ListaTrack.append('Coordinates: ' + str(row[2]) + ':' + str(row[4]) + '-' + str(row[5]) + '|Size: ' + str(
            abs(row[4] - row[5])) + '|CNVType: ' + CNVType + '|OMIM: ' + GEN + '|Ocurrence (' + str(
            int(row[19])) + '/' + str(total) + '): ' + str(percent) + '% |')
        ListaTrack.append('1000')
        ListaTrack.append('+')
        ListaTrack.append(row[4])
        ListaTrack.append(row[5])
        ListaTrack.append(Color)
        ListaTrack.append('1')
        ListaTrack.append(str(abs(row[4] - row[5]))+'.0')
        ListaTrack.append('0.0')
        ListaTrack.append(percent)
        ListaTrack.append('')
        ListaTrack.append('')
        Track.append(ListaTrack)

    Track.pop(0)

    df_pato = pd.DataFrame(Track, columns=columnasTrackV5Agilent)
    df_pato = df_pato.sort_values(by=[u'chromosome', u'start'])
    df_pato[u'exp_count'] = ''

    Track2_pato = df_pato.values.tolist()

    for row in Track2_pato:
        if (row[0][3] == '0') or (row[0][3] == '9'):
            row[0] = row[0][:3] + row[0][4]
            Track3_pato.append(row)
        else:
            Track3_pato.append(row)

    df_pato = pd.DataFrame(Track3_pato, columns=columnasTrackV5Agilent)



    fecha = str(date.today())
    filename = "/Track_%s_V5" %tipo+"_"+fecha+".bed"
    Final_track_Path = track_Path + filename
    df_pato.to_csv(Final_track_Path, sep='\t', index=False, encoding='latin-1')
    path ="/mnt/Lab_Secuenciacion/Analysis/nimarray/TrackGenerator/Track_%s_V5" %tipo+"_"+fecha+".bed"
    df_pato.to_csv(path, sep='\t', index=False, encoding='latin-1')


email_nimarray_V5()
print('END')

