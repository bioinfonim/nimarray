# -*- coding: utf-8 -*-

from nimutils import others as nim_others

def email_nimarray():
    body = u"""
    <html>

        <head>

            <title>Internal_email-29</title>

            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

            <meta name="viewport" content="width=device-width, initial-scale=1.0" />

            <style type="text/css">

                * {

                    -ms-text-size-adjust:100%;

                    -webkit-text-size-adjust:none;

                    -webkit-text-resize:100%;

                    text-resize:100%;

                }

                a{

                    outline:none;

                    color:#40aceb;

                    text-decoration:underline;

                }
                body{
                    font-family:Arial, Helvetica, sans-serif;,
                }
                a:hover{text-decoration:none !important;}

                .nav a:hover{text-decoration:underline !important;}

                .title a:hover{text-decoration:underline !important;}

                .title-2 a:hover{text-decoration:underline !important;}

                .btn:hover{opacity:0.8;}

                .btn a:hover{text-decoration:none !important;}

                .btn{

                    -webkit-transition:all 0.3s ease;

                    -moz-transition:all 0.3s ease;

                    -ms-transition:all 0.3s ease;

                    transition:all 0.3s ease;

                }
                .btn_link{
                    width:134px;text-decoration:none; background-color:#c20430; color:white;display:block; padding:12px 10px 10px;
                }
                table td {border-collapse: collapse !important;}

                .ExternalClass, .ExternalClass a, .ExternalClass span, .ExternalClass b, .ExternalClass br, .ExternalClass p, .ExternalClass div{line-height:inherit;}

                @media only screen and (max-width:500px) {

                    table[class="flexible"]{width:100% !important;}

                    table[class="center"]{

                        float:none !important;

                        margin:0 auto !important;

                    }

                    *[class="hide"]{

                        display:none !important;

                        width:0 !important;

                        height:0 !important;

                        padding:0 !important;

                        font-size:0 !important;

                        line-height:0 !important;

                    }

                    td[class="img-flex"] img{

                        width:100% !important;

                        height:auto !important;

                    }

                    td[class="aligncenter"]{text-align:center !important;}

                    th[class="flex"]{

                        display:block !important;

                        width:100% !important;

                    }

                    td[class="wrapper"]{padding:0 !important;}

                    td[class="holder"]{padding:30px 15px 20px !important;}

                    td[class="nav"]{

                        padding:20px 0 0 !important;

                        text-align:center !important;

                    }

                    td[class="h-auto"]{height:auto !important;}

                    td[class="description"]{padding:30px 20px !important;}

                    td[class="i-120"] img{

                        width:120px !important;

                        height:auto !important;

                    }

                    td[class="footer"]{padding:5px 20px 20px !important;}

                    td[class="footer"] td[class="aligncenter"]{

                        line-height:25px !important;

                        padding:20px 0 0 !important;

                    }

                    tr[class="table-holder"]{

                        display:table !important;

                        width:100% !important;

                    }

                    th[class="thead"]{display:table-header-group !important; width:100% !important;}

                    th[class="tfoot"]{display:table-footer-group !important; width:100% !important;}

                }

            </style>

        </head>
        <body>
            <table class="flexible" width="600" align="center" style="margin:0 auto;" cellpadding="0" cellspacing="0">

                <tr>
                    <td class="img-flex"><img href="https://ibb.co/vXbtZh3"><img src="https://i.ibb.co/1z3S8Mr/Captura-de-pantalla-2020-01-29-a-las-9-53-37.png" border="0" width="100%" cellpadding="0" cellspacing="0"/></td>
                </tr>
                <tr>

                    <td data-color="title" data-size="size title" data-min="25" data-max="45" data-link-color="link title color" data-face="verdana" data-link-style="text-decoration:none; color:#FFFFFF;" class="title" align="center" style="font:25px/30px verdana; color:#c20430; padding:10 0 40px;">

                                                            Generación de Tracks de Arrays para Cytogenomics v4 y v5
                    </td>

                </tr>
                <tr>

                        <td data-color="text" data-size="size text" data-min="20" data-max="30" data-link-color="link text color" data-link-style="font-weight:bold; text-decoration:underline; color:#40aceb;" align="left" style="font:bold 12px/30px verdana; color:#c20430; padding:0 0 40px;">

                            El departamento de Bioinformática y Desarrollo informa de que ya se encuentran <br> disponibles los tracks de este semestre. <br> Para cualquier consulta contacte con  <a href="mailto:bioinfo@nimgenetics.com"> bioinfo@nimgenetics.com.</a>

                            <ul style="line-height: 170%; padding-left: 50px;">

                                <li>Track variantes Benignas</li>
                                <li>Track variantes Patogénicas y Probablemente Patogénicas detectadas en arrays de Autismo</li>
                                <li>Track variantes Patogénicas y Probablemente Patogénicas detectadas en arrays 60K</li>
                                <li>Track variantes Patogénicas y Probablemente Patogénicas detectadas en arrays 180K</li>
                                <li>Track variantes Patogénicas y Probablemente Patogénicas detectadas en arrays 400K</li>
                                <li>Track variantes Patogénicas y Probablemente Patogénicas detectadas en arrays Prenatal</li>
                            </ul>


                        </td>

                </tr>

                <tr>

                    <td data-bgcolor="bg-button" data-size="size button" data-min="10" data-max="16" class="btn" align="center" >

                        <a target="_blank" class="btn_link" href="http://dashboard.nimgenetics.com/">Descarga los Tracks</a>

                    </td>

                </tr>




            </table>

        </body>
</html>
    """
    email_to = ['jgonzalez@nimgenetics.com,bioinfo@nimgenetics.com,acarro@nimgenetics.com,scomin@nimgenetics.com,lros@nimgenetics.com,mcalvente@nimgenetics.com,jsuela@nimgenetics.com,rnavarro@nimgenetics.com']
    #email_cc = []
    email_subject = '[Tracks Genómica] [Nuevos Tracks Disponibles]'
    #files = []
    nim_others.send_email(body, to=email_to, subject=email_subject)

def email_nimarray_V5():
    body = u"""
    <html>
             <tr>
    
            <td data-color="title" data-size="size title" data-min="25" data-max="45" data-link-color="link title color" data-face="verdana" data-link-style="text-decoration:none; color:#c20430;" class="title" align="center" style="font:25px/30px verdana; color:#eaeced; padding:10 0 40px;">

                Se ha actualizado correctamente la versión V5 del Track

            </td>
    """
    email_to = ['jgonzalez@nimgenetics.com,bioinfo@nimgenetics.com,acarro@nimgenetics.com']
    #email_cc = []
    email_subject = '[Tracks Genómica] [Nuevos Tracks Disponibles - Actualizada V5]'
    #files = []
    nim_others.send_email(body, to=email_to, subject=email_subject)


def email_destaca2(email):
    body = email
    email_to = ['jgonzalez@nimgenetics.com,lros@nimgenetics.com']
    # email_cc = []
    email_subject = '[Subida de Datos NIMArray] [Estado del Proceso]'
    # files = []
    nim_others.send_email(body, to=email_to, subject=email_subject)